(use-modules (srfi srfi-19)
	     (haunt post)
	     (haunt asset)
             (haunt site)
             (haunt builder blog)
	     (haunt builder assets)
             (haunt builder atom)
	     (haunt reader)
             (haunt reader commonmark)
	     (haunt reader skribe)
	     (theme)
	     (utils))

(define (gay text)
  `(span (@ (class "gay")) ,text))
(define (antiqua text)
  `(span (@ (class "antiqua")) ,text))
(define (hebrew text)
  `(span (@ (class "hebrew")) ,text))

(define about-page
  (static-page
   "About Me"
   "/csh/about.html"
   `((h1 "About Me")
     (p "My name is Caleb. I like studying foreign languages and playing with computers. I have a Jellyfin media server for the home with all my old DVDs on it, ready to stream to all the Roku TVs. Languages I enjoy include Hebrew, Yiddish, Russian, Spanish, French, Esperanto, Latin, Mandarin, Pennsylvania Dutch, and Arabic.")
     (h1 "Contact Caleb")
     (p ,(anchor "Addresses" "/csh/contact.txt"))
     (p ,(anchor "631C C434 A56B 5CBD FF21 2346 9764 3795 FA3E 4BCE" "/csh/pubkey.asc"))
     (h1 "Questions?")
     (blockquote (p "Why haven't you been on Zoom lately?"))
     (p "I've decided to stop using Zoom, because it abuses its users.")
     (p ,(anchor "Reasons not to use Zoom" "https://stallman.org/zoom.html"))
     (h1 "Other People")
     (p ,(anchor "MC on Gemini (requires Gemini app)" "gemini://gem.hack.org/mc/log/"))
     (p ,(anchor "David McMackins on the Web" "https://mcmackins.org/"))
     (p ,(anchor "Jason Self on the Web" "https://jxself.org/")))))

(site #:title "Caleb Herbert's Personal Site"
      #:domain "bluehome.net"
      #:default-metadata
      '((author . "Caleb Herbert")
        (email  . "csh@bluehome.net"))
      #:make-slug
      (lambda (post)
        (string-append
         (date->string (post-date post)
                       "~Y/~m/~d") "/"
                       (post-slug post) "/index"))
      #:readers (list commonmark-reader
		      sxml-reader
		      skribe-reader)
      #:builders (list (blog #:prefix "/csh/blog"
			     #:collections `(("Recent" "csh/blog/index.html" ,posts/reverse-chronological))
			     #:theme kolev-theme)
		       about-page
                       (atom-feed #:blog-prefix "/csh/blog"
				  #:file-name "/csh/feed.xml")
                       (atom-feeds-by-tag)
		       (static-directory "csh")))
