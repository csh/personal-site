-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

I prefer real letters. I have international stamps.

Caleb Herbert
PO Box 234
East Lynne, MO 64743
USA

XMPP: caleb@csh.snikket.chat

Active OMEMO fingerprints (ignore others):

* ThinkPad X200 (hermes): 0234875b3056f445e2b597f60c68cda8fa969b91341013665e2a000c3a489648
* Mobile: c6aad82e3ffae73ee28645721142b0692d119c50a3e65a01059a1d866ee63d09

Email: csh@bluehome.net

IRC: Kolev @ Libera.Chat, irc-nerds.net, ircnow.org

Fedi: @kolev@babka.social
-----BEGIN PGP SIGNATURE-----

iQGzBAEBCAAdFiEEYxzENKVrXL3/ISNGl2Q3lfo+S84FAmZVGRwACgkQl2Q3lfo+
S861OQwAlAZqUiYGRP0qKr628b1+rYze7CckMsdftKdytvRbBYFY5cAERyQOr8cI
Gpbaxs4IX6zBioXPr/2PFMy2rRyb7UbaKVapivsgcXSLLFu+9FzbA7KW4ZmUued1
N7GBjtJJpJTtVrUimmtByHldB5gumKRVj3muJ4Glv7lNSCflqWki8mW88tJKHqQw
ZUI5er4WJmaSnuGnS3QqeqNyplj52C+RPoY+IKKNgx3Y+3hCKDHbOmnGoCE8CUwq
Jj/vY1/zWtMiGtL4DLe664tIByCOdxBYRbFmOaRW4QY5yNLBM+fp92m6ulHiwETL
3+QdIhB0BX0f6Hbwz00L12cY+M8/Jr6Q3YlfUSRXznzofZtCAL2zA/0mwO7pKAgT
eOILuXfkyxnw2AE5221Nj3AFYU/l/+MswqpDICGXuJCjVMvLVtPOtAeL+DZeyAwh
MXoiFBkIkMguLICfsweyBVCknraARbcl04F9gMrEOrZjkdiv9TEq0zACKvNNLa88
Dpbv7Aqn
=TX7e
-----END PGP SIGNATURE-----
