;;; Copyright © 2018-2021 David Thompson <davet@gnu.org>
;;; Copyright © 2023-2024 Caleb Herbert <csh@bluehome.net>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (theme)
  #:use-module (haunt artifact)
  #:use-module (haunt builder blog)
  #:use-module (haunt html)
  #:use-module (haunt post)
  #:use-module (ice-9 string-fun)  
  #:use-module (haunt site)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-19)
  #:use-module (utils)
  #:export (kolev-theme
            static-page))

(define %cc-by-sa-link
  '(a (@ (href "https://creativecommons.org/licenses/by-sa/4.0/"))
      "Creative Commons Attribution Share-Alike 4.0 International"))

(define %cc-by-sa-button
  '(a (@ (class "cc-button")
         (href "https://creativecommons.org/licenses/by-sa/4.0/"))
      (img (@ (src "https://licensebuttons.net/l/by-sa/4.0/80x15.png")))))

(define (first-paragraph post)
  (let loop ((sxml (post-sxml post))
             (result '()))
    (match sxml
      (() (reverse result))
      ((or (('p ...) _ ...) (paragraph _ ...))
       (reverse (cons paragraph result)))
      ((head . tail)
       (loop tail (cons head result))))))

(define kolev-theme
  (theme #:name "kolev"
         #:layout
         (lambda (site title body)
           `((doctype "html")
             (head
              (meta (@ (charset "utf-8")))
              (title ,(string-append title " — " (site-title site)))
              (link (@ (rel "alternate")
                       (type "application/atom+xml")
                       (title "Atom feed")
                       (href "/csh/feed.xml")))
	      ,(stylesheet "reset")
	      ,(stylesheet "fonts")
	      ,(stylesheet "kolev"))
             (body
	      (p ,(link "Caleb" "/csh/")))
              ,body))
         #:post-template
         (lambda (post)
           `((h1 (@ (class "title")),(post-ref post 'title))
             (div (@ (class "date"))
                  ,(date->string (post-date post)
                                 "~B ~d, ~Y"))
             (div (@ (class "tags"))
                  "Tags:"
                  (ul ,@(map (lambda (tag)
                               `(li (a (@ (href ,(string-append "/csh/blog/feeds/tags/"
                                                                tag ".xml")))
                                       ,tag)))
                             (or (assq-ref (post-metadata post) 'tags) '()))))
             (div (@ (class "post"))
                  ,(post-sxml post))))
         #:collection-template
         (lambda (site title posts prefix)
           (define (post-uri post)
	     (string-append "/csh/blog/" (or prefix "/csh/blog/")
			    (string-replace-substring
			     (site-post-slug site post) "index" "") ""))

           `((h1 "Caleb S. Herbert")
	     (h1 ,title
                 (a (@ (href "/csh/feed.xml"))
                    (img (@ (class "feed-icon") (src "/csh/assets/images/feed.png")))))
             ,(map (lambda (post)
                     (let ((uri (string-append "/csh/blog/"
                                               (string-replace-substring (site-post-slug site post) "index" "")
                                               "")))
                       `(div (@ (class "summary"))
                             (h2 (a (@ (href ,uri))
                                    ,(post-ref post 'title)))
                             (div (@ (class "date"))
                                  ,(date->string (post-date post)
                                                 "~B ~d, ~Y"))
                             (div (@ (class "post"))
                                  ,(first-paragraph post))
                             (a (@ (href ,uri)) "read more ➔"))))
                   posts)))))

(define (static-page title file-name body)
  (lambda (site posts)
    (serialized-artifact file-name
                         (with-layout kolev-theme site title body)
                         sxml->html)))
