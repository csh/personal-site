title: Backup Procedure
date: 2024-08-17 18:33
---

This document outlines my backup procedure at the time of writing.  It aims to implement the 3-2-1 backup method without storing backups on the internet.

Locations/devices:

* Pixel 4a
* Laptop
* HUSKY HDD
* Farnsworth
* GEORGE HDD

Data sets:

* OS image
* OS state
* Home on Laptop
* Home on SDF
* Home on bluehome.net
* Photos
* Media

Software:

* Clonezilla
* Timeshift
* Borg Backup
* Syncthing

## OS Image

Completely image the OS on Laptop to HUSKY HDD using Rescuezilla (Clonezilla) **every 3 months**, or every time the OS is upgraded or rebased.

Copies:

1. Laptop
2. HUSKY HDD
3. Farnsworth

Offsite backup: HUSKY HDD.

## OS State

**Daily** copy the OS state to HUSKY HDD with Timeshift.

Copies:

1. Laptop
2. HUSKY HDD
3. Farnsworth

Offsite backup: HUSKY HDD.

## Home on Laptop

**Daily** copy user home directory on Laptop with to HUSKY HDD with Borg Backup.

Copies:

1. Laptop
2. HUSKY HDD
3. Farnsworth

Offsite backup: HUSKY HDD.

## Home on SDF

**Daily** copy user home directory on SDF to Laptop with `rsync`.

Copies:

1. SDF
2. Laptop
3. HUSKY HDD

Offsite backup: HUSKY HDD.

## Home on bluehome.net

**Daily** copy user home directory on bluehome.net to Laptop with `rsync`.

Copies:

1. bluehome.net
2. Laptop
3. HUSKY HDD

Offsite backup: HUSKY HDD.

## Photos

**Daily** copy photos with Syncthing.

Copies:

1. Pixel 4a
2. Laptop
3. HUSKY HDD

Offsite backup: HUSKY HDD.

## Media

**Weekly** copy Media, Photos, Homes, OS State, OS Image from HUSKY HDD to GEORGE HDD with `rsync`.

**Weekly** swap HUSKY HDD for GEORGE HDD at uncle's house.

Copies:

1. HUSKY HDD
2. Farnsworth
3. GEORGE HDD

Offsite backup: GEORGE HDD.
