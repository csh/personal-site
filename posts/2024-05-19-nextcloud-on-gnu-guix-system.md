title: Nextcloud On GNU Guix System
date: 2024-05-19 18:28
---

I found a guide on how to set up Nextcloud on GNU Guix System, but ran into a few complications.

[Nextcloud and Guix System Server](https://www.gnucode.me/nextcloud-and-guix-system-server.html)

I currently do not have a domain.  I have instead been using the hostname on the local network, "farnsworth".

## 4. Set up a basic nginx static website without encryption

The web server works, but I get the error message "403 Forbidden".

I did give the web server the recommended file permissions:

```
sudo chown -R nginx /srv
sudo chmod -R u-rwx /srv
```

## 5. HTTPS support

System reconfigure succeeded.  However, I was unable to set up certificates.

```
caleb@farnsworth ~$ sudo /var/lib/certbot/renew-certificates
Password: 
sudo: /var/lib/certbot/renew-certificates: command not found
```

## 6. Modify Guix config

The instructions link to a very long file.  I do not know exactly which changes to make to my system configuration.

[the-nx.com-current-config.scm](https://notabug.org/jbranso/linode-guix-system-configuration/src/master/the-nx.com-current-config.scm)

> Modify your guix config based on my the-nx.com-current-config.scm. You will need to enable these services (dbus-service), (service docker-service-type), (elogind service), (service certbot-service-type), and (service nginx-service-type).

I do have:

* D-Bus
* Docker
* elogind
* certbot
* Nginx

[farnsworth.scm, lines 44-97](https://codeberg.org/csh/dotfiles/src/commit/cc746839bce9ad28a490aba41760fe6dddb203fb/guix/.config/guix/farnsworth.scm#L44-L97)
