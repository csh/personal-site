title: Packed for DC
date: 2024-09-12 13:39
---

Did laundry.  Took a shower.  Packed for D.C.  Chatted online.

## Morning

Got up.  Got dressed.  Gathered laundry.  Went downstairs.  Got a Bubly.  Took my meds.  Took a shower.  Started laundry.  Ate Cheerios with soy milk.

Took a nap.

Put laundry in the dryer.

## Afternoon

13:30.  Called Wellbeing Center.  I'll be picked up at 08:30 tomorrow morning.

Chatted online.  Checked Mastodon.

Took a nap.

## Evening

17:29.  Ate rice noodles with broccoli, green beans, tofu, and peanut
sauce.  Watched _MASH_.

20:10.  Went upstairs.