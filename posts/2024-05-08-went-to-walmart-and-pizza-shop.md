title: Went to Walmart and Pizza Shop
date: 2024-05-08 17:31
---
Woke up at 22:30 last night and stayed up until midnight.

07:00.  Woke up but went back to sleep.

10:00.  Got up.  Got dressed.  Went downstairs.  Got a Squirt soda.  Took pills.  Brushed hair.

Went to Family Services.  They were closed for President Harry S. Truman's birthday.

Went to Walmart.  Got Pepto Bismol for someone at home.

Went to Papa Murphey's.  Got a small bottle of Pepsi, two large meat pizzas, and a large Gourmet Vegetarian pizza with no cheese on regular crust with extra sauce and sub artichokes for black olives.  Paid with my food stamps.  Fifty-three dollars.

Came home.  Took dogs out.  Got on laptop Hermes.  Logged into Snikket.  The tomato and cucumber plants are starting to grow something!

12:00.  Waited on pizzas to cook.  Checked email.  Checked web feeds.  Checked Gemini feeds.  Checked FSF member forum.  Checked System Crafters forum.  Checked Mastodon.  Checked Trisquel GNU/Linux forum.

[Farewell to Geminispace.info [requires Gemini app]](gemini://jsreed5.org/log/2024/202405/20240508-farewell-to-geminispace-info.gmi)

Zerock gave me some code to keep Emacs from using themes in the terminal, so that the theme "Deeper Blue" does not turn my terminal into a Blue Screen of Death.

```elisp
(defun disable-all-themes ()
  "disable all active themes"
  (dolist (theme custom-enabled-themes)
    (disable-theme theme)))

;; Disable color theme if in console.
(if (not window-system)
    (disable-all-themes))
```

[Zerock](https://mcmackins.org/)

13:00.  Ate half a pizza with tahini and crushed red pepper.  Got another lime Bubly.

14:30.  Added Tyler Back's website (of System Crafters) to my web feeds.  Added fonts and Icedove to Chromebook Guix's manifest file and pushed to Codeberg.  Moved web feeds from Liferea to Emacs Elfeed.

[Tyler D. Back (feed)](https://tylerdback.com/index.xml)

15:00.  Watched Guy's Grocery Games on Food Network.

16:14.  Turned on my air conditioner upstairs.  Today I learned that Debian has a symlink called "editor" that goes to a prioritized list of text editors, and defaults to GNU nano.  I changed it to Emacs.

17:26.  Found out that LilyPond's Emacs Lisp files get installed but Emacs cannot find them.  I'll poke around in /gnu/store to see where it is.

19:00.  Ate the rest of my pizza.  Rinsed mouth.  Stayed upstairs with my air conditioner, because downstairs is hot.  Chatted on IRC.

21:00.  Added documentation strings to my Scheme version of my script, "post".  Read Riastradh's Lisp Style Rules.

23:00.  Went to sleep.
