title: Tore Down the Garden
date: 2024-08-21 09:44
---

Chatted online.  Listened to the radio.  Tore down the garden.  Called friend.  Visited with family.  Watched TV.  Performed backups.

Today's agenda was to get a scheduled time for my ride tomorrow, and call a friend.

Had a long [Snikket](https://snikket.org/) call last night with a friend.

## Morning

08:30.  Got up before alarm.  Got dressed.  Combed hair.  Checked email and Snikket on phone.  Went downstairs.  Made tea.  Got a Starry.  Took meds.

09:46.  Got on laptop.  Checked [Free Software Foundation](https://fsf.org/) member forum.  Tuned into [KZHE](https://kzhe.com/) with the [Shortwave](https://flathub.org/apps/de.haeckerfelix.Shortwave) app.

10:00.  Fox News came on, so I switched it to 98.9 The Rock.  Went downstairs to check out the basement.  We want to finish the basement.

11:00.  Went out to the back yard and helped take down the garden.  Washed hands afterward.

## Afternoon

12:00.  Called friend but he did not answer.  Ate a bowl of Cheerios with soy milk.

13:00.  Visited with Uncle John on the front porch.

15:00.  Spent forever looking for old _résumé_ templates for LaTeX.  Couldn't find them anywhere.  It turned out they were in my home directory all along, in my Projects folder.

16:20.  Called and emailed Wellbeing Center about attending.  Watched Food Network.

## Evening

17:00.  Talked to Mom and friend from Norway about my poor adulting skills.  Ate a banana dipped in chocolate peanut butter, and dates dipped in chocolate peanut butter.

18:00.  Mom made me miso soup and rice for dinner.  Had miso soup with a bowl of rice topped with vegan furikake rice seasoning.  Drank a Corona beer.

![Miso soup with a bowl of rice](https://babka-mastodon.nyc3.cdn.digitaloceanspaces.com/media_attachments/files/113/002/915/344/340/467/original/edcd4ea5c598d50b.jpeg)

19:00.  Mom packed me rice and miso soup for tomorrow's lunch at the Wellbeing Center, since they do not serve vegan food.

20:00.  Watched Food Network on Spectrum TV.  Watched _MASH_.  Performed backups.

## Tomorrow

* Go to Wellbeing Center
