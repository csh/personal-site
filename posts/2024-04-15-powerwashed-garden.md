title: Powerwashed Garden
date: 2024-04-15 21:21
---
Got up at 08:00. Took meds with lime Bubly. Uncle powerwashed the garden. The ground is all muddy and the plants look destroyed. Filed issue with Conversations.

Drank a can of Squirt with the intent of doing some gardening work, but we are coming back in for a while, enough time for me to make and eat breakfast. Had the usual oatmeal with apple butter and hemp seeds, and tea with soy milk. Checked email while waiting on the oats. Nothing good in the inbox today.

Filed issue with Conversations, asking for the ability to spoof the client name so that people cannot tell what OS you're using.

[Allow client name spoofing](https://codeberg.org/iNPUTmice/Conversations/issues/251) 

Took a nap, got up at 14:00. Laptop battery was nearly dead. Went upstairs to charge. Chatted on IRC. Enabled logging in my XMPP chat client, Profanity. I hope this will let me see old messages after restarting the client.

16:08. Went downstairs. Battery charged. Got another can of Bubly and put it over ice. Watched Beat Bobby Flay on Food Network.

17:00. Had seven pieces of bread with guacamole and salsa verde. Watched M*A*S*H*. Found vegan, Kosher for Passover recipes.

[Vegan Start: Kosher for Passover](https://veganstart.com/category/kosher-for-passover/) 

[Passover - Vegan Ashkenazi](https://toriavey.com/recipes/passover/vegan-ashkenazi/) 

20:00. Watched the movie Diary of a Mad Black Woman. Battery is drained. I have an hour and a half left, so I should be good until bedtime.

20:45. Brought laptop upstairs to charge. Turned on air conditioner. Found the MATE lock screen command, so I can use it in CWM.

```
mate-screensaver-command -l
```

Ate a slice of pizza and a small rice bowl of cashew "fried" rice. Went back upstairs and got ready for bed.

