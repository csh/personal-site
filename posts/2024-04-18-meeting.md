title: Meeting
date: 2024-04-18 11:10
---
Got up at 10:00. Got dressed, went downstairs. Brushed my hair, combed and oiled my beard, put on deodorant. Took my pills. Made tea and oats: Had oats with just apple butter and no hemp seeds. Drank tea with soy milk.

Got on laptop. Logged into my XMPP client, Profanity. Opened a new log post in Emacs and started journaling. Moshed into SDF Public Access UNIX System and got on COM. Checked email, Mastodon, Gemini, and web.

12:22. Left for appointment. Drank tea in the waiting room. Got gas on the way home.

15:00. Returned from appointment. Everything went OK, I think. I hope it did, at least. Finally restarted my mobile device: It had to sync all my messages on Snikket. Subscribed to the podcast Free As In Freedom.

[Free As In Freedom](https://faif.us/) 

15:53. Had a wonderful visit to the bathroom. Marked all feeds as read.

17:00. Flashed GNU Boot to Hermes, my Lenovo ThinkPad X200s.

[How to Flash GNU Boot](/csh/posts/how-to-flash-gnuboot/) 

18:00. Backed up. Rebooted. GNU Boot was successfully installed! Diagnosed nsd issues with jrmu on IRCNow. Had two lentil-tahini sandwiches and a slice of bread with tahini. Washed it down with some strawberry Bubly.

19:50. Finally got reverse DNS working. Now all I need to do is set up mail! I may wait til tomorrow to do that. I'm getting tired.

21:00. Used mouthwash. Went upstairs. Got ready for bed.

