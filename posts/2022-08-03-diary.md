title: Dear Diary
date: 2022-08-03 00:00
---
Wednesday 20220803

02:00 CDT  Can't get to sleep
  
  I almost bought tobacco yesterday (20220802). Too scared of what Mom
  would say. It is currently two in the morning and I cannot for the
  life of me get back to sleep. Talked to my coworker Sal [last name],
  or texted him. Took a shower. Brushed my teeth. Mom cleaned and
  bleached my mask. It's so white!

  Out of condoms and lube. Need to go to Walmart and get some more.
  Cannot wait to have sex again! I have not had my dick sucked in
  ages.

  I wish this pen put out more ink. The faintness of the lines worries
  me. That said, it forces me to better control my distinction between
  l and t. Getting the line to not loop is a bit difficult.

  I looked into native use of tobacco. Their plant, nicotiana rustica,
  actually has more nicotine than commercial tobaccos, and natives do
  indeed smoke it, albeit without inhaling. I still cannot shake the
  curiosity for the unique flavors and smoky aroma of unadulterated
  tobac- co, and the very unique art of tobacco pipes. You just cannot
  get that with marijuana!

  It is now 03:00 CDT. and I cannot get back to sleep. I wish I had
  somebody to cuddle and sleep with. Agh, this nib is too fine! I wish
  I had something thicker.
