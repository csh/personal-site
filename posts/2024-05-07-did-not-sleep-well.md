title: Did Not Sleep Well
date: 2024-05-07 14:45
---
Did not sleep well.  Stayed up til 05:00.  Woke up at 11:45.  Got up at 13:00.

Chatted on IRC on laptop Miller, the Chromebook.  Went downstairs.  Got a lime Bubly.  Took pills.  Brushed hair.

14:00.  Got on laptop Hermes.  Logged into Snikket.  Checked email.  Checked FSF member forum.  Checked System Crafters forum.  Checked Trisquel forum.

15:00.  Checked Mastodon.  Had a delightful visit to the bathroom.  Checked web feeds.  Checked Gemini feeds.  Pushed Emacs appearance changes to Codeberg.

[Emacs as a Shell: Part 1 [requires Gemini app]](gemini://gem.librehacker.com/gemlog/starlog/20240506-0.gmi)

15:44.  Ate four slices of bread with guac.  Ate a medjool date.

Fell asleep.

18:00.  Watched Chopped on Food Network.  Today I learned the System Crafters theme is "Pale Night."

19:00.  Drank a margarita.

20:00.  Turned my air conditioner on.

20:30.  Went upstairs.
