title: Studied Pennsylvania Dutch
date: 2024-09-11 17:16
---

Went to Wellbeing Center.  Studied Pennsylvania Dutch.  Took therapy class.  Did chores.  Came home.  Watched TV.

## Morning

06:00.  Got a wake-up call.  Got up.  Got dressed.  Went downstairs.  Got a Bubly.  Took my pills.  Made tea.  Ate Cheerios and soy milk.  Mom packed my lunch.  I also packed two Bubly sparkling waters and two grain bars.

Sat outside with Mom while waiting for my ride.  Got in the van.  KFKF was playing a playlist of 9/11 themed songs and historical recordings.  "A Country Boy Can Survive" was reworded to "America Will Survive" and made to include the Americans in the cities, not just the country.

09:00.  Morning meeting.  I decided to go to Health & Wellness class, take care of the computer room, and take care of the lobby.  I did not order a lunch.

10:12.  Health & Wellness class.  I learned that even though reframing thoughts away from negative interpretations is healthy, it's not always the best thing to do when talking with others about problems, as sometimes people just want you to listen.

Sat outside and took a walk.  Colored a paper bag with color markers in the activity room.

## Afternoon

12:00.  Lunch.  Had rice noodles with tofu, broccoli, green beans, and peanut sauce.  Finally figured out how to use their microwave: You have to punch in zeroes to get a full minute.  Drank a Bubly to finish my meal.

12:30.  Waited on my case worker.  Called case worker.  He canceled our appointment.  He'll see me next week.

12:45.  Learned more Pennsylvania Dutch from the [_Hiwwe wie Driwwe_ course](https://hiwwewiedriwwe.wordpress.com/learn-the-dialect/).  Got to lesson 20.

14:30.  Vacuumed the computer room.  Wiped down the chairs and workstations.  Helped move tables back into the classroom.  Suggested vegan Meatless Monday on my paperwork; found out one of the counselors there avoids beef and pork.

15:30.  Signed out.  Got in the van.

16:00.  Came home.  Tired.  Emptied my bag.  Got a bottle of water.  Ate a grain bar.

## Evening

17:30.  Watched Food Network.

18:46.  Ate vegan meat lover's mini pizza with Hidden Valley plant-based ranch dressing.

Took a nap.

20:00.  Woke up.

21:00.  Went to bed.

## Tomorrow

* Walmart
* Laundry
* Mom's laundry
* Pack for D.C.