title: Getting Ready for Passover
date: 2024-04-21 23:52
---
Had issues logging into SSH. Got ready for Passover. Customized GNU Emacs. Trimmed my mustache.

Woke up at 06:00. Too early. Woke up at noon. Too late! Went downstairs, got a coconut pineapple Bubly, took my pills.

I tried logging into SDF. The password prompt did not come up. It just hanged! I don't know how I'll be able to push to Git today...

[gpg-agent: SSH Not Working](https://trisquel.info/en/forum/gpg-agent-ssh-not-working) 

Finished the lentils for breakfast. Had three slices of bread with a spoonful of tahini and two spoonfuls of lentils. Yum!

Checked Mastodon, web feeds, Gemini feeds, and email.

[‘A roof over our people’s heads’: the Indigenous US tribe building hempcrete homes](https://www.theguardian.com/us-news/2024/apr/16/hempcrete-indigenous-tribe-minnesota) 

Got ready for Passover. Made grocery list.

* celery
* fresh dill
* cashew butter
* cashew milk

Installed updates and rebooted laptop in the hopes that SSH will work. Somehow, it worked!

I've been irritable today, for some reason. Took a nap. Woke up at 17:00. Drank a Starbucks frap. Got a mug of tea.

Created my configuration file init.el for Emacs. Started with settings recommended by System Crafters.

[Basics of Emacs Configuration](https://systemcrafters.net/emacs-from-scratch/basics-of-emacs-configuration/)

[The 6 Emacs Settings Every User Should Consider](https://systemcrafters.net/emacs-from-scratch/the-best-default-settings/) 

Finally figured out how to indent 2 spaces in Bash scripts, thanks to tdback on System Crafters IRC. Added the line to my init.el.

```
;; Indent 2 spaces in shell scripts, per the Free Software
;; Foundation's Bash style guide.
(add-hook 'sh-mode-hook (lambda () (setq sh-basic-offset 2)))
```

20:20. Ate two pieces of pizza with tahini. Trimmed my mustache.

22:00. Went upstairs.

23:30. Installed Kanata and KMonad, and created directory for Emacs Lisp files. Could not figure out how to use either Kanata or KMonad. The only thing I want to do is map AltGr h to ẖ. Logged off before midnight.

[Kanata](https://github.com/jtroo/kanata) 

[KMonad](https://github.com/kmonad/kmonad) 
