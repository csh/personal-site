title: Dear Diary
date: 2022-08-05 00:00
---
Friday 20220805

Woke up at midnight. Brushed my teeth and oiled my beard. I hope I
  can get up at 04:30 CDT  this morning, so I have time to make coffee and
  brush my teeth, and oil my beard. Well, it's fifteen til one; time
  to get back to sleep now.
