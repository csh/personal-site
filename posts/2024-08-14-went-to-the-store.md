title: Went to the Store
date: 2024-08-14 22:09
---

Slept in.  Listened to the radio.  Went to the store.  Chatted online.  Watched TV.  Performed backups.

Today's agenda was to go to the store.

## Morning

Woke up to my alarm at 08:00 but went back to bed.  Didn't get up until 11:30.  My neck was cramping extremely bad from laying in Mom's bed using my laptop.  I need to start sitting in a chair or something.

11:30.  Got up, got dressed, combed my hair, went downstairs.  Got a Starry and took my meds.  An amber alert went off on my mobile.

## Afternoon

12:00.  Plugged my mobile into the charger.  Got on my laptop.  Turned on [KZHE](https://kzhe.com/).  Backed up important files in home directory on Farnsworth (my server) to my main drive.  Checked Mastodon.  Checked email.

15:00.  Came home from Walmart.  Got Field Roast sausage egg and cheese biscuit, cilantro, green onion, tofu, guacamole, vegan cheese slices, margarine, Bubly, Starry, beer, soy milk, barbecue pistachios, and a few other things.  Drank Starbucks Oatmilk Frappuccino and Starbucks Pink Drink on the way home.  Mom got a lottery ticket.  Brought in the groceries and put them up, and took out the trash.  Got an orange cream Bubly sparkling water from the fridge.

15:30.  Ate [vegan sausage egg and cheese](https://fieldroast.com/product/classic-style-sausage-egg-cheese-plant-based-breakfast-sandwich/) muffin.  Delicious!  Can't wait for grilled cheese tonight.

16:30.  Discovered KDE chat client [Kaidan](https://kaidan.im/) but it does not support group chats.  I guess Dino is still supreme.

## Evening

17:00.  [Singpolyma](https://singpolyma.net/) introduced me to [Decker](https://beyondloom.com/decker/).  Watched Food Network.

18:00.  Chatted online.  Ate my grilled cheese and tomato soup.

19:00.  Installed Night Theme Switcher and Colorblind Filters GNOME extensions with the Extension Manager app.  Now, my theme automatically changes at 5 PM and my screen is grayscale.

20:00.  Watched Food Network, chatted online.  Installed the Apostrophe app to more nicely edit my documents.  Synced remote home directories to my laptop.  Ran backup.

21:00.  Watched _MASH_ on [Jellyfin](https://jellyfin.org/).  I must say, I'm enjoying composition in Apostrophe much better than in GNOME Text Editor.  It's very distraction-free and clean.  Finished my tea.  Flipped over my meds (to show I have yet to take them) and marked off the day on the calendar.  Reading about [`btrbk`](https://github.com/digint/btrbk) to back up my actual OS, not my personal files.

22:00.  Went upstairs.

## Tomorrow

* Take out the trash