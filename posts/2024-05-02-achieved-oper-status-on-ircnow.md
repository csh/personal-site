title: Achieved Oper Status on IRCNow
date: 2024-05-02 12:16
---
Achieved oper status on IRCNow.  Worried about Uncle John.  Ate spaghetti for dinner.

11:30.  Got up.  Got dressed.  Went downstairs.  Got a blackberry Bubly.  Took pills.  Combed hair and beard.

12:00.  Ate Cheerios with soy milk.  Took out the trash.  Got on laptop Hermes.  Checked System Crafters forum.  Checked Mastodon.  Checked email.  Checked web feeds.  Checked Gemini feeds.  Logged into Snikket.  Checked Trisquel forum.

13:00.  Fell asleep.

14:00.  Woke up.  Achieved oper status on IRCForever for IRCNow.

15:10.  Went to local convenience store.

16:02.  Found out OpenBSD disables Unicode by default over SSH connections.  Every other host I've connected to (NetBSD, Trisquel, Fedora, Guix System) displays Unicode just fine once it reaches the host, but OpenBSD replaces entered Unicode characters with question marks!  I had to set an environment variable to get Unicode to work on my OpenBSD SSH session.

```
export LC_CTYPE=en_US.UTF-8
```

17:00.  We're worried about Uncle John.  He hasn't been seen since 06:30 today.  Ate a few dates and walnuts.  Got a Squirt soda.  Found out where John is: At the local hospital for overnight observation, for shortness of breath.

18:00.  Ate whole wheat spaghetti with pasta sauce, bell pepper, mushroom, fennel, and walnuts.  Got a blackberry Bubly.  Watched King of the Hill.

20:00.  Watched Bob's Burgers.  Went upstairs.
