title: Dear Diary
date: 2023-11-20 11:30
---

Woke up.  I went to sleep at 05:00; I did not want to look at the
clock or see what time it is.  Got up, put some clothes on, went
downstairs, took my meds.

11:30.  The clock said 11:30.  Whew!

Mom asked me what I wanted for Thanksgiving.  We decided on [mashed
potatoes and gravy][1], sweet potatoes, [green been casserole][2],
cranberry sauce, veggie tray with [tofu French onion dip][3], pumpkin
pie, and [key lime cheesecake][4].

12:30.  Wrote an [email][7] to the `help-guix` mailing list about my
Chromebook UCM config I tried to get working [last night][4].
Attached me and [Efraim][6]'s code.

Had a bowl of macaroni and peas.  Yum!

Went to Whole Foods in Overland Park, near Pan-Asia Market.  Got vegan
apple pie, vegan pumpkin pie, vegan turkey roast, a fruit tray, vegan
oatmilk eggnog cold brew coffee, and vegan croissants.

Went to Hy-Vee in Belton.  Got vegan cheesecake, vegan heavy whipping
cream, vegan chocolate peanut butter cups, vegan marshmallows, sweet
potatoes, veggie tray, olives, vegan French onion dip.

Went to Uncle John's.  Got turkey.

17:30.  Returned home.  Took in groceries.  Helped Mom put away the
refrigerated stuff.  Mom baked the croissants.

19:00.  Had a croissant.  [Tried to add CSS][8] to my site (and
failed).  Added CSS files to site source, even though I cannot use
them yet.  Added [Alyssa][9]'s CSS along with [David Thompson][10]'s.

> Personal Site v2.2:
>
> - Add CSS files
> - New posts

21:38.  Made no progress on the CSS issue.  Watched _All In The
Family_.

22:06.  Looking at [Awesome Haunt][11], since it's a simpler website.
But it's too simple.  It doesn't have a blog or anything.

23:02.  Jim took the dogs out.  I still couldn't figure out how to add
CSS and extra pages to my website...

[1]: https://www.forksoverknives.com/recipes/vegan-salads-sides/mashed-potatoes-and-gravy/
[2]: https://www.drfuhrman.com/recipes/1686/nutty-green-bean-casserole
[3]: https://www.drmcdougall.com/recipes/tofu-dips/
[4]: https://www.target.com/p/daiya-dairy-free-gluten-free-vegan-key-lime-frozen-cheezecake-14-1oz/-/A-53523881
[5]: https://bluehome.net/csh/2023/11/19/dear-diary.html
[6]: https://tooot.im/@efraim
[7]: https://lists.gnu.org/archive/html/help-guix/2023-11/msg00052.html
[8]: https://codeberg.org/csh/personal-site/issues/6
[9]: https://rosenzweig.io/
[10]: https://dthompson.us/
[11]: https://awesome.haunt.page/
