title: Memorial Day
date: 2023-05-27 00:00
---

Today is Memorial Day. We got up at 6:00 a.m. to leave with Aunt Sue
to go down to Verona and Monett, to tend to Grandma's grave. I rushed
to eat my raisin bran, as we thought Aunt Sue would be arriving at
seven, not six.

![Grandma's grave](/csh/assets/images/memorial-day-2023.jpg)

Before leaving town, we went to QuikTrip. I got a large can of lime
Bubly and a white chocolate macadamia nut Lenny & Larry's Complete
Cookie.

