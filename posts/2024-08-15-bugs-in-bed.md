title: Bugs in Bed
date: 2024-08-15 21:11
tags: Guix, diary
---

Chatted online.  Listened to the radio.  Did laundry.  Watched TV.  Performed backups.

Today's agenda was to wash my bedding and take out the trash.

## Morning

Bugs were crawling on me all night.  I don't know what kind of bugs they were, but they were not biting me.  I'm sure they got in through the air conditioner.  I need to wash my bedding; maybe that would help.

07:30.  Woke up.  Chatted on [Snikket](https://snikket.org/).  Got dressed.  Went downstairs.  Mom still wasn't up.  Made tea.  Ate a bowl of Frosted Flakes with soy milk.  Went back upstairs.  Chatted for a little while.

09:00.  Fell asleep.

## Afternoon

12:00.  Woke up.  Went downstairs.  Filled travel mug with tea.  Took meds.  Got on laptop.  Turned on Shortwave to listen to [KZHE](https://kzhe.com/).  Logged into server Farnsworth and compressed my image of laptop Miller.  Had a delightful visit to the restroom.  Transferred compressed disk image of Miller to my backup drive.

13:00.  Aunt Sue came home.  Checked email.  Chatted online.

14:23.  Put laundry in the dryer.  Brought Mom's laundry into the laundry room.  Reheated some macaroni and had it with sriracha.  Switched to 98.9 The Rock and then to 101 The Fox.

15:00.  Discovered a GNOME GUI for `pass` called [Ripasso](https://github.com/cortex/ripasso), but it's not complete.  Watched Food Network on Spectrum TV.  Installed [QtPass](https://qtpass.org/).

16:00.  Today I learned [Gentoo recommends XFS](https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Disks#Filesystems) along with Red Hat.

## Evening

17:00.  Got more tea.  Just got an idea: Reconstitute jasmine green tea concentrate with Starry lemon-lime soda!

18:00.  Got a Starry lemon-lime soda.  Drank it quickly.  Got a peach Bubly sparkling water.  Wrote some code to mount my backup drive automatically on Farnsworth.  I hope it works.

```
(file-system (mount-point "/mnt/HUSKY")
             (device (file-system-label "HUSKY"))
             (type "xfs")
             (create-mount-point? #t))
```

Ate two grilled cheeses stuffed with green onion and cilantro, with a side of sliced tomato with garlic, Mrs. Dash, and cilantro.

19:00.  Watched _Beat Bobby Flay_ on Food Network.

20:00.  Trying to upgrade my Guix system on Farnsworth and automount HUSKY.  I'll postpone these steps til tomorrow, because I need to make sure the Jellyfin server works tonight for Mom.

```
$ guix pull
$ sudo guix system reconfigure guix/.config/guix/farnsworth.scm
```

21:00.  TV lost internet connection but my laptop did not.  Chatted on [SDF](https://sdf.org/).

Performed backups.  First, I finished my work on this article.

```
caleb@miller:~/Private/Projects/personal-site$ make
caleb@miller:~/Private/Projects/personal-site$ make publish
```

Then, I synced my remote home directories to my laptop.

```
caleb@miller:~/Sync$ ./pull 
```

Then I just went into Pika Backup and pressed Backup Now.

## Tomorrow

* Shower
* Cut fingernails
* Wash bedding
* Dry bedding
* Upgrade Farnsworth
* Light Sabbath candles