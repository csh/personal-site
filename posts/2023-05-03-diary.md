title: Dear Diary
date: 2023-05-03 00:00
---

I couldn't sleep well last night. I ended up breaking my rule of no
phones in bed by grabbing my mobile device to look at
[IRC](https://libera.chat/) until I fell asleep.

I got up at noon. I took my medications. I drank some coffee with
honey and almond milk. I ate a bowl of Cheerios with almond milk.

I revamped my website. I used [Jekyll Now](http://www.jekyllnow.com/)
to add message previews and an RSS feed. Something's wrong with the
colors and style of the site still, but at least the thing works.

I'm looking into connecting my [Western Electric 2500](https://commons.wikimedia.org/wiki/File:AT%26T_push_button_telephone_western_electric_model_2500_dmg_black.jpg)
to my VOIP number so that I get calls on my telephone and text
messages on my laptop. I'm thinking of buying a [SIP ATA](https://www.amazon.com/Grandstream-GS-HT802-Analog-Telephone-Adapter/dp/B01JH7MYKA/ref=sr_1_3?crid=P9WL49GMZ6FO&keywords=sip+ata&qid=1683143032&sprefix=sip+ata%2Caps%2C244&sr=8-3).

5:34 p.m. Just returned from a walk. Walked with Mom to turn in the
rent check.

I had an Arnold Palmer with a shot of bourbon.

I had dinner. I ate four slices of bread with a big bowl of vegetable
soup.

I spent the night on IRC.

