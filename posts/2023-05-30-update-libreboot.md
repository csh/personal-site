title: Update Libreboot
date: 2023-05-30 00:00
---

These instructions currently do not work. I have no idea how to
disable memory protection, with `grubby` or otherwise.

## Disable Memory Protection

Edit `/etc/default/grub`.

    sudo nano /etc/default/grub

Add `iomem=relaxed` to the end of the `GRUB_CMDLINE_LINUX` line, **inside** the quotes. Mine looks like the following.

    GRUB_CMDLINE_LINUX="rd.luks.uuid=luks-024de9aa-b8cc-4a91-a861-59a4c6d4e712 rhgb quiet iomem=relaxed"

Update GRUB.

    sudo grub2-mkconfig -o /boot/grub2/grub.cfg

Reboot.

## Flash

Download and extract [Libreboot utils](https://mirrors.mit.edu/libreboot/stable/20160907/libreboot_r20160907_util.tar.xz).

Enter the extracted directory.

    cd ~/Downloads/libreboot_r20160907_util

Find your ROM size.

    ./flashrom/x86_64/flashrom -p internal

Download and extract [the appropriate ROM](https://mirrors.mit.edu/libreboot/testing/20230423/roms/).

    sudo ./flash update
