title: Dear Diary
date: 2022-11-05 00:00
---
I got up.

I made tea with Bigelow Constant Comment, Twinings Earl Grey, and
genmaicha.

I got dressed. I put on my rainbow heart pin and yamulke.

I took my meds and B12.

I ate breakfast. I had Tasteeos (Cheerios) with soy milk and honey.

Mom and I went to the hospital to see Grandma. Uncle Jeff was already
there. Grandma did not want visitors, so I stayed in the background
and did not say anything. We went to the waiting room, and I took some
of the no-cost coffee and read Genesis 12 from the Gideon Bible on the
table.

Uncle Jeff got a nice handicap van from amsvans.com.

When we got home, I had olive bread with garlic hummus for lunch.

Mom gave her boyfriend Michael "Mike" Domerese his birthday present
early. It was a winter coat.

Mom is making vegetarian beans with vegetarian beef broth and
cornbread.

Mom is fighting with Uncle Joe and Uncle Jim on the phone, because
they insist on going to the hospital and disturbing Grandma's rest.

