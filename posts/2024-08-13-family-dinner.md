title: Family Dinner
date: 2024-08-13 19:57
---

Did not have family dinner.  Chatted online.  Showered.  Listened to the radio.  Napped.  Watched TV.  Performed backups.  Today's agenda was to go to the store and have dinner with family.

## Morning

09:30.  Mom was still asleep.  Woke up, took shower, put on deodorant.  Said good morning to the internet chats.  Combed hair, got dressed.  Got a peach Bubly and took my meds.

10:00.  Turned on the radio to 98.9 The Rock.  Checked email.  Checked Mastodon.  Switched radio to KZHE.  Made tea.

Not having family dinner.  Uncle Joe's Boston terrier BJ fell from a high place and had to go to the vet.

11:30.  Ate a bowl of Frosted Flakes with soy milk.

Fell asleep.

## Afternoon

13:30.  Woke up.

14:00.  Discussed building a small NAS.  [David McMackins](https://mcmackins.org/) suggested getting a [small form factor PC](https://www.ebay.com/itm/166777303609?widget_ver=artemis&media=COPY).

Loaded mini fridge with Starry and Bubly.  Crushed up empty soda can boxes and put them in the trash.  Got some tea.

16:30.  Helped Uncle Jim take in groceries.  Didn't really want to: I don't use any of that stuff.  He got **a lot** of Pepsi.

## Evening

17:00.  Read an [article about Borg Backup](https://opensource.com/article/17/10/backing-your-machines-borg).  Watched Food Network.

18:00.  Ate vegan macaroni and cashew cheese ([recipe](https://www.drmcdougall.com/recipes/cheezy-baked-macaroni/)) with sriracha and green Cholula.  Ate medjool dates before and after dinner.  Watched _Chopped_ on Food Network.  Chatted on [SDF](https://sdf.org/).

19:00.  Thinking about getting a MacBook Air and running Fedora Asahi on it.  [Gnafu](https://themayhaks.com/~gideon/blog/) recommended HP EliteBook.

20:00.  Watched Food Network.  Published this post.  Backed up remote hosts.  Backed up laptop.

## Tomorrow

* Go to the store
