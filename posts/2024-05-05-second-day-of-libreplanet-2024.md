title: Second Day of LibrePlanet 2024
date: 2024-05-05 12:15
---
Second Day of LibrePlanet 2024.  Nothing else eventful.  Had Mexican food for dinner, with a margarita.

11:00.  Woke up.  Checked IRC.  Got up at noon.  Went downstairs.  Took pills.  Got a mango Bubly.  Watched LibrePlanet video streams.

13:44.  Checked email.  Checked web feeds.  Checked Gemini feeds.  Checked Mastodon.  Checked System Crafters forum.  Checked Free Software Foundation members forum.

14:00.  Ate four slices of bread with guac.  Ate a date and some walnuts.  Published article.

[Proprietary Software I Do Use](/csh/posts/proprietary-software-i-do-use/)

Fell asleep on the couch.

16:30.  Watched TV.

17:41.  Had a bean, spinach and mushroom enchilada with a side of Spanish rice.  Had a margarita.

18:25.  Logged into SDF Public Access UNIX System and got on COM.

19:30.  Got another margarita.  José Cuervo this time.

20:23.  Learned that there is a free software replaccement for programs like Folding@home, where you donate your idle computing resources to science projects.  It's called BOINC.

[BOINC](https://boinc.berkeley.edu/)

Rinsed mouth with mouthwash.

21:00.  Went upstairs to chat on IRC and go to bed.
