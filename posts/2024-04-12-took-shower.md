title: Dear Diary
date: 2024-04-12 10:48
---
Took shower. Went to Walmart to get groceries. Installed graphical Gemini client and several Go programs for Gemini-related stuff. Installed some window managers.

10:00. Got up, took shower, brushed my teeth, oiled my beard, combed and brushed my hair and beard, put on deodorant, got dressed. Checked email. Logged into Snikket. Started new diary entry in Emacs.

11:00. Made tea and oats. Installed ScanCode Toolkit.

12:00. Ate oats with maple apple butter and hemp seeds. Drank two cups of tea with soy milk.

Went to Walmart. Could not afford DayQuil, but got $80 USD worth of groceries. We're having tacos and burritos tonight. Below is my entire shopping list.

* tomatoes
* hamburger (small)
* sour cream
* cheese (shredded)
* tortillas (flour)
* salsa
* Pepsi
* Bubly (lime & strawberry)
* DayQuil
* refried beans (fat-free)
* guacamole
* Taco Bell sauce, hot & mild
* oats
* tomatillos, 10
* cilantro
* Country Crock Original margarine
* serrano pepper
* onions
* tomatoes, petite, 28 oz.

14:46. Returned home from Walmart. Had a Starbucks oatmilk frap on the way home. Mom tried Bubly Burst, a version of Bubly with sweetener added; she said it had a Diet Pepsi aftertaste, and preferred my Bubly with regular sugar added.

15:56. Finished watching the movie Nefarious, a movie about a demon possessing a man facing execution. It was a good film, albeit religiously biased toward Christianity, especially on the abortion issue. In Judaism, abortion is generally avoided, but if the fetus is a threat to the mother's health, it must be destroyed "limb by limb."

[What does Jewish law say about abortion? - The Forward](https://forward.com/news/501397/what-does-jewish-law-say-about-abortion/)

16:47. Wrote mail to MC about Gemini and self-hosting servers. Built and installed the graphical Gemini client Castor. Also insalled the Lagrange AppImage. I like Castor better due to its simplicity, but it does not support client certificates.

19:10. Tasted the homemade salsa verde. It's perfect! Mom forgot about the Spanish rice until I reminded her. I hope this isn't too much work for her. On the other hand, I really want homemade Spanish rice. Got a can of strawberry Bubly. Logged into SDF Public Access UNIX System to chat in COM, and tuned into aNONradio in VLC.

Discovered gemlog.sh, a nice utility for generating Gemini logs.

[gemlog.sh](https://git.sr.ht/~nytpu/gemlog.sh)

21:57. Ate three bean burritos with pico de gallo, guac, and salsa verde. Had some Spanish rice on the side. The Spanish rice was soggy. Finished my can of strawberry Bubly.

22:20. Rinsed mouth with Listerine. Went upstairs. Listened to aNONradio.

23:00. Installed simgal, i3 and cwm.

[simgal](https://github.com/mchackorg/simgal)

