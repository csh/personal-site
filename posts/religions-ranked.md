title: Religions, Ranked From Buddhist to Baptist
date: 2023-11-22 14:03
tags: religion, politics
---

Warning: This is a controversial post.

Someone once said, "I don't believe in tolerance.  I believe in
acceptance."  With that in mind, I will do my best to accept all, as
long as they accept others.  But some religions are just destructive
to the world, and must be replaced.

## Acceptable

Most religions are alright.  I'm sure your religion is on here.  If
not, ask me to add it; I probably forgot.

- Buddhism - Doesn't try to convert anyone.  Accepts science.
- Judaism - We don't try to convert anyone.
   Community-oriented. Accepts science.
- Unitarian Universalism - Gay-friendly.  Accepts science.
- Islam - One God.
- Sikhism - One God.
- Christian: Episcopal - Gay-friendly. Community-oriented.  Accepts science.
- Christian: Methodist - Gay-friendly. Community-oriented.  Accepts science.
- Christian: Mennonite - Humility and service.  Community-oriented.
- Christian: Amish - Humble.  Doesn't try to convert anyone.  Community-oriented.
- Christian: Latter-Day Saint - Community-oriented, respectful of other beliefs.
- Christian: Roman Catholic - Gay-friendly, community-oriented, accepts science.
- Christian: Lutheran - Notable Lutherans include [Donald Knuth][1],
  creater of **TeX**, a very sensible man.  Hopefully Lutherans don't subscribe to [Martin Luther's antisemitism][2].
- Christian: Eastern Orthodox - Community-oriented.
- Christian: Seventh-Day Adventist - Vegetarian. Respects the true Sabbath.
- Wicca - Gay-friendly.  Accepts science.
- Shinto - Doesn't try to convert anyone.
- Hinduism - Eh.

## Unacceptable

I thought Baptists were the worst, but this list doesn't actually end
with Baptists.  It turns out there are things much worse out there.
If your religion is on this list, please consider looking at the
religions above, for your sake and the sake of the people around you.

- [New Atheist](https://en.wikipedia.org/wiki/Messianic_Judaism) - Biblical literalism.  Anti-community.  Not humble.  Disrespects other beliefs.
- Christian: Pentecostal - Anti-gay.  Biblical literalism.  Anti-science.
- Christian: Salvation Army - Anti-gay.  Biblical literalism.  Anti-science.
- Christian: Christian Science - Anti-gay.  Anti-medicine.  Anti-science.
- Christian: Jehovah's Witness - Anti-gay. Anti-medicine.
- Christian: Baptist. Anti-gay.  Biblical literalism.  Anti-science.  Disrespects other faiths.
- Westboro Baptist Church - Need I say more?
- Scientology - Anti-science.  Anti-community.  Anti-medicine.
- [Messianic Christianity](https://en.wikipedia.org/wiki/Messianic_Judaism) - Lies about what religion they are.  Actively tries to convert Jews.  Anti-gay.  Anti-science.  Biblical literalism.
- Black Hebrew Israelite - Racist.  Anti-gay.  Biblical literalism.  Anti-science.  Disrespects other faiths.
- Radical Islam - Perverts the **Qur'an**.  Actively tries to convert everyone _by threat of death_.

[1]: https://web.archive.org/web/20060925022700/http://www.stanfordalumni.org/news/magazine/2006/mayjun/features/knuth.html
[2]: https://en.wikipedia.org/wiki/Martin_Luther#Posthumous_influence_within_Nazism
