title: Proprietary Software I Do Use
date: 2024-05-05 14:40
---
TV. I use Spectrum TV service. It functions by a nonfree DRM program installed on the nonfree Roku systems. I also have Jellyfin set up on them.

Router. The Spectrum router requires a nonfree mobile app to configure the router. I have no clue if you can call tech support and have them switch their provided router to Bridge Mode, to use a LibreCMC router instead. I don’t pay the internet bill, so I leave the router alone and don’t install my own.

Tax filing. And other work/government form submission systems.

Web banking. I live in a rural area without an ATM. I use the bank’s web program to manage my money. This also means I neglect the use of cash.

Signup Genius. My religious congregation uses this web program for event planning. I’ve tried just shooting the organizer an email, but she complains.

Meetup.com. I use this web program to find social groups in my area, to meet people. I have not benefited much from it; I think my local religious congregation has been more fruitful, to be honest, and they are accessible with free software via email.

Firmware. To avoid the Microsoft Tax, I bought a Chromebook after my ThinkPad X200 for my mom’s use, and put GNU/Linux on it. It has only one USB port and can only do Wi-Fi with a card that requires nonfree firmware. If I used a Wi-Fi dongle, it would use up the only USB port, and flash drives wouldn’t be able to be used. I decided installing the nonfree firmware was my punishment for being stupid and buying the Chromebook.

Mobile device. After not having one for probably five years, I caved to the pressure from family and got one after one of my respected free software friends got a Pixel with GrapheneOS. I regret it, because people automatically assume I will comply with their request to install such and such proprietary app on it. Before, I had an easy out: I could just say “I don’t have a mobile device. I can’t use that app.” I’m currently trying to phase out my use of the device, but it’s hard now that I use Cheogram.

Discord. My mobile device has Discord. I’m trying to quit. My main attraction is the group chat for a local LGBTQ social group. Since their events are coordinated by Meetup (nonfree JS) I am considering dropping Discord and using Meetup only, to minimize my us and discourage idle chatter online.

Zoom. I have stopped using Zoom only recently. I used it to attend my religious services, because I cannot afford gas to go in person. I’ve just been sucking it up and missing out on the community meetings with my congregation, until I can go in person. I explained the issue to one of the organizers about how I won’t use Zoom and why.

Doxy. My therapist counsels me over the nonfree web program Doxy. I could probably eliminate this from the list by going in person.
