title: First Night of Passover 2024
date: 2024-04-22 15:51
---
Got up at 12:30 in the afternoon. Went to Walmart and got dill, celery, pistachios, cashew milk, almond butter, tahini, and carrot juice. Had to go to Price Chopper for matzo, and it wasn't technically kosher for Passover. Got a bottle of margarita for my alcohol needs, since I can't have beer or whiskey. Checked mail on the way home. Came home and put up the groceries. Finished off the soy milk with a big bowl of Cheerios with maple syrup.

Checked email, Mastodon, web feeds, Gemini feeds.

[Volkswagen workers in Tennessee voted to unionize.](https://www.theguardian.com/us-news/2024/apr/19/volkswagen-unionize-uaw-tennessee) 

Checked out the System Crafters forum. Commented on the thread about Kanata. I'm still having issues getting Kanata to work. I set permissions but I still got errors. Decided to reboot to maybe fix the groups issue.

[Using Kanata to Remap Any Keyboard: Permissions](https://shom.dev/start/using-kanata-to-remap-any-keyboard/#permissions) 

Snacked on some pistachios. Discourse is really nice forum software.

20:30. Matzo ball soup is almost done. Chatted on IRC. Checked Fedora forum. Watched Bob's Burgers.

21:30. Had matzo ball soup with five matzo balls, and a big pickle spear. Took a swig of margarita. Rinsed my mouth with Listerine. Logged off.
