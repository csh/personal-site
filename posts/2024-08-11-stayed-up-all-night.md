title: Stayed Up All Night
date: 2024-08-11 22:00
---

Stayed up all night.  Harvested the garden.  Listened to the radio.  Watched TV.  Talked on the phone. Today's agenda was to harvest the garden.

## Morning

Couldn't go to sleep last night.  Laid in bed until 04:00.  Shouldn't have had that coffee.  On the bright side, I figured out a way of backing up:

* Keep regular files on an encrypted flash drive
* Back up home directory to a Borg repo on the flash drive
* Save a disk image of the flash drive to my main external hard drive

I thought about using `systemd-homed` for this, but I think I'll just do a regular encrypted file system.

09:45.  Woke up.  Got dressed.  Brushed hair.  Went downstairs.  Got a Starry.  Took pills.  Checked email and Snikket.  Checked Mastodon.

11:00.  Went out to the garden and got cucumbers and jalapenos.  Went inside, chatted on Snikket, and switched on Shortwave to listen to music.  Listening to 94.9 KCMO right now.

## Afternoon

12:00.  Got a bowl of Frosted Flakes with soy milk.  Made Earl Grey tea.  Rinsed travel mug with boiling water.  Installed Syncthing and configured it to start automatically.  Thinking about getting phone back out, to use it as a camera.

13:00.  Listening to 101 The Fox.

Fell asleep.

14:35.  Woke up.  Got some tea in my travel mug.

15:00.  Watched _Guy's Grocery Games_ on Food Network.

16:40.  Just had a very long (more than an hour long) call with Roey on Snikket.

## Evening

17:17.  Synced phone camera with laptop.  I have a lot of stupid photos to delete.

18:14.  Finished deleting and organizing photos.  Got a Starry.

19:00.  Finished tea.  Installed [TMSU](https://tmsu.org/).

20:00.  Ate a bowl of cashew macaroni with sriracha sauce.  Reheated another cup of tea in the microwave.

21:00.  Drank half a beer.  Someone sent me a Milton Friedman video in `#fsf`.  I know I'm supposed to love my fellow, but that man is just socially irresponsible.

22:00.  Went upstairs.

## Tomorrow

* Shower
* Harvest tomatoes
