title: Helped Uncle Use Amazon
date: 2024-12-18 20:45
---

Helped Stoner Uncle use Amazon.  Went to store.  Checked post office.
Wrote letters.

Pinched a nerve in my left arm due to lying on my left elbow 24/7.
Shoulder is numb.  Decided to sit at the dining room table with my
laptop, so I'm sitting properly, and maybe my arm can heal.

"Breakfast": Drank a can of Starry.

Went to Belton to go and help Stoner Uncle use Amazon on his Fedora
GNU/Linux laptop.  It took maybe fifteen minutes.  He got his candy
ordered.

Went to Hy-Vee while in town.  Got miso paste, Field Roast sausage egg
and cheese English muffins, cookie dough, sriracha, stir-fry sauce,
and peanut sauce.

Checked post office before heading home.  No mail whatsoever.

Lunch: Microwaved a Field Roast sausage egg and cheese English muffin.

Wrote letters.  Responded to Pennsylvania Dutch pen pal: His letter
was two whole pages, typed!  Responded to [Dallin
Crump](https://dallincrump.com/).  Addressed the envelopes and put a
stamp on them.

Set up some pillows on Mom's bed so I can sit up and not lie on my
arm.  I wish we had a proper couch to sit on, but it's covered in junk
thanks to Hoarder Uncle.

Dinner: Finished the other half of my sweet potato, steamed broccoli,
and [peanut dressing](https://www.drmcdougall.com/recipes/peanut-dressing/).

Dessert: Vegan Talenti gelato with hot apple pie.

Watched an Andrew Zimmern חנוכה (Hanukkah) special on Food Network.  I
can't wait for לאַטקעס (latkes) next week!  Not that I should be eating
them... I really need to clean up my diet.  I do not want to know what
my cholesterol levels are like, with all this fried and sugary garbage
I eat.

Chatted online for the rest of the night.