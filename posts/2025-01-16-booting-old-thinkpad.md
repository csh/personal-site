title: Booting the Old ThinkPad
date: 2025-01-16 16:08
---

Booting my laptop is a chore.  It takes a long time, so I limit my
reboots to once a day.  This can be counter-intuitive when you use
[GNU Guix](https://guix.gnu.org/).

I use a Lenovo ThinkPad X200 with [GNU
Boot](https://www.gnu.org/software/gnuboot/web/) as my primary
computer now.  I migrated away from the HP "Blooglet" Chromebook
because it lacks RAM, disk space, a good keyboard, and the ability to
run a [fully free OS](https://www.gnu.org/distros/free-distros.html).

## Boot Procedure

Currently, my boot procedure is as follows.

1. Press power button.
2. Press `b` to use a regular BIOS.  (Guix System does not work with GNU Boot's default GRUB option.)
3. Type passphrase.
4. Select boot option.
5. Type passphrase.
6. Log in.

This can be an issue when running Guix System, as making even trivial
system changes, such as adding a user, may require a reboot.
Supposedly this is remedied by the new `reboot --kexec` command, but
I'm not sure if it will work with my encrypted disk.

## Ideal Boot Procedure

On Fedora, I had a much simpler boot procedure.  I wish I could get
this on Guix System on GNU Boot.

1. Press power button.
2. Type passphrase.
3. Log in.
