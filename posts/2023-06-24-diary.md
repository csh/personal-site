title: Dear Diary
date: 2023-06-24 00:00
---

I got up at around 8 a.m. I got a cup of water and took my pills. I
packed hummus, bread and a can of Pepsi. Mom and I got in the car.

We went to Habitat for Humanity ReStore to get another can of gray
paint for my room. I stayed in the car and messaged on my mobile
device.

We picked up my mom's boyfriend Mike. We hit the road and set off for
the house in East Lynne, MO.

Mike nearly finished painting my room. It looks nice. The back wall is
a dark blue, and the other walls are white. My room has a staircase
going to it. It is very quiet up there; I can't hear anything
downstairs. It's nice.

I swept the floor in my room and on the staircase. I collected the
Arctic Air Pure Chill so we could take it back to Walmart and get our
money back.

Mike at a ham sandwich and we left the house.

We went to Walmart. Mom returned the Arctic Air Pure Chill and got me
a bottle of cold water. I stayed in the car. Mike went in to use the
bathroom.

We dropped off Mike at his house and went home.

I took a nap on the couch.

I got on Bender, my Libreboot X200T, and ran `sudo guix system
reconfigure null.scm`. The system built, and `rde` greeted me, but
there was no internet connection and no `nmtui` to get one. I had to
roll back to my GNOME system config. [Adding the networking feature](https://codeberg.org/csh/null/commit/61e4ec5242d8a8500f2ff4fac0655b1265397b3d)
and reconfiguring again fixed the issue, but I still cannot figure out
how to add Sway.

5:15 p.m. I ate my lemon garlic hummus with bread, and drank a cup of
water.

I chatted on `#gaygeeks` on Libera.Chat while watching _All in the
Family_.
