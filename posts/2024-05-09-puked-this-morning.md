title: Puked This Morning
date: 2024-05-09 13:54
---
Puked this morning.

10:00.  Woke up nauseous.  Got dressed.  Went downstairs.

I puked.  Still felt queasy afterwards.

Rinsed mouth with mouthwash.  Brushed hair.  Took pills.  Drank a cup of water.  Put on shoes and hat.

Got in the car.  Had to turn around and get Tums.  Went to Family Services and scheduled an appointment for tomorrow, because their wait time was two and a half hours.

Came home.  Still nauseous.  Took a nap.  Woke up feeling better.  Got on laptop Hermes.

14:00.  Checked email.  Checked web feeds.  Checked Gemini feeds.  Checked Mastodon.  Checked FSF member forum.  Checked System Crafters forum.  Checked Trisquel GNU/Linux forum.  Logged into Snikket.

[New techniques read ancient scrolls](https://stallman.org/archives/2024-mar-jun.html#8_May_2024_(New_techniques_read_ancient_scrolls))

[Distributed System Daemons: More Than a Twinkle in Goblins' Eye](https://spritely.institute/news/spritely-nlnet-grants-december-2023.html)

15:00.  Ate a bowl of Cheerios with soy milk.

Took a nap.

18:30.  Got up.  Took out trash.  Got another can of Bubly.  Turned on air conditioner upstairs.

19:30.  Ate two pieces of pizza with tahini and crushed red pepper.  Finished pizza.

20:24.  Emailed Richard Stallman about the competition between Emacs Lisp and Guile, and how everything on my system is in Guile except Emacs.  Asked him if he could get people to revive Guile Emacs.  Also asked him why he likes Scheme and not Common Lisp.

Drank half a beer.

21:00.  Went upstairs.
