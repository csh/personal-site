title: Chromebook Guix Now Builds
date: 2024-04-29 14:57
---
Went to pharmacy and Family Services.  Compass Health changed my case worker.  Chromebook Guix now builds.  Enjoyed lentils and bread for dinner.

08:15.  Got up to my alarm.  Got dressed, went downstairs, took pills.  Went to the pharmacy to get my uncle's prescription.  Came back home to take the dogs out.  Went to Family Services to fix my food stamps.  Came home.  Ate Cheerios with cashew milk.  Tried building system.scm on Miller.  Reinstalled Guix package manager on Farnsworth and did "guix pull".  Took a nap.

12:00.  Was woken up.  Got a phone call from Compass Health.  My case worker has been changed to someone else, and he doesn't give rides to medical appointments.  I meet him on the same day as my psychiatry appointment, on May 21st.  Went back to sleep.

14:45.  Got up, went downstairs, got a lime Bubly, and got on laptop Hermes.  Checked email.  Checked System Crafters forum.  Rebooted Farnsworth.  Posted reply to Chromebook issue on System Crafters forum.  Checked Mastodon.  Ran "guix pull" on Farnsworth again, in case it went offline in the middle of pulling.  Checked web feeds. Checked Gemini feeds.  Checked Trisquel forum.

[Free Your Software, Free Yourself](https://jxself.org/free-yourself.shtml)

[Unawareness of injustice of "modern digital society"](https://stallman.org/archives/2024-jan-apr.html#28_April_2024_(Unawareness_of_injustice_of_modern_digital_society))

[How Google decided to enshittify search](https://stallman.org/archives/2024-jan-apr.html#28_April_2024_(How_Google_decided_to_enshittify_search))

[Installing Debian without non-free firmware](https://trisquel.info/en/forum/installing-debian-without-non-free-firmware) 

15:52.  Logged into Snikket.  Logged into SDF Public Access UNIX System.  Ate two medjool dates.  Got another can of lime Bubly.  Had another medjool date and a matzo with almond butter.  Found out the almond butter has palm oil in it!  Threw the rest of it away; it was almost empty, anyhow.

17:00.  Got a blackberry Bubly.  Having lentils for dinner.  Took a nap.

[Breakfast Lentils](https://www.mondaycampaigns.org/meatless-monday/recipes/breakfast-lentils)

19:00.  Ate four slices of bread with tahini and lentils for dinner.  Mom put hoisin sauce in the lentils.  Luckily it was only a small batch and I ate most of it.  Watched The Ugliest House in America on HGTV.

20:00.  Got a response to my Chromebook issue on System Crafters forum.  I made the changes to my code and will build it on Farnsworth or Miller tonight.  Watched My Lottery Dream Home on HGTV.

20:40.  Went upstairs to chat on IRC and test my Chromebook Guix code.
