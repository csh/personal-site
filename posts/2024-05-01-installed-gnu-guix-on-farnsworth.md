title: Installed GNU Guix on Farnsworth
date: 2024-05-01 12:34
---
Went to Walmart.  Installed GNU Guix on Farnsworth.  Had spaghetti for dinner.

10:00.  Woke up.  Got dressed.  Took pills.  Brushed hair.

Went to Walmart with Mom and aunt.  Got cilantro, green onion, lentils, minced garlic, lemon juice, mushrooms, whole wheat spaghetti, pasta sauce, bell pepper, Asian salad mix, fat-free Asian salad dressing, walnuts, mango Bubly, apple butter, and soy sauce.

12:00.  Returned home.  Put up groceries.  Ate a big bowl of Cheerios with soy milk and honey.  Got on laptop Hermes.  Started a new blog post.  Logged into Snikket.

13:00.  Checked email.  Reinstalled GNU Guix on Trisquel GNU/Linux on Farnsworth.  Ran "guix pull" on Farnsworth.  Checked System Crafters forum.  Checked Mastodon.  Checked web feeds.  Checked Gemini feeds.

14:00.  Took a nap.  It's too hot in my room.  I should have turned on my air conditioner.

16:00.  Woke up.  I didn't plan on sleeping for two hours.  Downed my can of blackberry Bubly.  Turned on my air conditioner.  Chatted on IRC.

18:27.  Hungry.  Ate pistachios and a medjool date while waiting on pasta.  Got a lime Bubly.  Connected laptop Hermes to charger.

19:00.  Ate whole wheat pasta with pasta sauce, mushrooms, tomato, green bell pepper, and green olives.  Ate a can of mandarin oranges.

20:00.  Today I learned Ireland gave blood donors a free pint of Guinness up to the year 2009.

[Book Review: Doctors and Distillers by Camper English](https://www.nytimes.com/2022/07/07/books/review/doctors-and-distillers-camper-english.html)

21:00.  Went upstairs to enjoy the AC and maybe hack on Guix or Guile.
