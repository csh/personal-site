title: שבת שלום
date: 2024-08-23 11:20
---

Got call from Wellbeing Center.  Chatted online.  Went to Walmart.  Called friends.  Listened to the radio.  Watched TV.  Fixed Sabbath candles.  Performed backups.

Today's goals were to brush teeth, shower, call the Wellbeing Center, and fix Sabbath candles.

## Morning

10:50.  Got up.  Checked [Snikket](https://snikket.org/) on phone.  Got dressed.  Combed hair.  Went downstairs.  Got a Starry and took pills.  Got on laptop and checked Mastodon and email.

11:42.  Received phone call from Wellbeing Center.  I have to be ready by 07:00 Monday.  I guess I don't need to call them.

## Afternoon

12:00.  Got Cheerios and soy milk.  Finished off the soy milk with a cup of chocolate soy milk.  Reheated the rest of my tea and put it in my travel mug.

13:53.  Came back home from convenience store and Walmart.  Brought in groceries.  Got Mom's cigarettes, my wine, Cheerios, Raisin Bran, Starry, Bubly, and bread.

14:30.  Copied my calendar to my little [Rhodia notepad](https://www.rhodiapads.com/collections_orange_12.php).  Tuned into [KZHE](https://kzhe.com/).

15:21.  Watched TV.

16:00.  Ate three medjool dates with chocolate peanut butter.  Ate half a can of plain Pringles.

## Evening

17:00.  Got Sabbath candles ready.  Removed cork from wine bottle.

18:00.  [Local man needs help](https://www.gofundme.com/f/donate-to-help-joe-find-a-safe-new-home) getting a new home.  Heated up Amy's Chinese Noodles & Veggies.  Ate two.

19:00.  Went upstairs to talk to friend on the phone.

20:30.  Hung up the phone.  Came back downstairs.  Finished glass of wine.

21:35.  Researched plane and train tickets to Washington, D.C.

22:00.  Performed backups.  Went upstairs.

## Tomorrow

* Read weekly תורה (Torah) portion