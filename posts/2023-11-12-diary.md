title: Dear Diary
date: 2023-11-12 16:11
---

Got up at 11:30 a.m.

Took my Prozac. Took my Abilify. Drank a tall cup of water.

Ate a bowl of beans with cornbread. "JIFFY" makes a good [mix][5] that
is vegetarian and vegan. We use soy milk for the milk and flax seeds
for the egg.

Tried installing [Guix][1] twice using the [guide][2] from System
Crafters. I didn't want to use the nonfree kernel, but I get an
[error][3] when I try to install normally. I tried SSHing into the
installr last night, but that did not work; I still have to use my
terrible TV as a monitor, with its high [overscan][4] rate.

Mom watched her ghost shows. Mike hurt really bad and lied down for a
while.

Smoked a quarter of a joint with Mike.

Took a nap and woke up at 19:00.

Ate two pieces of warm cornbread.

Watched _All In The Family_.

Checked Guix install. Substitute died. `podiki` says to redo the
install. I'm tired of doing this install over and over. Luckily, he
says all I hae to do is redo the `system init` command.  I restarted
it. We'll see how it turns out.

Roey didn't have a good day. He is still flooding his head with Israel
news.

20:00. Mike and I smoked a joint.  Mike went to bed.  He has a fever
and is not feeling well at all.

20:22. I went to my room. I'm going to check on the install and maybe
do it over one more time if it fails again.

[1]: https://guix.gnu.org/
[2]: https://systemcrafters.net/craft-your-system-with-guix/full-system-install/
[3]: https://issues.guix.gnu.org/66786
[4]: https://www.howtogeek.com/252193/hdtv-overscan-what-it-is-and-why-you-should-probably-turn-it-off/
[5]: https://www.jiffymix.com/products/vegetarian-corn-muffin-mix/
