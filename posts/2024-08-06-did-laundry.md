title: Did Laundry
date: 2024-08-06 22:33
---

Took a shower.  Did laundry.  Chatted online.  Listened to music.  Watched TV.  Today's agenda was to shower, do laundry, and check the post office.

## Morning

05:00.  Went on a Wikipedia binge about Norwegian language.  Went back to bed because laptop was dead.

10:30.  Woke up.  Put new socks in sock drawer and discarded the plastic wrapper.  Went downstairs, got a Bubly sparkling water, put my laptop on the charger, brushed my hair, and took my pills.  Went back upstairs and gathered up dirty clothes and put them in the washer downstairs.  Took a shower.

11:35.  Got on laptop.  Checked [Snikket](https://snikket.org/).  Checked email.  Checked Mastodon.

## Afternoon

12:00.  Ate Cinnamon Toast Crunch with soy milk.

12:23.  Put laundry in the dryer.

Fell asleep.

14:00.  Woke up.  Collected laundry.  Chatted on Snikket.

15:30.  Went upstairs.  Got on the Free Software Foundation Mumble server.  No public activity happening.  Listened to KQRC 98.9 The Rock.

## Evening

17:00.  Created new user account on laptop.  Went downstairs.  Tuned into south Arkansas classic country station [KZHE](https://kzhe.com/).  Finished my beer from last night and opened a new (Shiner Bock) beer.

18:00.  Ate three Amy's bean burritos with Taco Bell Hot Sauce, and some coffee-flavored Oreos.

[Gnafu the Great](https://themayhaks.com/~gideon/blog/) showed me the free software-friendly [ODROID](https://www.hardkernel.com/shop/odroid-go-ultra-clear-white/) gaming system which runs the [ROCKNIX](https://rocknix.org/) operating system.

19:00.  Added another episode of _Futurama_ to the Jellyfin server.  Got the movie _Big Eden_ (2000) at the recommendation of someone in the gay chat.  Tuned to 98.9 The Rock because KZHE was lagging.

20:21.  Watched _Beat Bobby Flay_.  Heard Kamala Harris has chosen Tim Walz as running mate.  Chat says LBJ was the last president to have to deal with a vice president from the opposing party, and Nixon was the first to choose a running mate.

20:50.  Got a Starry.

21:41.  Copied _Big Eden_ to the Jellyfin server.  Mom got chicken noodle soup.  I got more plastic spoons from Aunt Sue's room and put them in the silverware drawer.

22:00.  Watched _MASH_ on Jellyfin.  Today I learned the guy who played Winchester was gay.

22:30.  Went upstairs.  Chatted.

## Tomorrow

* Post office
* Change insurance
* Sign documents

