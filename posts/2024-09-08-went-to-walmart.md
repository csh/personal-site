title: Went to Walmart
date: 2024-09-08 20:43
---

Went to Walmart.  Watched TV.  Chatted online.  Called friend.

## Morning

Got dressed.  Took pills.  Made tea and oatmeal.

Ate oatmeal with pumpkin butter.  Drank tea.

## Afternoon

Went to Walmart.  Got cilantro, green onion, cow's milk (for family), Bubly sparkling water, pumpkin butter, vegan pizza, vegan breakfast sandwich, and Starbucks frap and Pink Drink.

Went to Price Chopper.  Got rice noodles, Thai peanut sauce, and Velveeta cheese (for family).

Drank frap and Pink Drink on the way home.

Went to post office.  No new mail.

Came home.  Fell asleep for half an hour.  Dreamed about having a new house with a fancy toilet with a bidet.

## Evening

Chatted online.  Ate vegan supreme pizza with vegan Hidden Valley ranch dressing.

Called friend and talked for an hour.

Drank a Guinness beer.  Watched _MASH_.

Got a message from the Wellbeing Center.  They'll be giving me a ride tomorrow.  I'm nervous; I did not expect this.  They said I had to wait until Wednesday.

Took a shower.

## Tomorrow

* Laundry
* Shower
* Wellbeing Center
