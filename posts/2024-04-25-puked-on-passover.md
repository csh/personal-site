title: Puked On Passover
date: 2024-04-25 09:31
---
It was a dark, cloudy, rainy day. Nothing much going on. Puked in the middle of the night. Thought about installing GNU Guix System onto Miller, the Chromebook. Mom's room has leaks. Worked on ircforever.org.

Puked at 2 in the morning. Didn't make it to the sink. Puked all over the kitchen floor. Felt a lot better after that.

09:00. Got up. Took my pills. Had a glass of Sunny-D.

Got on laptop. Logged into Snikket. Logged into SDF Public Access UNIX System. Checked web feeds. Checked Gemini feeds. Checked Mastodon. Checked System Crafters forum. Checked Trisquel forum. Took a nap.

12:25. Woke up.

12:41. Aunt came home with Tums. I took two. Maybe now I can eat something. Got another glass of Sunny-D.

13:19. Got a cup of tea with cashew milk. Downloaded nonfree Guix ISO for Miller the Chromebook. Wrote installer to flash drive.

16:00. Finally ate something: Salted baked potato slices with ketchup. They're delicious if you soak them in salt water before baking! Had another cup of tea.

Today I learned that Microsoft has released MS-DOS 4.0 as free software. It's a little disingenuous to release something just because it's useless, but it may have some benefits to retrocomputing hobbyists. Maybe even FreeDOS will have some benefit from it.

[Microsoft Frees MS-DOS 4.0](https://cloudblogs.microsoft.com/opensource/2024/04/25/open-sourcing-ms-dos-4-0/) 

17:22. There are water leaks in my mom's room! I'm getting dripped on. A drop even got on my laptop. Checked email, cleared inbox. Marked off the day on the calendar.

19:30. Logged into ircforever.org. Connected to IRC with WeeChat and registered my account Kolev with the nicks Kolev, Kol3v and K0lev. Made some a shell function to edit a file as root in the specified way, by making a backup first in the specified format. It complements my "e" alias, which opens "$VISUAL". I created a "todo.txt" file of the things I need to do tomorrow, when I log in again.

```
# Edit file as root
E() { 
        doas cp $1 $1.`date "+%Y%m%d%S"` # Back up file
        doas $VISUAL $1
}
```

Checked personal email. The company MFJ is going out of business! I just got my antenna from them maybe a year or so ago! Discussed mail workflow with someone; they thought it odd that I'd put all important mail into Archive and delete everything else, then go back to Archive to read the important mail.

21:00. My laptop keeps going offline! WTF! At least I know nmtui works... I'm going to log off, go upstairs, and set up GNU Guix System on host Miller, the Chromebook.
