title: Slept Too Much
date: 2024-08-07 22:16
---

Slept too much.  Signed documents to change insurance.  Went to post office.  Watched TV.  Mild anxiety/boredom.  Today's agenda was to sign documents and go to the post office.

## Morning

11:00.  Woke up.  Got on laptop.  Friend from Norway said I sleep too much.  I agree!  I can't believe I slept this long!  Mom got up not too long before me.  Got fully dressed: Dress shirt, pants, yarmulke, shoes and socks.  Combed my hair.  Went downstairs.  Got a orange cream Bubly sparkling water, took my meds.

11:30.  Plugged laptop into charger.  Mom started making macaroni and cheese.  Took out the trash.  Went outside and sat with the laptop.  It's a little hard to see the laptop screen outside.  Saw a huge grasshopper.

## Afternoon

12:25.  Went inside and got some Cinnamon Toast Crunch with soy milk.  Put two cans of Starry in Mom's mini fridge.  Signed documents.

13:00.  KZHE was playing too sappy songs, so I changed it to 101 The Fox.  Lots of ads.  Switched to a few different channels.  Streams have stopped working.  Restarted Shortwave and it works now.

13:25.  Got a Starry.  Sean wanted to use my computer to run TurboTax; my friends recommended we use [Open Tax Solver](https://opentaxsolver.sourceforge.net/) which does not support Kansas or Missouri, and is probably all sorts of jank.

14:30.  Checked Mastodon.  Checked email.  Got in the car, turned on the Wi-Fi hotspot, and got back on my laptop.  Dropped off the documents in the mail and then checked the PO box.  Took Uncle Jim to the bank.

Got home.  Opened letter containing my printed-out QR code.  Cut out the QR code and taped it to my notepad.  It fit perfectly!

16:17.  Uncle Jim took the car keys to get gas for the lawn mower.  Tuned into south Arkansas classic country station KZHE.  Got a few coffee Oreos.

## Evening

17:00.  Ate Manwich lentils on whole wheat hotdog buns, and a few more Oreos, at the stove (because there is no table to eat at).  Reading about Norwegian dialects again.  It's such a mess.

17:30.  Watched _Guy's Grocery Games_ on Food Network on TV.

18:00.  Jim finally stepped out of the room.  First he had a phone call to make with my mom's phone, then he got stuck watching the TV.  (Uncle Jim can't for the life of him keep a phone.  He always breaks or loses it somehow.)  Got my last Shiner Bock beer.

19:00.  Bored.  Anxiety up.  My anxiety always seems to go up when I'm bored.  I don't like the lack of stimulation.

20:00.  Watching a home improvement show on HGTV.  Joined my friend's IRC server.

21:00.  Watched _MASH_.

22:00.  Rinsed mouth with Listerine.  Went upstairs.  Chatted until I fell asleep.
