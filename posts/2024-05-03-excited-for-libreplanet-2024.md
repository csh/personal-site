title: Excited for LibrePlanet 2024
date: 2024-05-03 12:24
---
Went to Uncle John's.  Got pizza.  Watched TV.  Excited for LibrePlanet 2024.

11:30.  Woke up.  Got dressed.  Went downstairs.  Got a can of blackberry Bubly.  Took pills.  Brushed hair.  We're having pizza for lunch.  Logged into Snikket.

12:48.  Checked email.  Fell asleep.

14:00.  Ate almost half a vegan Papa Murphey's Gourmet Vegetarian pizza.  Got a mango Bubly.  Went to Uncle John's.

Did errands for John: Submitted bills; got prescriptions; went to post office; got cow's milk.  Drank a Monster Energy drink.  Postponing shoe shopping to tomorrow.

17:13.  Came home.  Watched Guy Fieri's Diners, Drive-Ins and Dives.  Watched My Lottery Dream Home.

18:21.  Split hairs about the user freedom in the computer hardware field in Copyleftistas on Snikket.

19:00.  Checked Mastodon.  Checked System Crafters forum.  Checked web feeds.  Checked Gemini feeds.  Sent email to Chris "lispmacs" Howard.

[Chris Howard](gemini://gem.librehacker.com/)

[Urgent: Working people need Congress](https://stallman.org/archives/2024-mar-jun.html#1_May_2024_(Urgent%3A_Working_people_need_Congress))

[Feds reconsidering cannabis](https://stallman.org/archives/2024-mar-jun.html#1_May_2024_(Feds_reconsidering_cannabis))

20:00.  Watched more My Lottery Dream Home.  Chatted on #libreplanet on Libera Chat IRC.  Installed Olivetti Mode ("elpa-olivetti" in Trisquel) on laptop Hermes.  Subscribed to Evan Boehs's blog.

[Evan Boehs blog (RSS)](https://boehs.org/in/blog.xml)

21:00.  Ate a few slices of pizza with tahini and red pepper flakes on top.  Got another Bubly.  Rinsed my mouth out with the last of the Listerine.  Mom can't handle the smell of the Listerine; I'm thinking of getting Cepacol or Oxyfresh instead.

21:45.  Went upstairs to chat on System Crafters IRC.

