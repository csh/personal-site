title: No Tech Day
date: 2024-09-07 21:13
---

Avoided tech for twenty hours.  Read the weekly תורה portion.  Read Jewish books.  Napped.  Chatted online.  Watched TV.  Watched a movie.

Today's goals were to get some reading done and avoid writing or using technology until the evening.

## Morning

09:00.  Got up.  Got dressed.  Put on my שבת vest.  Went downstairs.  Brushed teeth.  Put the kettle on and made oatmeal with pumpkin butter.  Had a cup of jasmine tea.  Ate my oatmeal on the front porch.  The weather was beautiful.

Read the weekly תורה (Torah) portion.  Read books.

* _Schwetz mol Deitsch: An Introductory Pennsylvania Dutch Course_
* _Gates of Shabbat: A Guide to Observing Shabbat_
* _A Day Apart: Shabbat at Home_

## Afternoon

Read another book, _The Lady Farmer Guide to Slow Living_.  My only complaint about the book is that the author gives credence to Sally Fallon of the [Weston A. Price Foundation](https://en.wikipedia.org/wiki/Weston_A._Price_Foundation).

Took a nap.

Watched Mom research things to do next Saturday.

16:00.  Gave up on the no tech שבת (Sabbath).  Turned on my mobile device and laptop.  My Snikket server would not connect until I rebooted my device again.  Greeted my friends online.  Checked email.  Checked Mastodon and posted about my שבת experience.  Also posted about my [home rules](https://bluehome.net/csh/blog/2024/09/06/home-rules.html) fantasy.

## Evening

17:00.  Watched _Diners, Drive-Ins and Dives_ on TV.  Signed into [SDF](https://sdf.org/) COM chat.

18:30.  Ate Amy's tofu scramble with Field Roast breakfast sandwich.

19:00.  I get my food stamps on ראש השנה (_Rosh HaShana_, Jewish New Year) so I'll be able to go to Pan-Asia Market to get more tea.

20:00.  Watched the movie _A Time to Kill_.

21:00.  Watched _MASH_.