title: Install G-Golf on Fedora 38
date: 2023-06-14 00:00
tags: guile, fedora, gnome
---

Installing GNU [G-Golf](https://www.gnu.org/software/g-golf/) on Fedora is a little tricky, since you have to
do the installation manually. This article explains how to do it.

## Packages

Install the following packages.

    guile30
    autoconf
    automake
    guile30-devel
    glib
    gobject-introspection
    gobject-introspection-devel
    gtk4
    gnulib-devel
    cairo-devel
    
## Install Guile-Lib

Force the use of Guile 3.0.

    sudo ln -s /usr/bin/guile3.0 /usr/bin/guile
    sudo ln -s /usr/bin/guild3.0 /usr/bin/guild
    sudo ln -s /bin/guile-config3.0 /bin/guile-config
    sudo ln -s /bin/guile-snarf3.0 /bin/guile-snarf
    sudo ln -s /usr/bin/guile-tools3.0 /usr/bin/guile-tools

Download and unzip Guile-Lib.

    wget https://download.savannah.nongnu.org/releases/guile-lib/guile-lib-0.2.7.tar.gz
    tar -xvf guile-lib-0.2.7.tar.gz

Go into the source repository and build it.

    cd guile-lib-0.2.7
    GUILE=/usr/bin/guile ./configure --with-guile-site=yes --prefix=/usr
    make
    sudo make install

## Install G-Golf

Download G-Golf.

    git clone git://git.savannah.gnu.org/g-golf.git

Go into the source repository and build it.

    cd g-golf
    ./autogen.sh
    GUILE=/usr/bin/guile ./configure --prefix=/usr --with-guile-site=yes

Error..

> configure: error: Package requirements (guile-lib-1.0 >= 0.2.5) were not met:
>
> Package 'guile-lib-1.0', required by 'virtual:world', not found
> 
> Consider adjusting the PKG_CONFIG_PATH environment variable if you
installed software in a non-standard prefix.
> 
> Alternatively, you may set the environment variables GUILE_LIB_CFLAGS
> and GUILE_LIB_LIBS to avoid the need to call pkg-config.
> See the pkg-config man page for more details.

    make
    sudo make install 
