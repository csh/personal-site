title: Dear Diary
date: 2024-04-14 11:10
tags: diary
---
Subscribed to kiln mailing list. Got pizza. Discussed Judaism with Mom.

11:11. Got up a little before 11. Took my pills. Finished my lime Bubly from last night. Finished my leftover tea. Checked email. Got a response from Ian M. Jones about my site root issue.

> Put all your content under content/csh/, take "/csh" off of your config's url paths, set any URLs that point to / to /csh/, and just use relative URLs elsewhere. That might work.

Had a wonderful visit to the bathroom. Subscribed to the kiln mailing list. Sent an email asking the same question I posed to Ian M. Jones.

[kiln-discuss: Change site root](https://lists.sr.ht/~adnano/kiln-discuss/%3C78f7ed43-7830-4299-9fba-c91ac29993f1@bluehome.net%3E)

Checked Mastodon. Mom ordered pizza. Went to Walmart to get DayQuil and bottled water. Went to Papa Murphey's to pick up my pizza. The store had a sign that had an overtly Christian message: "He is risen." The clerk preached to us about God's Providence and how we need to trust in Him that He will provide. Mom got disgruntled at the preaching. I told her Judaism teaches that if the poor can give pennies, they should give pennies, but if giving means they can't eat, then they must not give. On the ride home, we talked about the Heartland Baptist Church and how it was money-hungry, trying to squeaze money out of my poor family. I also learned that their school is unaccredited, and my childhood friend Andrew was pulled out of it because of that. We also talked about how Judaism teaches that one should not just be complacent in everything, that we are partners in God's Creation and must participate by tilling the soil, helping the poor, and healing the sick. You can't just expect God to provide by just doing nothing.

13:00. Returned home. Brought in the water. Mom put pizza in the oven.

Found out how to view Imgur links.

```
feh https://i.imgur.com/kOm3JXJ.jpeg
```

VLC works for Imgur videos.

15:00. Restored website to the Haunt version.

16:11. Watching Beat Bobby Flay on Food Network. One of the judges is vegan, so the competitors have to make their dishes vegan! Someone responded to my post on the kiln mailing list.

