title: Internet Filters
date: 2024-08-25 11:20
---

Showered.  Chatted online.  Played with browser configuration.  Researched Hasidic Jews and internet filters.  Washed body pillow.  Dried body pillow.  Watched TV.

Today's goal was to shower, wash body pillow, and dry body pillow.

## Morning

07:30.  Got up, went downstairs, took my pills, took a shower.

08:30.  Checked [Snikket](https://snikket.org/).  Made my [list of approved sites](https://bluehome.net/csh/leechblock.txt) for the browser extension LeechBlock NG.  Played with [my Curlie links page](https://curlie.org/public/page?user=ke0vvt).

09:30.  Checked email.  

11:00.  Read the [חדר (cheder) student handbook](https://chedermonsey.org/media/pdf/1243/ubWI12430496.pdf) and the [parent handbook](https://chedermonsey.org/media/pdf/1243/eFwV12430494.pdf).  Washed body pillow.  Put Starry and Bubly in the fridge.

## Afternoon

12:00.  Made tea.  Looked into [Pi-hole](https://pi-hole.net/) but it seems complicated to set up, especially on [GNU Guix](https://guix.gnu.org/).

13:00.  Discovered [Steven Black's `hosts` file project](https://github.com/StevenBlack/hosts).  Blocked porn, gambling, fake news, and social media.

Fell asleep in the chair in the living room.

15:35.  Woke up.

16:00.  Read חב״ד's (Chabad's) ["Instructions for Barbers"](https://www.chabad.org/library/article_cdo/aid/4537683/jewish/instructions-for-Barbers-PDF.htm). 

## Evening

17:00.  Watched TV.

18:00.  Reheated rice and miso soup.  Ate rice with rice seasoning and pickle slices.

20:00.  Finished beer.  Performed backup.  Shut off computer.

## Tomorrow

* Go to Wellbeing Center