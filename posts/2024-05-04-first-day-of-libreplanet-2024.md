title: First Day of LibrePlanet 2024
date: 2024-05-04 14:50
---
First day of LibrePlanet 2024.  Went to Walmart.  Got new shoes.

07:30.  Checked IRC: LibrePlanet folks were ready and waiting for the conference to start.  I got up, got dressed, went downstairs, took my meds, took my vitamin B12, and finished a mango Bubly.

Went to Walmart and got a bunch of food for tomorrow, and some new shoes, and some mouthwash, and some premixed José Cuervo margarita.  Drank a Monster on the way home.  Brought in and put up the groceries.  Ate two pieces of pizza with tahini and crushed red pepper.

15:00.  Watched LibrePlanet on laptop and chatted on IRC.  Found some other fellow amateur radio operators.  Checked email.  Got my Free Software Foundation member cloak.  Tried to access Free Software Foundation associate member forum, but was denied.  Finally got in after deleting cookies.  Made several posts.  Had a margarita.

17:30.  Checked System Crafters forum.  Checked web feeds.  Checked Gemini feeds.  Checked Mastodon.

[Columbia U. protesters vile statements](https://stallman.org/archives/2024-mar-jun.html#3_May_2024_(Columbia_U._protesters_vile_statements))

18:30.  Watched Madea's Family Reunion.  Watched A Madea Family Funeral.

20:00.  Ate spaghetti.  Drank (and finished) the carrot juice.  Ate (and finished) mango sorbet.  Got a mango Bubly.  Rinsed my mouth with Parodontax.

21:00.  Getting tired.  Might head to bed soon.

21:25.  Went upstairs to get ready for bed.

