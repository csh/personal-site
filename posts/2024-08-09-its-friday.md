title: It's Friday!
date: 2024-08-09 20:05
---

Listened to the radio.  Chatted online.  Slept too much.  Watched TV.  Lit שַׁבָּת (Sabbath) candles.
Today's agenda was to do some tidying and light שַׁבָּת candles.

## Morning

08:14.  Woke up to alarm.  Checked chats.  Checked email.  Downloaded _The Brave Little Toaster_ from [Zerock](https://mcmackins.org/)'s Nextcloud and added it to my Jellyfin.

Fell asleep.

11:26.  Woke up.  Got dressed.  Went downstairs.  Got a Bubly.  Took pills.  Tuned in to classic country station KZHE.  Checked email.

## Afternoon

12:00.  Checked Mastodon.  Ate Cinnamon Toast Crunch with soy milk.  Took the trash out.

13:00.  Uncle Jim still isn't back with the car. Discussed WebRTC vs. Mumble on Mastodon.

13:40.  Uncle Jim returned with the car.  I'm feeling very restless.  I wish I could get outside.

14:00.  Found a free software dating app called [Alovoa](https://alovoa.com/).  Not many people on it.  I hate dating apps, anyway.

15:00.  Finished the coffee Oreos.

Fell asleep.

## Evening

17:00.  Woke up.  Checked IRC.  Tuned into KZHE with my friend from Norway.  Shut off the radio and watched _My Lottery Dream Home_ on HGTV on Spectrum TV.  Put new candles in the שַׁבָּת candlesticks.  Restocked the mini fridge with Starry and Bubly.  Rinsed out wine glass.

18:00.  Got an orange cream Bubly.

19:00.  Ate tater tots with ketchup and Gardein F'sh Filets with malt vinegar.

20:00.  Lit שַׁבָּת candles.  Drank a glass of wine.  Drank a glass of wine diluted with peach Bubly sparkling water.

21:00.  Watched _MASH_.  Mom put out שַׁבָּת candles.

21:50.  Went upstairs.  Installed Waydroid on Chromebook.  Might install Signal again.  Signal kept saying I wasn't human.  I gave up.  Sent email about it to my friend who wants me to use Signal.

23:27.  Shut laptop and went to sleep.
