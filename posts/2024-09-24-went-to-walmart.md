title: Went to Walmart
date: 2024-09-24 13:26
---

Went to Walmart.  Chatted online.

## Morning

09:00.  Said [מודה אני](https://www.chabad.org/library/article_cdo/aid/623937/jewish/Modeh-Ani-What-and-Why.htm).  Got up.  Got dressed.  Got a water.  Took pills.  Made Assam tea and oatmeal.  Ate oatmeal with pumpkin butter and hemp seeds.

## Afternoon

Went to Walmart.  Got soy milk, tofu, bananas, salt and pepper pistachios, Bubly sparkling water, green onions, and a vegan sausage egg and cheese English muffin.

Ran into Joseph Stamper, W4NRA, at Walmart.  He reminded me to look into the antenna building workshop with the ham radio club.  Unfortunately, it falls right on יום כפור (Yom Kippur) so I cannot go to that one.  I've already made commitments to go to יום כפור sservices.

Got gas when we got back into East Lynne.

16:40.  Cleared out email inbox.  Applied to reset my library card PIN.  Did not receive mail to reset my PIN.  Chatted online.

Came across an interesting book about the American Revolution, where the Founding Fathers are chastised for their disobedience and violent resistence.  I wonder if we'd still be under British rule if the Mennonites had their way.  [_In God We Don't Trust_](https://scrollpublishing.com/products/in-god-we-dont-trust/).  Emailed the library to include [more books about the Amish](https://amishamerica.com/best-amish-books/).

## Evening

17:30.  Watched the movie _American Insurrection_ (2021).

19:30.  Ate rice noodles with stir fry vegetables, tofu, and [peanut dressing](https://www.drmcdougall.com/recipes/peanut-dressing/).  I had to make the peanut dressing from scratch because we were out of store-bought.  It still turned out OK.