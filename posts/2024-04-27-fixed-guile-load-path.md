title: Fixed GUILE_LOAD_PATH
date: 2024-04-27 14:36
---
Got up at 09:30.  Got a glass of Sunny-D.  Took pills and vitamin B12.  Mom wasn't home: She had to take my uncle to the hospital.  I downloaded and installed the todo.txt shell script.  Found a guide on how to fix audio on Chromebooks with non-standard GNU/Linux distributions.  Took a nap.

[Notes for non-standard GNU/Linux distributions](https://raw.githubusercontent.com/chrultrabook/docs/a281259c3016d754540500e4eeea40874098b8eb/src/docs/installing/distros.md) 

Woke up at 14:00!  Got laundry.  Got another glass of Sunny-D.  Spent half an hour debugging my Git repo because I moved some submodules around in the repository.  Finally reorganized my configs to be in individual Stow packages. Added audio instructions to chromebook-guix.

[git status - fatal: cannot chdir](https://www.colinspear.com/technical-posts/git-submodule-fatal-cannot-chdir/) 

17:18.  Took a break.  Checked email: Sorted, not read.  Checked web feeds. Read a really good article by Jason Self about how free software is for everyone, not just those we agree with. Checked Gemini feeds.

[Word is that TikTok will shut down in the US rather than sell its US business to some other company.](https://www.theguardian.com/technology/2024/apr/25/bytedance-shut-down-tiktok-than-sell) 

[The Challenge of Inclusion](https://jxself.org/inclusion.shtml) 

18:00.  Mom spoke very positively to her brother about my McDougall diet.  She really tried to sell it.  Hopefully they will be open to the idea: Someone in their house has a weight issue.  Got another glass of Sunny-D.  Thinking about what kind of potato I want to eat.  Can't wait until Passover is over.  Had a matzo with almond butter while waiting on the hash browns to cook.  Finished off the Sunny-D.

19:30.  Ate hash browns and potatoes O'Brien with ketchup.  Poured a small glass of margarita.  Pushed dotfiles and chromebook-guix to Codeberg.

20:13.  Tried building system.scm in chromebook-guix.  I still have trouble getting the module to load.

[no code for module (chromebook alsa-lib) #1](https://codeberg.org/csh/chromebook-guix/issues/1#issuecomment-1792156) 

Added the repo to "GUILE_LOAD_PATH".  Tried building again.  It seems to be building.

20:49.  Went upstairs.  Thunderstorm!  Blackey (our dachshund) is scared.
