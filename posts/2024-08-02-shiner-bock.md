title: Shiner Bock
date: 2024-08-02 21:51
---

## Morning

05:00.  Woke up.  Said [מוֹדֶה אַנִי](https://www.chabad.org/library/article_cdo/aid/623937/jewish/Modeh-Ani-What-and-Why.htm) blessing.

> מוֹדֶה אֲנִי לְפָנֶיךָ מֶלֶךְ חַי וְקַיָּם, שֶׁהֶחֱזַרְתָּ בִּי נִשְׁמָתִי בְּחֶמְלָה. רַבָּה אֱמוּנָתֶךָ.

> _I offer thanks to You, living and eternal King, for You have mercifully restored my soul within me; Your faithfulness is great._

Turned on the radio with
[Shortwave](https://directory.fsf.org/wiki/Shortwave) app to classic
country station [KZHE](http://kzhe.com/) in Arkansas.  Checked email.
Read ["Share Free Software With Your Friends and
Colleagues"](https://www.fsf.org/blogs/community/share-free-software-with-your-friends-and-colleagues)
in the _FSF Bulletin_.  Browsed the [FSF shop](https://shop.fsf.org/)
and considered a few products to support the [Free Software
Foundation](https://fsf.org/).

* [wooden GNU head sticker](https://shop.fsf.org/stickers/wooden-gnu-head-sticker)
* [Super Sticker Mega Multi Pack](https://shop.fsf.org/stickers/wooden-gnu-head-sticker), the
* ["Ask me about free software" button](https://shop.fsf.org/gear/ask-me-button)
* [GNU emblem classic pin](https://shop.fsf.org/gear/gnu-emblem-classic-pin)
* ["Fight for your user rights" bag](https://shop.fsf.org/gear/fight-your-rights-bag)

06:00.  KZHE started playing a stupid comedy show.  Changed to local
rock station KQRC.  There's a stupid talk show on right now.  Tuned
into local public/variety station KKFI instead.

Continued reading the "Share Free Software" article.  Chatted on
[Snikket](https://snikket.org/) and IRC with [Dino](https://dino.im/).

06:30.  Shut off radio.  All the stations are on stupid talk shows
now, and [aNONradio](https://anonradio.net/) is probably on some
stupid Japanese stuff, so I didn't even bother to check it.

07:00.  Took a nap.

08:30.  Woke up.  Shut off alarm.  Went downstairs to use the
bathroom.  Everyone was still asleep.  Went back upstairs.  Chatted on
IRC.  Turned on the radio to KZHE.

09:00.  Agh!  KZHE is playing Fox News!  All they said politically was
that the Harris campaign got a lot of money, even after the Democrats
switched to Harris.

09:30.  [Wrote to the developer of Shortwave](https://babka.social/@kolev/112892908158760265) on Mastodon.  Thanked him
for the app and asked him how it sources stations, so I can get my
favorite station KAYQ on Shortwave as well.

09:46.  Mom is up now.  Went downstairs.  Took my pills.  Brushed my
teeth.  Took a shower.  Combed my hair.  Oiled and brushed my beard.
Got dressed.  Put on my whimsical אני כלב ("I'm Caleb" / "I'm a dog")
button.

10:46.  Had two bowls of Frosted Flakes with soy milk.  Finished the
box.

11:17.  Chatted on IRC and listened to KQRC until Mom put Jack FM on
the TV.

## Afternoon

12:30.  Chatting with friend from Norway.  He currently has an
autistic interest in American culture, particularly from the
Seventies.  Got a "bellini bliss" Bubly.

13:00.  Aunt Sue came home.  Uncle Jim got mad that she did not get
milk.

13:21.  Went to Walmart.  Got a twelve pack of Starry, a thirty pack
of Hamm's beer, a six pack of Shiner Bock beer, a gallon of cow's
milk, a Starbucks Frap, a Starbucks Paradise Drink, and some new
clothes for Mom.  Stopped and got Mom's carton of cigarettes on the
way home.

14:55.  Came home.  Put beer in the fridge.  Put up the milk and the
Starry.  Mom showed me a cute video of Tibetan mastiffs on her
Facebook feed.  So cute!

15:28.  Ugh.  Uncle Jim says the TV won't work and that it's my
[Jellyfin](https://jellyfin.org/) server's fault.  That's impossible!
Turns out the cause was no batteries in the Roku remote...  So stupid!

Wrote down the recipe to Grandma's vegetable soup.  It will be
published as soon as this entry is published.

16:00.  Watched TV.  Told former coworker about the laptop one of my
uncles is looking to sell.  Chatted on IRC.

## Evening

17:00.  Turned on the country radio station KZHE from Stamps, Arkansas
on my laptop with my earbuds in.

Mom came inside with **huge**, fat cucumbers from the garden!

![Assortment of very large cucumbers on a bed.  Kindle tablet to the right.  One cucumber is yellow.](https://babka-mastodon.nyc3.cdn.digitaloceanspaces.com/media_attachments/files/112/894/777/970/675/732/original/44e7056371a923ba.jpeg)

18:00.  Mom fixed my שַׁבָּת (Shabbat) candles for tonight.  I cleared the
TV tray.  Wine is ready.  I won't be having tea this שַׁבָּת because the
kitchen is a disaster area.

18:44.  Microwaved an Amy's Tofu Scramble, or two.

19:30.  Listened to country music.  Opened a Shiner Bock beer.
Watched Craig Topham on the FOSSY 2024 conference.

20:00.  Lit שַׁבָּת candles.  Drank a glass of wine.

20:30.  Took out the trash.  People are moping in `#gaygeeks` IRC chat
tonight.  Had a piece of almond dark chocolate.  Chatted on COM on
SDF.

21:00.  Added Spectrum TV to my browser bookmarks.  I don't like the
idea of DRM but we have it, so I might as well use it.  Tuned into
KZHE again.

21:30.  Tried to go and brush my teeth.  The dogs barked like crazy
when I tried going into my aunt's room to brush.  I ended up not
brushing my teeth.  I went to bed shortly after and listened to KZHE.

22:00.  Went to sleep.
