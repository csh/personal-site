title: Dear Diary
date: 2022-09-22 00:00
---
Thursday 20220922
  04:00 CDT  Woke up early
  
  I awoke early, at 04:00 CDT. I wore my rainbow heart pin. I forgot
  my watch.
  
  For breakfast, I had raisin oatmeal. For lunch, I had Papa Murphey's
  Gourmet Vegetarian Pizza with vegan cheese, olives, and marinara
  sauce. For dinner, I had chilli with tortillas.
    
  XX:XX CDT  Received goodies from David [last name] at my place
  
  After work, I washed my clothes. I donated $79 to the Temple. I sent
  Mom the dates for Rosh HaShana and Yom Kippur. I inked my pen. David
  came by our apartment with goodies for me. I got a dozen pastries
  and a chocolate cake.

  20:00 CDT  Saw Roey Katz's journal format on a Jitis videoconference with him

  Roey Katz showed me his journal on Jitsi at around 19:00 CDT. He
  gave me his scripts and a directory layout. I plan to use it in
  conjuction with Logarion.

  I brushed my teeth. I wrote in my diary. I went to bed.
     
