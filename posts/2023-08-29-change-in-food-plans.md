title: Change In Food Plans
date: 2023-08-29 00:00
---

In the past few weeks, I've been eating a lot of rich food. Vegan ham
and cheese sandwiches; vegan macaroni and cheese; full-fat hummus with
bread. If I keep this up, I'll be one hundred sixty pounds again,
which is in the overweight range, and makes my pants too tight.

I need to get back to the McDougall diet. I need to stop relying on
premade foods and start making my own food. Make my own hummus and my
own macaroni sauce. Stop eating all the vegan cheeses and vegan
meats. Cut the oil out of my diet. Just eat oats, beans, rice, sweet
potatoes, broccoli, salad, and avocado toast.

For breakfast, have overnight oats with soy milk and peaches. For
lunch, have low-fat hummus, celery, and bread. For dinner, have
[mashed potatoes and gravy](https://www.forksoverknives.com/recipes/vegan-salads-sides/mashed-potatoes-and-gravy/)
with [bean loaf](https://www.brandnewvegan.com/recipes/jeff-novick-bean-burgers).

Other meal ideas:

- Vegetable soup with lentils and rice
- [Potato soup](https://blog.fatfreevegan.com/2009/02/quick-and-easy-potato-soup.html) with bread
- Sweet potatoes and broccoli with [peanut dressing](https://www.drmcdougall.com/recipes/peanut-dressing/)
- [Macaroni](https://www.drmcdougall.com/recipes/cheezy-baked-macaroni/)
- [Breakfast lentils](https://www.veggienumnum.com/vegetarian-recipes/breakfast-lentils/) with rice

There are several things I must stop buying, including:

- Amy's Kitchen frozen meals
- hummus
- chips
- cola
- vegan chicken
- vegan cheese
- vegan ice cream
- fruit juice (except grape juice, for Shabbat)

Instead, I should buy:

- canned chickpeas
- rice
- broccoli
- celery
- carrots
- nutritional yeast
- Bubly (sparkling water)
- potatoes
- sweet potatoes

I'll report my progress in the coming weeks.
