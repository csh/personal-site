title: Dear Diary,
date: 2022-01-01 00:00
tags: guix, sway, emacs
---
Saturday 20210101 [note: file titled 20220101 but entry says "January1, 2021"]

  I did the dishes. I combed my beard, and oiled it.
  I brushed my teeth.

  I improved my Guix Home configuration. I added a Makefile and added
  my Emacs and Sway configs.

  Today I ate avocado toast, hummus, carrots, and eggnog. And I cut my
  fingernails.
