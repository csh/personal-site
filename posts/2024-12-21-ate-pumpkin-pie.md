title: Ate Pumpkin Pie
date: 2024-12-21 15:32
---

Ate pizza.  Ate pumpkin pie.  Chatted online.  Watched TV.

12:30.  Finally got out of bed.  Just haven't been feeling too well.
Not looking forward to my family's Christmas dinner.

Chatted online.  Fell asleep in front of the TV.  Not feeling well.

15:00.  Ate two slices of pizza marinara.  Ate two slices of pumpkin
pie with coconut whipped cream.  Feeling a lot better.

Made [Yunnan Arbor Tree Black Tea](https://www.shangtea.com/tea/p/yunnan-arbor-tree-black-tea).
Turned on Mom's Christmas tree lights.

Watched _National Lampoon's Christmas Vacation_ and _Elf_ on TV.

Tried the Shiner Bock Holiday Cheer beer.  It tastes like a peanch
IPA.  Blech!

21:50.  Went to bed.