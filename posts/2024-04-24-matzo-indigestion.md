title: Matzo Indigestion
date: 2024-04-24 15:21
---
Went to Walmart. Had major indigestion.

Got up at 11:00. Went to Walmart. Got lime Bubly and blackberry Bubly, Sunny-D, and a Starbucks frap (I forgot it has oatmilk; I'll have to wait until after Passover to drink it.) Had major indigestion on the ride to Walmart; I almost thought I'd throw up. We were going to get Pepto, but we thought we still had it.

When we got home, we found out we did not have Pepto. I took the groceries in and laid down, to try and be gentle with my stomach. I had some carrot juice.

15:31. Checked email. Checked web feeds. Checked Gemini feeds. Checked Mastodon. Logged into COM on SDF Public Access UNIX System. Checked System Crafters forum and made a post about audio on Chromebooks running Guix System. Ate two little packets of Swedish Fish Minis.

[Interview with David Wilson](https://www.fsf.org/blogs/community/what-role-community-plays-in-free-software-and-more-interview-with-david-wilson) 

[Help Wanted: Guix Audio on Chromebooks](https://forum.systemcrafters.net/t/help-wanted-guix-audio-on-chromebooks/623?u=kolev) 

17:34. Logged into IRCNow training server. Sent test email: I'm missing SPF and DMARC.

18:21. I made it to the end of the course! I got email fully working!

Took another nap. Stomach gas was getting to me.

21:11. Went upstairs.
