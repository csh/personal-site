title: Uncle Went to the Hospital
date: 2024-04-30 12:40
---
Uncle went to the hospital.  Chatted online.  Tried fixing audio on Miller the Chromebook.  Had bean burritos for dinner.

Stayed up til 02:30 chatting on IRC and waiting for system to build.  It built successfully!  I rebooted into the new system, but sound still did not work.

12:15.  Woke up.  Got dressed.  Went downstairs.  Took pills.  Got a pineapple coconut Bubly.  Brushed my hair.

Got on laptop Hermes.  Checked System Crafters forum.  Replied to mrok that the system built but I still have no sound.  Checked email.  Checked web feeds.  Checked Gemini feeds.  Checked Mastodon.

[A long wait in the casualty dept, UK](https://stallman.org/archives/2024-jan-apr.html#29_April_2024_(A_long_wait_in_the_casualty_dept%2C_UK))

[Cashless society](https://stallman.org/archives/2024-jan-apr.html#29_April_2024_(china_cashless_society))

[NOAA in Danger](https://stallman.org/archives/2024-jan-apr.html#29_April_2024_(trump_noaa_climate_crisis))

13:30.  Mom and uncle left to go to the hospital.  I stayed home, drank an oatmilk Starbucks frap, and ate Cheerios with soy milk.  Had a delightful visit to the bathroom.  Logged into Snikket.

14:00.  Emailed bug-guix the transcript of my failed "guix pull" on Trisquel.  Posted about it to the Trisquel forum as well.

15:10.  Mom and uncle return home.

15:54.  Made some changes to Chromebook Guix.  Named package "chromebook-alsa-lib".  Built system.  Posted update to System Crafters forum.

16:31.  Found out Gnafu will now be working full time with MBOA.  Having bean burritos for dinner tonight.  Plugged in laptop Hermes.

[Gnafu](https://themayhaks.com/~gideon/blog/index.html) 
[MBOA](https://mboa.dev/)

17:39.  Learned how to kill a whole line without doing C-a C-k: "kill-whole-line" or C-S-backspace.  Unfortunately, my terminal won't let me type it.  I guess I'll stick to C-a C-k.  I also learned how to navigate by sentences: M-a ("backward-sentence") and M-e ("forward-sentence").

[Killing by Lines](https://www.gnu.org/software/emacs/manual/html_node/emacs/Killing-by-Lines.html)

18:00.  Watched Farmhouse Fixer on HGTV.  They fixed John Proctor's house.  Someone on System Crafters IRC is making a guide on how to use rde.  Ate a few medjool dates.

[mighty easy rde](https://bienjensu.prose.sh/mighty-easy-rde) 

19:00.  Turned on air conditioner upstairs.  It's twice as hot up there!  Ate 6 bean tacos on corn tortillas with Taco Bell Hot sauce.  Got a glass of iced water.  Rinsed mouth with Listerine.

20:00.  Chatted on IRC.  Heard some news that Russian soldiers are being poisoned.  Got tired.  Thought of going to bed already.  Watched Wildcard Kitchen on Food Network; the lady that had to cook without oil refused to take on the challenge!  We cook without oil all the time!

[Ukrainian women ‘luring starving Russian soldiers to their deaths with poisoned CAKES](https://www.the-sun.com/news/11209447/ukrainian-women-russian-soldiers-deaths-cakes/) 

21:00.  Went upstairs to work on Miller.
