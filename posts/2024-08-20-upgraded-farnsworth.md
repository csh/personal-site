title: Upgraded Farnsworth
date: 2024-08-20 21:43
---

Chatted online.  Upgraded server.  Went to Wellbeing Center.  Made weekly schedule.  Watched TV.  Performed backups.

Today's agenda was to go to the Wellbeing Center.

## Morning

06:45.  Got up.  Turned my salt lamp on.  Made my bed.  Got on laptop.  Checked Snikket.  Checked email.  Upgraded Farnsworth.

```
guix pull
guix upgrade
guix package -m "$HOME/manifest.scm"
sudo guix system reconfigure "$HOME/dotfiles/guix/.config/guix/farnsworth.scm"
sudo reboot
```

08:00.  Checked Mastodon.  Crossed off todos in notepad.  Removed old pages from notepad.  Went downstairs.  Took pills with leftover tea from last night.  Took shower, put on deodorant, brushed hair, oiled and brushed beard.  Went back upstairs to get dressed.  Went downstairs and sat with laptop.

09:00.  Ate Apple Jacks with soy milk.  Reheated the rest of my tea and put it into my travel mug.

09:30.  Case worker called.  Getting new primary care physician and checking out the Wellness Center today.

10:00.  [Solicited donations](https://babka.social/@kolev/112994941386666442) for my 2025 FSF associate membership on Mastodon.  Emailed the FSF about whether they will work on free frontends to important websites and web apps.

11:00.  Went to Raymore.  Visited Wellbeing Center.  Going to try it out.

## Afternoon

13:36.  Came home.  Returned some phone calls.

14:00.  Mapped out my general schedule for the next two months.

16:00.  Just got done eating six biscuits with guacamole and chocolate peanut butter spread.  Man, I need to change what I eat...

## Evening

17:00.  Considering a [JMP SIM](https://jmp.chat/sim) so I have a "real" cellphone that can be called at any time of day and be answered.  Made tea.

18:00.  Watched Food Network.  Feeling really good right now.  Ate a vegan grilled cheese.

19:00.  Logged into [SDF](https://sdf.org/) and chatted on COM.

20:00.  `tob` refers me to a [video about springtime tea _long jing_](https://yewtu.be/watch?v=7DmnpLY-V68).

21:00.  Watched _MASH_.  Performed backup.