title: Slept Too Much
date: 2024-08-18 21:30
---

Slept too much.  Chatted online.  Listened to the radio.  Took out the trash.  Depressed; nothing to do.  Watched TV.  Performed backups.

## Morning

09:00.  Woke up to alarm, shut it off, went back to bed.

## Afternoon

12:00.  Woke up, went downstairs, combed hair, got a Starry, took meds.  Got on laptop.  Checked Snikket, email, Mastodon.

13:00.  Listened to 101 The Fox on the radio.  Took out the trash.  Restocked mini fridge with Starry and Bubly.  Feeling nervous because I don't have much to do, and the AC is making the room cold.

14:00.  Researched flash drives.  The highest capacity seems to be [one terabyte](https://www.techradar.com/news/best-usb-flash-drives#section-best-for-gamers).

15:00.  Depressed.  Nothing to do.  Finished medjool dates.

Slept because I'm depressed and can't do anything.

## Evening

17:00.  Woke up.  Got a Bubly.  Made grocery list.

* medjool dates
* bread
* lentils

18:00.  Made lemon giner herbal tea.  Got a Starry.  Mom's making biscuits; I'm going to put guacamole and [Breakfast Lentils](https://www.mondaycampaigns.org/meatless-monday/recipes/breakfast-lentils) on them.  Got another Bubly.

Analyzed disk space on HUSKY HDD.  The biggest folder is my DVDs folder at 800 GB.

19:00.  Ate four biscuits with guacamole and breakfast lentils.

20:00.  Ripped _Air Force One_ ISO to an MKV file and uploaded it to my Jellyfin server.

21:00.  Watched _MASH_.  Performed backups.