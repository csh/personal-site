title:
date: 2024-09-16 17:03
---

Went to the Wellbeing Center.  Met someone new.  Ate McDougall-compliant food all day.  Chatted online.

## Morning

06:00.  Got a wakeup call.  Got dressed.  Ate Cheerios with soy milk.  Packed bag.  Waited on ride, drank coffee and chatted with Mom.  Rode to Wellbeing Center.

Researched things to do in Washington, D.C.

11:30.  Talked with friend on the phone.

## Afternoon

12:00.  Lunch time.  Ate potatoes, celery and carrots with [fat-free golden gravy](https://www.drmcdougall.com/recipes/fat-free-golden-gravy/).  Chatted with a new person; they were really cool to talk to.

Did chores.  Got ready to go home.  Chatted online on the way home.

## Evening

17:00.  Came home.  Put away my dishes from lunch.  Ate two bananas.  Made grocery list.

* Cheerios
* bananas
* tahina
* beef jerky for Mom

18:00.  Emailed DC Minyan about gay Reform converts.  Emailed Rabbi Grussgott about what I should see in D.C.  Ate several slices of Italian bread with tahini and lentils.  Ran out of tahini.  Had the last piece of bread with honey mustard.

20:24.  Went to bed.