title: Recipes
date: 2023-05-03 00:00
---

My mom makes these recipes for me regularly. All of them are vegan, low in fat, and
follow the [McDougall diet](https://www.drmcdougall.com/education/information/cpb/) plan.

## Dinner

- [Breakfast Lentils](https://veggienumnum.com/vegetarian-recipes/breakfast-lentils/) with tahini on toast.
- [Bean Burgers](https://www.brandnewvegan.com/recipes/beans-legumes/jeff-novick-bean-burgers) - Makes good meatloaf.
- Baked sweet potatoes with [Peanut Dressing](https://www.drmcdougall.com/recipes/peanut-dressing/).
- Spaghetti with [garlic toast](https://www.drmcdougall.com/recipes/roasted-garlic-bread/) and [cashew parm](https://minimalistbaker.com/how-to-make-vegan-parmesan-cheese/)
- Stir fry with [Asian Ginger Sauce](https://www.drmcdougall.com/recipes/asian-ginger-sauce/).
- [Mashed Potatoes and Gravy](https://www.forksoverknives.com/recipes/vegan-salads-sides/mashed-potatoes-and-gravy/)
- [Vegetable Soup](https://bluehome.net/csh/2021/10/18/vegetable-soup)

## Dessert

- [Banana Bread](https://www.drmcdougall.com/recipes/banana-nut-bread/)
