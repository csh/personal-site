title: Watched TV
date: 2024-05-06 12:34
---
Watched TV.

12:30.  Got up, went downstairs, got a can of Bubly, took pills.  Logged into Snikket.  Checked email.  Checked FSF members forum.  Checked Mastodon.  Checked System Crafters forum.  Checked Trisquel forum.

13:00.  Checked web feeds.  Checked Gemini feeds.  Made tea and oats.  Ate oats with apple butter and hemp seeds.  Drank tea with soy milk.

Fell asleep.  Don't know when I woke up.

18:00.  Watched TV the last few hours.  Watched My Lottery Dream Home on HGTV.  Had a margarita.  Got an iced tea.  Subscribed to Alysssa Rosenzweig's blog.

[Alyssa Rosenzweig](https://rosenzweig.io/)

19:30.  Watched Ugliest House in America on HGTV.

20:30.  Watched My Lottery Dream Home on HGTV.

21:00.  Tornado watch.  Ate mushroom spinach bean enchilada with Spanish rice.

21:30.  Went upstairs.

