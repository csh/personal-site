title: Where to Go for Mother's Day 2024?
date: 2024-05-11 11:50
---
Styled my code.  Wondered where to go for Mother's Day.

06:30.  Woke up.  Checked IRC.  Went back to sleep.

11:30.  Got up.  Got dressed.  Went downstairs.  Cut fingernails.  Brushed hair.  Got a can of Bubly.  Took pills.  Took vitamin B12.

Got on laptop Hermes.  Logged into Snikket.

12:00.  Styled my code in Chromebook Guix and Dotfiles, and pushed to Codeberg.  Removed unnecessary comments, made sure lines stayed within the eighty character limit, and added page breaks in logical places.

13:00.  Checked web feeds.  Checked Gemini feeds.  Checked email.  Checked Mastodon.  Checked Trisquel GNU/Linux forum.  Checked System Crafters forum.  Downloaded System Crafters video about new Guix features.  Checked FSF member forum.

[Mental illness' childhood-causation, AUS](https://stallman.org/archives/2024-mar-jun.html#10_May_2024_(Mental_illness'_childhood-causation,_AUS))

[Experimenting with New Guix Features - System Crafters Live!](https://redirect.invidious.io/live/cKUD5XxjtlY)

13:30.  Ate a few barbecue roasted pistachios.  Ate Cheerios with soy milk.

14:30.  Trying to figure out where to go to eat tomorrow for Mother's Day that has vegan options and is not Asian food.  Thinking about Olive Garden.

18:30.  Ate a handful of tater tots with ketchup, and three pieces of Gardein f'sh, and drank a can of Sprite.

20:00.  Went upstairs.
