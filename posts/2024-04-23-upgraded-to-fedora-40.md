title: Upgraded to Fedora 40
date: 2024-04-23 22:38
---
Switched to the straw hat. Added 0x0.st to Emacs. Updated to Fedora 40. Case worker came by. Remapped keyboard.

[My straw hat (left)](http://gohnbrothers.com/images/hat5.jpg) 

## Morning

Woke up at 06:45. Went downstairs. Got a lime Bubly. Took pills. Stayed up until 08:00. Nobody was awake, so I went back to bed.

Woke up again at 12:15. Got a pinapple coconut Bubly. Got on laptop, hermes.

Added 0x0.st pastebin script to my Emacs.

[0x0 - MELPA](https://melpa.org/#/0x0) 

Checked Mastodon, System Crafters forum. Enabled Kanata systemd service: Still cannot get line under h. Checked Fedora forum. Checked Trisquel forum. Checked web feeds. Checked Gemini feeds.

[Rob's Gemini Capsule: Thoughts and Photos [requires Gemini app]](gemini://jsreed5.org/log/2024/202404/20240422-2024-week-15-16-thoughts-and-photos.gmi)

Began upgrading Chromebook (miller) to Fedora 40.

14:30. Had four matsot - three with guacamole, one with almond butter. Downed a glass of carrot juice. Checked email.

Today I learned the iBad has had no calculator all these years.

[iBadOS Could Ship With Built-In Calculator App](https://arstechnica.com/gadgets/2024/04/ipados-18-could-ship-with-built-in-calculator-app-after-14-calculator-less-years/) 

## Afternoon

16:30. My case worker came by. She said I've been doing well and that we just need to meet once a month.

Tried editing .XCompose, hoping to get ẖ from Compose _ h. It did not work.

```
<dead_stroke> <h>                       : "ẖ"   U1E96 # LATIN SMALL LETTER H WITH COMBINING MACRON BELOW
<Multi_key> <underscore> <h>            : "ẖ"   U1E96 # LATIN SMALL LETTER H WITH COMBINING MACRON BELOW
```

Posted to the Trisquel forum.

[Customizing XCompose](https://trisquel.info/en/forum/customizing-xcompose) 

## Evening

Going to have hash browns with matzo ball soup for dinner.

19:30. Ate hash browns with guac, and matzo ball soup, for dinner. Shom helped me get H̲ and ẖ to work on my keyboard.

20:46. Rinsed mouth with Listerine. Got ready for bed. Watched Wildcard Kitchen on Food Network.

21:30. Chatted on IRC. A new user, Richard Davis, showed up in the System Crafters IRC. He is a composer and uses GNU LilyPond!

[Richard Davis](https://richarddavis.xyz/en/index.html) 

22:00. Went upstairs. Backed up my data on Miller, the Chromebook. Published photo of my laptop setup to SDF.

[Photo of Chromebook and ThinkPad](/csh/media/IMG_20240423_222411_deskshot.jpg) 

23:30. Added the following letters to my keyboard: ŭ π ſ ĝ ẖ ĵ λ ĉ ñ μ. Got a Bubly.

## Thanks

[Shom](https://shom.dev/) 
