title: Sold Saxophone
date: 2024-09-10 18:10
---

Sold saxophone.  Researched plane tickets.  Launched [PA Dutch chat](https://anonymous.cheogram.com/deitsch@groups.csh.snikket.chat).  Watched TV.

## Morning

07:00.  Nobody was awake.  Went back to sleep.

11:00.  Got woke up.  Time to sell saxophone.  Went to Grandview.  The guy was nice, but we had to go to an ATM.

## Afternoon

Came home.  Made Assam tea.  Had it with caramel almond creamer.

Looked at plane tickets to D.C.  Decided when I'm going to go, and what I'm going to pack.  

Received the [Temple Israel](https://templeisraelkc.org/) newsletter.  Marked all the High Holidays on the calendar.

## Evening

18:00.  [Doug Madenford](https://padutch101.com/) offered to advertise my [PA Dutch chat](https://anonymous.cheogram.com/deitsch@groups.csh.snikket.chat) on his YouTube channel.  I made a Snikket circle for them to join, and provided a web chat as well.  We'll see how many people we get tomorrow at five or six o'clock in the evening.  I hope it goes well.  I also announced it on Mastodon.

19:00.  Ate a plate of rice noodles, tofu, green beans, broccoli, and peanut sauce.  It was good!  Got a Guinness beer.

20:30.  Finished beer.  Watched TV.  Internet has been flaky tonight, disconnecting and prompting for password.

21:00.  Just asked when I'll be picked up to go to the Wellbeing Center.  Ten til seven!  I'm not sure about that.  Well, good  night.