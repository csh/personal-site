title: Four Hours
date: 2024-05-10 15:55
---
Waited almost four hours at Family Services.  Went to Walmart.  Had vegan fish and tater tots for dinner.

Woke up.  Got dressed.  Went downstairs.  Got a can of Bubly.  Took pills.  Brushed hair.

Went to Family Services.  Spent almost four hours (three and a half) there, waiting for the phone call to my interview for food stamps.  Got extremely hungry; hadn't eaten anything all day.  Got approved for food stamps.  Ran into Steven Peterson, my aunt Fay's nephew; he's gay and has a curable form of cancer.

Went to Walmart.  I was so hungry, I decided to break my diet.  For the ride home, I got prunes, shelled barbecue roasted pistachios, a little thing of hummus and pretzels, a vanilla chai drink from Bolthouse Farms, and an oatmilk frap from Starbucks.  For home, I got green onion, lentils, cilantro, two boxes of Bubly, a carton of soy milk, tater tots, Gardein F'sh Filets, and Amy's Kitchen tofu scramble.  For Mom, I got cow's milk and beef jerky.

Rode home.  Ate the hummus and pretzels.  Drank the Starbucks frap.  Ate some pistachios.  Chugged down the vanilla chai.  Brought the groceries in.  Microwaved one box of tofu scramble.  Scarfed it down.

16:19.  Checked email.  Checked web feeds.  Checked Gemini feeds.  Checked Mastodon.  Checked Trisquel GNU/Linux forum.  Checked System Crafters forum.  Checked FSF member forum.  

[$18,400 Amish Buggy](https://amishamerica.com/18400-dollar-amish-buggy/)

[Medieval leprosy via fur trade](https://stallman.org/archives/2024-mar-jun.html#9_May_2024_(Medieval_leprosy_via_fur_trade))

19:00.  Got a lemon sorbet Bubly.

20:30.  Ate three pieces of Gardein f'sh and two handfuls of tater tots, with malt vinegar.

21:00.  Went upstairs.
