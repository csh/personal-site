title: Took Cans to Recycling
date: 2024-08-01 20:49
---

Today was a good day.  Went to scrap metal place, Dollar Tree,
Walmart, and post office.

## Cans

Went to the scrap metal place in Lee's Summit.  We have at least eight
large trashbags full of aluminum cans from all our drinks over the
past year.  The cans amounted to fifty-five pounds of aluminum, worth
twenty-seven U.S. dollars.  The car smells like beer now, but
twenty-seven dollars is twenty-seven dollars!

## Dollar Tree

We went to Dollar Tree before going to Walmart.  We got disposable
baking pans for cooking tonight, to make things easier.  Doing dishes
has been too difficult lately.

## Walmart

We went to Walmart and I got food for the next few nights:

* Amy's Chinese Noodles & Veggies
* Amy's Tofu Scramble
* Amy's Veggie Loaf
* Amy's bean burritos

Mom got some frozen meal for everybody else, too.  I got my usual
Starbucks oatmilk Frappuccino from the checkout line.  I also
discovered something really amazing: Walmart has Starbucks Pink Drink
(another vegan drink) in the checkout line, too!  I got that as well.
I drank both on the ride home.

## Post Office

We stopped by the post office.  I got the mail from the P.O. box.
Everyone got some mail today.  I checked with the postman to see if I
had any packages: My new pills came.  The psychiatrist upped my
prescription, so these pills are especially new.

## General Update

A lot has happened since my last proper blog post.  I've switched back
to using the [Haunt static site generator](https://dthompson.us/projects/haunt.html) because it is
better supported on [GNU Guix System](https://guix.gnu.org/) and
because it allows me to better customize [my homepage](https://bluehome.net/csh/).

I'm doing an experiment.  I've put my mobile device away in a drawer
somewhere, and will not be using it for a month.  Instead, I will be
using my Chromebook, making phone calls on Wi-Fi with
[JMP](https://jmp.chat/).  I already tried making a call to my friend
earlier with my Chromebook, and it worked OK.  It was a little choppy,
however.  I'm hoping that was just a one-time thing.

My uncle has become impossible to live with.  He took the car and did
not come home until six in the morning.  He fights with us when we're
trying to get chores done.  He threatens to kill our animals.  He
talks to himself all night and all day, and then tries to pick fights
with us.  We've been locking our bedroom doors at night because he's
so unpredictable and he has guns.  We are fed up with his craziness.

Needless to say, this is why my psychiatric medication has been upped.

I've switched back to using a regular graphical email client.  Emacs
worked fine, but migrating it to a new machine was a little tough, so
I just went back to Evolution.  It works.

I've discovered [Shortwave](https://directory.fsf.org/wiki/Shortwave),
a very nice little GNOME app for listening to internet radio streams.
I even got SDF's [aNONradio](https://anonradio.net/) on there,
alongside actual radio stations like KKFI, Kansas City's public radio
station; KQRC, KC's local rock station; and KZHE, a classic country
station in southern Arkansas.  (I could not get KAYQ from Warsaw,
Missouri, my preferred classic country station.  It is only on Amazon
Alexa.)
