title: Movies As Books
date: 2025-01-19 15:38
---

Because of the issues around streaming disservices and Digital
Restrictions Management (DRM) I've been thinking of replacing movies
with books.  Luckily, the transition isn't as difficult as one may
think, as many movies are in fact derived from books.  Therefore, I
will present here a list of movies or series that are originally
books.

* _ Hidden Figures: The American Dream and the Untold Story of the Black Women Mathematicians Who Helped Win the Space Race_
* _Crouching Tiger, Hidden Dragon_
* _Dune_
* _Election_
* _Game of Thrones_
* _Harry Potter_
* _It_
* _Jurassic Park_
* _Rita Hayworth and Shawshank Redemption_
* _Schindler's Ark_
* _The Body_
* _The Color Purple_
* _The Devil Wears Prada_
* _The Godfather_
* _The Green Mile_
* _The Invention of Hugo Cabret_
* _The Lord of the Rings_
* _The Princess Bride_
* _The Princess Diaries_
* _The Silence of the Lambs_
* _The Wizard of Oz_

That should get you started.  Happy reading!
