title: Frost Warning
date: 2024-04-20 12:47
---
Worked the garden. Went to Walmart. Frost warning. Started setting up mail on IRCNow.

Got up at noon. Got a can of lime Bubly. Took pills. Took vitamin B12. Made tea and oats. Had the oats with apple butter and hemp seeds. The tea was Twinings English breakfast with a little bit of Twinings Winter Spice, served with soy milk.

13:00. Checked email, Mastodon and Gemini. Went out to the garden. Planted green beans, radishes, and cucumbers.

14:36. Came back inside. Got a lime Bubly. Washed my hands. Woke up my laptop. Checked my web feed reader. Went to Walmart to get Pepsi, Gatorade, Pringles, and beer.

16:00. Returned home. Had to rush out to the garden and cover the plants; there's a frost warning. Read the Free Software Foundation's Bash style guide. Edited my script "post" to comply. I'm concerned about the that the shebang "#!/bin/bash" will not work on Guix System, but perhaps it's worth it to add a symlink.

[Bash style guide](https://savannah.gnu.org/maintenance/fsf/bash-style-guide/) 

18:00. Began setting up SMTP on OpenBSD for IRCNow. Got mail set up, but it did not pass all the tests. I need to do SPF and DMARC tomorrow.

[SPF](https://wiki.ircnow.org/index.php?n=DNS.SPF) 

[DMARC](https://wiki.ircnow.org/index.php?n=DNS.DMARC) 

20:34. Got pizza to eat. Had three pieces. Dipped the crust in tahini. Yum!

21:29. Logged off.

