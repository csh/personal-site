title: Appointment
date: 2024-08-19 21:57
---

Went to psychiatry appointment.  Went to store.  Sat outside.  Listened to the radio.  Watched TV.  Cleaned laptop with wipes.

Today's agenda was to go to my psychiatry appointment and go shopping.

## Morning

09:00.  Woke up to my alarm.  Got up, got dressed, brushed my hair.  Went downstairs, took my meds with leftover lemon ginger tea, and got on my laptop.  Checked Snikket, email, and Mastodon.  Got a new episode of _Futurama_ on Jellyfin.

10:00.  Got ready for psychiatry appointment.  Grabbed my shopping list, wallet and keys.

## Afternoon

13:45.  Back from psychiatry appointment and Walmart.  Got biscuits, Just Egg, Amy's meals, bread, dates, lentils, soy sauce, peanut butter chocolate spread, and cow's milk for the house.  Drank Starbucks chocolate brownie Frappuccino and Pink Drink on the way home.  Took in the groceries.  Ate the rest of the Frosted Flakes with soy milk and some Cheerios, with chocolate peanut butter spread on a slice of bread.

14:00.  Had a delightful visit to the restroom.  Got a cherry Bubly sparkling water.

15:00.  Followed #backup and #DataHoarder on Mastodon.

16:00.  Sat outside, listened to the crickets.  Came back inside.  Put on the Shortwave app to [KZHE](https://kzhe.com/).

## Evening

17:00.  Got a Starry.  Watched _Beat Bobby Flay_ on TV.  Cleaned laptop with eyeglass cleaner.

18:00.  Got a pineapple coconut Bubly sparkling water.

19:00.  Ate four biscuits: Two with lentils, eggs and cheese; two with guac and lentils.  **So** good!

20:00.  Emailed [B&H](https://www.bhphotovideo.com/) in Yiddish.

21:00.  Watched _MASH_ on Jellyfin.

22:00.  Performed backups.  Went to bed.

## Tomorrow

* Go to Compass Health