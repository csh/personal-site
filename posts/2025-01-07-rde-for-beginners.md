title: rde For Beginners
date: 2025-01-07 16:26
---

These instructions detail a step-by-step guide to getting set up with
the lightweight, minimalistic [`rde` desktop](https://trop.in/rde/).
It covers everything one would need for basic computing, such as web
browsing, chatting, and listening to music.

To get started, use `rde` Kickstart.

    $ git clone https://git.sr.ht/~whereiseveryone/rde-kickstart
    $ guix shell -m manifest.scm

Make sure to fill in your user info.

```
(feature-user-info
     #:user-name "caleb"
     #:full-name "Caleb Herbert"
     #:email "csh@bluehome.net"
     #:emacs-advanced-user? #f)
```

Now, reconfigure.

    $ just reconfigure

Make sure to re-run this command every time you want a change to your
`rde` configuration to take effect.

By default, this does not do much.  It just installs GNU Emacs.  To
get a full desktop, we'll need to add a lot more.

* Desktop
* Keyboard layout
* Terminal
* Web browser
* Chat
* Radio

## Desktop

Add the `wm` and `keyboard` modules.  Sway requires the keyboard module.

```
#:use-module (rde features keyboard)
#:use-module (rde features wm)
```

Then add the following code for a good set of defaults.

```
(rde-config-home-environment
 (rde-config
  (features
   (list
   ;; ...

    ;; Keyboard
    (feature-keyboard
     #:keyboard-layout (keyboard-layout "us"))
                      
    ;; Desktop
    (feature-sway)
    (feature-waybar)
    (feature-swaylock)))))
```

You may also want to use the `(rde features networking)` module for
`(feature-networking)`.

## Terminal

First, you need to choose a terminal emulator and a shell.  `foot`
works well on all machines, and Bash is the standard shell on most
GNU/Linux systems.

Load the `terminals` and `shells` modules.

```
#:use-modules (rde features terminals)
#:use-modules (rde features shells)
```

Add the `foot` feature.

```
(rde-config-home-environment
 (rde-config
  (features
   (list
   ;; ...

   ;; Terminal
   (feature-foot)
   (feature-bash)))))
```

## Web Browser

Add the `(rde features web-browsers)` module.

I recommend using a Mozilla-based browser with `(feature-librewolf)`.

If you insist on a Chromium-based browser, I recommend Ungoogled
Chromium with `(feature-ungoogled-chromium)`.

## Chat

I recommend [Snikket](https://snikket.org/) (XMPP) for all your chat
needs.  Unfortunately, the only client I can recommend for Snikket at
this time is Dino.  Even though it is not a program for GNU Emacs, it
supports all the critical features one expects of a modern chat
client, including end-to-end encryption.  Hopefully one day there will
be a decent Snikket client for Emacs, but Dino will have to suffice
for now.

To use it, all you have to do is add it to your packages.

```
(feature-base-packages
     #:home-packages
     (map specification->package
	  (list "dino")))
```

Make sure to add the `(gnu packages)` module.

## Radio

You may also want to listen to music while you use Emacs.  `eradio` is
an excellent package for this.  Just add it to your `#:home-packages`
list!

```
(feature-base-packages
     #:home-packages
     (map specification->package
	  (list "dino" "emacs-eradio")))
```

You will also have to configure Emacs to use it.  Add the following
under `(feature-emacs)`.

```
     #:extra-init-el
     '((setq eradio-player '("mpv" "--no-video" "--no-terminal"))
       (global-set-key (kbd "C-c r p") 'eradio-play)
       (global-set-key (kbd "C-c r s") 'eradio-stop)
       (global-set-key (kbd "C-c r t") 'eradio-toggle)
       (setq
        eradio-channels
        '(("94.9 KCMO" . "https://provisioning.streamtheworld.com/pls/KCMOFMAAC.pls")
          ("KQRC 98.9 The Rock!" . "https://live.amperwave.net/manifest/audacy-kqrcfmaac-hlsc.m3u8")
          ("101 The Fox" . "https://playerservices.streamtheworld.com/api/livestream-redirect/KCFXFMAAC.aac")
          ("KKFI 90.1 Kansas City, MO" . "http://pacificaservice.org:8000/kkfi_64?type=.mp3")
          ("iHeart2000s" . "https://stream.revma.ihrhls.com/zc6850/hls.m3u8")
          ("aNONradio" . "http://anonradio.net:8000/anonradio")
          ("Chabad.org Jewish Music" . "https://stream.radio.co/sdfd68a101/listen")
          ("Chabad.org Torah Classes Radio" . "https://streams.radio.co/s9f35c3afc/listen")
          ("KZHE" . "http://crystalout.surfernetwork.com:8001/KZHE-FM_MP3")))
       )
```

The variable `eradio-channels` contains my personal selection of
stations, both local and remote.  You can find more channels to stream
on [Radio Browser](https://www.radio-browser.info/).

## Appearance

By this point, you may have a functional desktop, but it lacks that
`rde` flare.  Let's change that.

Modules:

```
#:use-module (rde features emacs-xyz)
#:use-module (rde features fontutils)
```

Features:

```
    (feature-emacs-appearance)
    (feature-emacs-modus-themes)
    (feature-emacs-all-the-icons)
    (feature-fonts)
```
