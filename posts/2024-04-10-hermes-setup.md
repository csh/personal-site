title: Hermes Setup
date: 2024-04-10 22:03
tags: computers
---
Start terminal on login.

* Trisquel > System > Preferences > Personal > Startup Applications
* "Add". Name: "MATE Terminal". Command: "mate-terminal". "Add". "Close".

Make terminal bigger.

* Edit > Profiles > New > Profile name: "Kids"
 Edit > Profiles... > Kids > Edit
* ☐ Use the system fixed width font
* ☑️ Use custom default terminal size
* Default size: "132" columns "43" rows.

Install tmux and GNU Emacs.

```
sudo apt install tmux emacs
```

Mount remote filesystem.

* Caja
* File > Connect to Server...
* Server: "farnsworth"
* Type: SSH
* Folder: "/mnt"
* User Name: "caleb"
* Password:
* ☑️ Remember this password
* ☑️ Add bookmark
* Bookmark Name: "HUSKY"
* Connect

Copy backup.

* Caja
* HUSKY
* Caleb > Backup > Chromebook
* Drag "backup-miller-caleb" to desktop
* Wait for hours

Restore from backup.

* Install backup program: `sudo apt install vorta`
* Trisquel > Accessories > Vorta
* Add existing repository
* File (Choose a local path as repository)
* Archives
* Selected Archive
* Mount
* Drag and drop files to desktop

Connect to Snikket.

```
sudo apt install profanity
profanity
/connect caleb@csh.snikket.chat
/omemo gen
/carbons on
/omemo fingerprint Mom
/omemo trust [FINGERPRINT HERE]
/omemo start Mom
```

Configure keyboard and input methods. Add Chinese, Russian, Hebrew, Compose, emoji.

```
sudo apt install ibus-mozc ibus-libpinyin fonts-noto-color-emoji
killall ibus-daemon
```

* Trisquel > System > Preferences > Hardware > Keyboard
* Layouts
* ☐ Separate layout for each window
* Options
* Position of Compose key:
* ☑️ Menu
* Trisquel > System > Control Center
* IBus Preferences
* Input Method
* Add: Russian, Hebrew, Chinese: Intelligent Pinyin, Japanese (Mozc)
* General
* ☑️ Show icon on system tray (DOES NOT WORK)

Restore Abrowser settings.

```
cd ~/Desktop/caleb/
rm -rf ~/.mozilla
cp -R .mozilla/ ~/
cd ~/.mozilla
mv icecat abrowser
```

Set up password manager.

```
sudo apt install pass
cp -R ~/Desktop/caleb/.password-store/ ~/
```

Restore OpenPGP keys.

```
rm -rf ~/.gnupg/
cp -R ~/Desktop/caleb/.gnupg ~/
```

Set up SSH.

```
echo 'export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)' >> ~/.bashrc
echo 'gpgconf --launch gpg-agent' >> ~/.bashrc
```

Set up Git.

```
mv ~/Desktop/caleb/.gitconfig ~
```

Set up Icedove.

* Trisquel > Internet > Icedove
* Email address: "csh@bluehome.net"
* Password:

```
gpg --export-secret-keys > ~/Desktop/caleb.asc
```

* Menu > Account Settings > End-To-End Encryption
* Add Key...
* Import an existing OpenPGP Key
* Continue
* Select File to Import
* Desktop > caleb.asc
* Continue
* Close

Set up Syncthing.

* sudo apt install syncthing
* Trisquel > Internet > Start Syncthing
* Trisquel > Internet > Syncthing Web UI
* Actions > Show ID
* On mobile device: Syncthing > Devices > + > Scan QR Code
* On mobile device: Folders > Camera > hermes
* Trisquel > Preferences > Personal > Startup Applications
* Name: Syncthing
* Command: /usr/bin/syncthing serve --no-browser --logfile=default

Install Haunt.

```
sudo apt install guix
guix pull
guix install glibc-locales
echo 'export GUIX_LOCPATH=$HOME/.guix-profile/lib/locale' >> ~/.bashrc
guix install fontconfig
xset +fp $(dirname $(readlink -f ~/.guix-profile/share/fonts/truetype/fonts.dir))
guix install nss-certs
echo 'export SSL_CERT_DIR="$HOME/.guix-profile/etc/ssl/certs"' >> ~/.bashrc
echo 'export SSL_CERT_FILE="$HOME/.guix-profile/etc/ssl/certs/ca-certificates.crt"' >> ~/.bashrc
echo 'export GIT_SSL_CAINFO="$SSL_CERT_FILE"' >> ~/.bashrc
echo 'GUIX_PROFILE="$HOME/.guix-profile"' >> ~/.bashrc
echo '. "$GUIX_PROFILE/etc/profile"' >> ~/.bashrc
echo 'GUIX_PROFILE="$HOME/.config/guix/current"' >> ~/.bashrc
echo '. "$GUIX_PROFILE/etc/profile"' >> ~/.bashrc
guix install haunt
```

Fix MATE login after installing Guix. (Thanks, Avron!)

```
echo 'export XDG_DATA_DIRS="/usr/local/share:/usr/share/${XDG_DATA_DIRS:+:}$XDG_DATA_DIRS"' >> ~/.profile
```

Install yt-dlp.

* Go to yt-dlp's GitHub and click on "Releases".

[yt-dlp on GitHub](https://github.com/yt-dlp/yt-dlp)

* Right-click on "yt-dlp"
* "Save Link As..."
* Save to "~/.local/bin/".

```
echo 'export PATH="$HOME/.local/bin:$PATH"' >> .bashrc
chmod +x .local/bin/yt-dlp
```
