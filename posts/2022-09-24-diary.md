title: Dear Diary
date: 2022-09-24 00:00
---
Saturday 20220924
  08:00 CDT  Woke up at a decent time. Ate a vegan blueberry scone I
  got from David Russell Thursday (20220922) with some matcha tea that
  Mom got me Monday (20220919) while wearing a blue and white striped
  robe. Got dressed with my green and grey checked button-up shirt and
  my black broadfall "Amish" pants.

  10:00 CDT Transcribed diary entries while Mom was doing the
  dishes. I got almost all of them transcribed and added to this diary
  repository. The ones I have yet to transcribe are in the root of
  "supplemental/". My friend Roey Katz edited my transcriptions to
  match his format, and I merged his changes. I also added GNU GPLv3
  license headers to his scripts that I am using in the "scripts/"
  directory. If this diary system gets published (with Roey's
  approval) then I will call it "Yoman."

  18:00 CDT  Talked to my friend Roey Katz about his diary system. We
  discussed this over the phone and in private message on Libera.Chat
  IRC. I asked him if each entry had to have the day and date at the
  top, and he said it did. I asked him if the file names had to
  exactly match YYYYMMDD and he said they did. He moved my page scan
  from 20210721 to "supplemental/20210721.supplemental/20210721.jpg".

  18:30 CDT  I wrote my first diary entry. Once I finished writing this
  entry, I sent him a copy of this very first diary entry written in
  his format.

  20:17 CDT  Watched American Horror Story. It turns out that the
  perverted doctor in the insane asylum is a Nazi war criminal, and a
  new patient at the facility is Anne Frank.
