title: Walmart
date: 2024-04-17 21:31
---
Went to Walmart. Checked mail.

Got up a little before 11:00. Brushed hair, oiled beard, combed beard, put on deodorant. Took pills, made and ate tea and oats. Logged into SDF Public Access UNIX System.

11:30. Went to Walmart. Got dark chocolate, English breakfast tea, cilantro, and green onion. Going to have lentil sandwiches for dinner tonight. Had an oatmilk Starbucks frappuccino on the way home.

[Breakfast Lentils](https://www.mondaycampaigns.org/meatless-monday/recipes/breakfast-lentils) 

12:53. Returned from Walmart. Checked mail: The PO box was so full! Brought in groceries. Read the mail: My primary care physician is retiring. Had a wonderful visist to the bathroom. Checked Mastodon and email: Nothing.

13:26. Logged into IRCNow Minutemin Bootcamp training host. I hope I can tackle "unbound" today. 

<https://kolev.host.planetofnix.com/>

15:26. Finished unbound and nsd on the Minutemin Bootcamp. All I have left is reverse DNS, and email stuff. My trainer jrmu left for a few hours; I took a break.

Nervous about tomorrow. I have to demonstrate how my autism, depression and anxiety makes it so that I can't keep a job.

18:00. Checked Gemini. librehacker made a post about trees and birds. jrmu introduced me to my future teammates on IRCNow.

19:00. Had coconut pineapple bubly with a spoonful of honey and a shot of whiskey. Ate two lentil-tahini sandwiches for dinner. Finished the last of my tea. Talked about GNU Hurd in #guix on Libera.Chat.

Subscribed to some blogs.

[gnucode](https://gnucode.me/index.html)

[jxself](https://jxself.org/)

[Richard Stallman](https://stallman.org/)

[freakingpenguin](https://freakingpenguin.com/) 

21:30. Went upstairs and got ready for bed. Chatted on COM and IRC.
