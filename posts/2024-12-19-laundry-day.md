title: Laundry Day
date: 2024-12-19 12:40
---

Did laundry.  Took a shower.  Wrote letters.  Chatted online.  Ordered
pizza.  Watched TV.

11:30.  Showered.  Got up, went downstairs, grabbed a Starry
lemon-lime soda, took my pills, grabbed a towel, and took a shower.

12:30.  Did laundry.  Went upstairs.  Collected laundry.  Went
downstairs, waited on Mom to put some laundry in with mine.  Waited a
few minutes.  Put laundry in the wash.

14:48.  Wrote two letters and addressed them.  One had the Name of God
in it.  I'm not sure what to do with it.  I can't throw it in
recycling or trash.  I guess I have to keep ahold of it.

15:34.  Ordered pizza.  Chatted online.  Went to go get pizza.
Dropped off the mail.  Checked the mail.  No mail.

16:44.  Came home.  Brought in pizza.  Put my vegan pizza in the oven.
Drank two cups of tea.

17:30.  Ate half a family size Gourmet Vegetarian pizza with no
cheese.  Added [cashew parm](https://minimalistbaker.com/how-to-make-vegan-parmesan-cheese/)
and crushed red pepper flakes.

19:39.  Watched TV.  Harry Potter cooking competition on Food Network.