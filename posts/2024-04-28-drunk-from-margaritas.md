title: Drunk From Margaritas
date: 2024-04-28 14:53
---
Woke up at 08:30 to my alarm, but went back to sleep and woke up at 14:00 again.  Logged into my laptop to find that my fonts weren't working.  I had to fix the path to my fonts.  Codeberg is down; Git just hanged and I couldn't push my changes.  I guess other Codeberg users were having the same issue, so it's not my setup.  I even tried using SSH directly, and it worked with other hosts just fine.  Ate a matzo with almond butter.

Logged into Snikket.  Checked email.  Checked Gemini feeds.  Checked web feeds.  Checked Mastodon: Got some replies on my Chromebook issue.  Got a message from my friend from Norway; haven't heard from him in a while!  Checked System Crafters forum: No replies, but I'll post an update.  Tried building system.scm again, to post the result.

[Brazilian indigenous groups rallied together in Brasilia against the construction of a railroad through Amazonia, intended by its backers to facilitate mining and deforestation.](https://www.commondreams.org/newswire/major-brazilian-mobilization-demands-indigenous-land-rights-in-the-face-of-mounting-threats)

[A "new government" was installed in Haiti.](https://www.theguardian.com/world/2024/apr/25/new-haiti-government-sworn-in-during-secret-ceremony)

[Traffic noise stunts bird growth even in the egg: Another reason to #fuckcars.](https://www.theguardian.com/environment/2024/apr/25/noise-from-traffic-stunts-growth-of-baby-birds-study-finds)

18:00.  Tomorrow, I have to get up early, to fix my food stamps.  It's that time again, to redo it.  Put my suspenders on my clean pair of pants for tomorrow.  Pushed dotfiles to Codeberg, now that it's up again.  Ate big potato with salsa for dinner, with pieces of matzo dipped in almond butter.

19:00.  Logged into SDF Public Access UNIX System.  Found out my step-brother is now my step-sister and has breasts.

19:53.  Oh no!  Reya, one of our cats, pushed my glass teapot onto the floor!  Luckily, only the lid broke, but it still sucks: I haven't had that teapot for very long!  Made another margarita.

20:47.  Had a piece of dark chocolate with a glass of cashew milk.  Yum!  So creamy...  Found out the user buttstuf left SDF a while ago.  Sad.  Added "tree" to my manifest in chromebook-guix.  Pushed chromebook-guix to Codeberg.

21:40.  Posted Chromebook audio problem to help-guix mailing list.  Went upstairs.  Might install Guix on Trisquel on Farnsworth before going to bed.
