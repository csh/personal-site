title: Improved Website
date: 2024-08-04 17:41
---

Chatted online.  Ate brunch.  Napped for an hour.  Listened to the radio.  Improved website.  Ate dinner.  Watched TV.

## Morning

04:00.  Woke up.  Said [מודה אני](https://www.chabad.org/library/article_cdo/aid/623937/jewish/Modeh-Ani-What-and-Why.htm) blessing.  Checked IRC and [Snikket](https://snikket.org/).  Tuned into classic country station [KZHE](http://kzhe.com/).  Checked Mastodon.  Checked email.

05:00.  Went back to sleep.

10:24.  Woke up.  Checked laptop.  Got dressed.  Went downstairs.  Brushed hair and beard.  Got a Starry and took pills.  Got on IRC and [Snikket](https://snikket.org/).

Anxiety up.  Checked schedule.  Only thing I need to do is eat breakfast or check my email.  I'm not ready to eat just yet, so I'll check my email.

11:00.  Added Frosted Flakes and Bubly to the shopping list.  Went into the kitchen and got some Cheerios with soy milk.  Had a few Oreos in the leftover soy milk.

Fell asleep.

## Afternoon

13:21.  Woke up.  Tuned into KQRC, our local rock station.  Decided to print a single QR code pointing to a redirect to a Snikket invite.

```
https://bluehome.net/csh/snikket/39e7fja9/
```

`https://bluehome.net/csh/snikket/` will redirect to the Snikket instance.  Perhaps in the future, it will be a landing page for the "Chat" link on my homepage, directing people depending on whether they already have Snikket or not.

14:44.  Added [94.9 KCMO](https://www.949kcmo.com/) to the [Shortwave app](https://flathub.org/apps/de.haeckerfelix.Shortwave) using [`radio-browser.info`](https://www.radio-browser.info/)!  Now I can access several stations:

* 94.9 KCMO
* 101 The Fox
* KKFI
* KZHE
* 98.9 The Rock!
* [aNONradio](https://anonradio.net/)

15:00.  Installed [Pika Backup](https://flathub.org/apps/org.gnome.World.PikaBackup) so I can do backups later.  Thinking of just backing up to the Jellyfin server.  Still listening to KQRC.  Got a bottle of water.  Mom got a beer already.

16:17.  Added a [chat](https://bluehome.net/csh/chat/) link to my homepage.  It's beautifully simple and I think it will work well.  Logged into [SDF](https://sdf.org/) and chatted on COM.

## Evening

17:30.  Drank a bottle of Shiner Bock beer.  Thinking of getting something to eat.

18:00.  Ate my two servings of Amy's veggie loaf, and a few Oreos.  Watched _Naked and Afraid_.

19:00.  Made a QR code for my permanent invite link.  Not sure how to print it.  Might ask someone to print it and mail it to me.  Wished Roey a happy birthday.

20:00.  Asked someone from the local GNU/Linux user group if he could print and mail me the QR code for my Snikket invites.  He said he'll get it sent out tomorrow.

21:00.  Watched _MASH_.  Ate a few more Oreos.  Put more water bottles in the fridge.

22:00.  Went upstairs.
