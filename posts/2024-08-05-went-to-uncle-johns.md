title: Went to Uncle John's
date: 2024-08-05 22:26
---

Went to Uncle John's.  Got groceries.  Chatted online.  Watched TV.

## Morning

04:43.  Woke up.  Got on laptop to check time.  I guess it's late enough to get up for a while and then sleep for an hour or two.  Checked [Snikket](https://snikket.org/).  Tuned into [KZHE](https://kzhe.com/) classic country.  Posted photos to the family chat: a lake, and an alarm clock with four bells.

05:50.  Turned off Shortwave.  Closed laptop and went back to bed.

08:42.  Woke up.  My alarm has been going off for a while.  Got dressed.  Went downstairs.  Mom was still in bed but she got up.  Brushed hair and beard.  Got a Starry.  Took pills.

09:16.  Lawyer called.  Need to turn in the paperwork.  Told them how my medication has been increased and I have three days out of the week where I have great difficulty doing my everyday tasks.  Turned on southern Arkansas classic country station KZHE with the [Shortwave](https://flathub.org/apps/de.haeckerfelix.Shortwave) app for [GNOME](https://gnome.org/).

09:30.  Got in the car.  Left for Uncle John's.  Forgot to bring his DVDs back.

10:00.  Uncle John was watching _Iron Man 2_.  Mom cut his hair.  I took a nap.

11:30.  Woke up and got some Diet Pepsi from Uncle John's fridge.  I really don't like Diet Pepsi.

Went to Walmart in Harrisonville.  Got Frosted Flakes, Cinnamon Toast Crunch, two cartons of soy milk, a box of orange cream Bubly sparkling water, a box of peach Bubly, a chocolate Starbucks frappuccino, a Starbucks Pink Drink; and, for my mom, a bag of beef jerky.  Chugged the Starbucks drinks on the way home.

## Afternoon

13:10.  Came home.  Brought in groceries and put them up.  Had a nice visit to the restroom.  Ate a bowl of Cinnamon Toast Crunch with soy milk.

Got on computer: Cymor said he just mailed off my printing job.  Three copies.

14:30.  Arguing with friend from Norway about the written Norwegian language versus the spoken Norwegian language and how, according to what I've read, **nobody speaks written Norwegian**.

15:50.  Watched _Beat Bobby Flay_ on Food Network.

16:00.  Tuned into south Arkansas classic country station [KZHE](https://kzhe.com/).

## Evening

17:00.  Switched computer to dark theme for the evening.  Checked email.  Checked FSF member forum.

17:11.  Mom just gave Uncle Jim her car keys again.  He has to be back by 19:00.

If I get an ereader, I will get a Kobo, because it can run the free software program KOReader.  I've installed KOReader on my laptop as well.  Going to practice using it.

18:38.  Reheated some pizza for dinner.  Had five pieces.  Also had ten coffee Oreos.  Man, I'm going to gain weight...

19:00.  Uncle Jim has not returned.  Got a Shiner Bock beer.  Took the trash out.  Tuned to [aNONradio](https://anonradio.net/) and logged into [SDF](https://sdf.org/).  My SDF terminal session is lagging **hard** today.

20:00.  Watching _BBQ Brawl_ on Food Network.  Listening to aNONradio still.  Had to switch hosts on SDF to get a faster connection.  `rie` has a good connection.  Found a [list of NOAA Weather Service streams](https://www.weatherusa.net/radio) but the Kansas City one is not available.  Installed Mumble and pointed it to [grue.world](https://grue.world/)'s Mumble server (which currently does not have a stable address).

21:00.  Watched _MASH_ on [Jellyfin](https://jellyfin.org/).  Logged into the Free Software Foundation Mumble server.

21:43.  Rinsed mouth with Listerine and went upstairs.  Listened to country music until I fell asleep.

## Tomorrow

* Shower
* Do laundry
* Check post office box
