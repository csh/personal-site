title: Self Care
date: 2024-08-16 21:38
---

Did a lot of self care today: Cut fingernails, washed bedding.  Chatted online.  Listened to the radio.  Watched TV.  Lit שבת (Sabbath) candles.

Today's agenda was to shower, cut fingernails, wash bedding, upgrade Farnsworth, and light Sabbath candles.

## Morning

09:00.  Woke up to my alarm.  Went back to bed.

10:50.  Woke up.  Got dressed.  Went downstairs.  Put the kettle on for tea.  Got a Bubly and took my meds.  Got on laptop.  Chatted on [Snikket](https://snikket.org/).  Checked email.  Turned on the radio to 101 The Fox.  Cut fingernails.  Brought Mom's laundry into her room.

## Afternoon

12:00.  Filled travel mug with tea.  Restocked the mini fridge with Starry lemon-lime soda and Bubly sparkling water.  Put my plush blanket in the washing machine.  Emailed [Libiquity](http://www.libiquity.com/), a U.S.-based seller of free software ThinkPads, about selling them my damaged ThinkPads.

13:00.  Got postal mail.  Received postcard from Jack Hill, KM4MBG.  Refilled travel mug with the other half of the teapot's worth of tea.  Put plush blanket in the dryer.

14:00.  Uncle John stopped by.  Gave him his movies back, and gave him some ham.

15:00.  Got plush blanket from dryer.  Watched TV.  Jack Hill introduced me to [Perkeep](https://perkeep.org/), a system for archiving all your documents, photos and data.

## Evening

18:00.  Reviewed saved messages on Snikket.  Someone told me to use `non-boot-file-system-service` for my semi-permanently attached external hard drive on Farnsworth.  Emailed `help-guix` mailing list about it, to ensure the issue gets addressed.  Also emailed [a Mennonite church](https://churchofgodinchristmennonite.net/) to ask them how they explain to their non-Mennonite friends why they won't use this or that technology.

19:00.  Ate a plate of waffle fries and [Gardein f'sh filets](https://www.gardein.com/fishless/fsh-filets) with malt vinegar.  Finished it off with a medjool date.  Got a pineapple coconut Bubly sparkling water.

20:00.  Lit שבת (Sabbath) candles.  Was a bit late.  Finished the wine.  Watched _My Lottery Dream Home_ on HGTV.

21:00.  Put on _MASH_ on Jellyfin.  Thought about backing up, but might wait.  This article is not done yet.

21:30.  Might go ahead and wrap up this article and do backups.  Good night.  I'll probably go to bed at 22:30.

## Tomorrow

* Read weekly תורה (Torah) portion