title: Stop Cursing
date: 2023-05-03 00:00
---

I have made it my goal to stop cursing. Not only is it impolite, but
it's something you can't turn on or off. You have to stop completely
if you want to make sure the words do not slip from your
mouth. Whether at work, over the radio, or around new people, you
don't want to be caught off guard. Additionally, using curse words can
often obscure our thoughts, attaching a strong emotion to something
without actually describing what is happening.

## Getting Caught

When you're in the habit of cursing, you **will** get caught. No
question about it. At work, at prayer, in public, meeting new
people... You don't want that to happen. The best policy on cursing is
simply to stop.

## Mental Clarity

Using curse words can often interfere with your clarity of
thought. Instead of using a curse word, try actually describing what
it is that is troubling you. Don't just use minced oaths like "oh
hamburgers." Say something meaningful, or replace the phrase: "Oh no!"

## Substitute Words

In place of "oh, shit," say "oh, no."

In place of "what the fuck," say "what on earth?"

In place of "fuck" or "dammit," use other interjections like "agh!"

Take a tip from Cee Lo Green. In place of "Fuck You," say "Forget You."

If any other replacement for the ef word fails, you could use a
variation of "wretch," so that "fucking" would become "wretched" or
"wretchedly."

When something is "the shit," just say it's "the stuff" or "the good
stuff." By extension, a lot of instances of "shit" can be replaced by
"stuff." Think "and stuff" and "that stuff" rather than "and shit" and
"that shit."

George Carlin said "fuck the fucking fuckers." To be more descriptive
and less profane, and suitable for radio, we can say "forget the
stupid losers."

For sexual acts, we can use other words besides "fuck." We can simply
say "do me" or "take me." If that's not strong enough, you could say
"plow" or "conquer."

Instead of "cock," you can just say "thing" or "junk." Instead of
"pussy," you can say "cooch." If using "pussy" to refer to a person,
you can simply say "wimp;" it's more descriptive, anyway.

## Remove the Word

In place of "pain in the ass," just say "pain" or "huge pain."

In place of "what the fuck," just say "what?!"

## Use Adjectives

Sad, awful, terrible, painful.
