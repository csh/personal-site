title: Tornado Watch
date: 2024-04-16 12:21
---
Tornado watch today. Subscribed to Gemini logs. Published CWM configs.

Stayed up til 04:00 last night. Woke up at 10:00. Got up at 11:00. Put on deodorant, combed hair, brushed and oiled my beard, took my pills, chugged down the last of yesterday's tea, made tea and oats. Just plain with apple butter, no hemp seeds. Checked and cleared email: Got a decent amount of mail, albeit junk. Checked Mastodon: Nothing. Checked Gemini: Nothing new, subscribed to librehacker and MC.

[librehacker](gemini://gem.librehacker.com/) 

[MC](gemini://gem.hack.org/mc/) 

Well, we're not going to plant the seeds. It is extremely windy today! The neighbor's power is out. There is supposedly a tornado watch. We had to shut the windows.

I'm running into an issue with urxvt: It does not use Noto Color Emoji for emoji. I may have to switch back to MATE Terminal. Switched back to MATE Terminal. Disabled scrollbar and menu bar. This will work, I guess. Published configs.

[Dotfiles](https://codeberg.org/csh/dotfiles) 

Chatted on COM on the SDF Public Access UNIX System. Changed my MATE Terminal theme to Campbell, a color palette from Microsoft. It's very vibrant and clean-looking, without deviating from traditional colors.

[Color Schemes in Windows Terminal](https://learn.microsoft.com/en-us/windows/terminal/customize-settings/color-schemes) 

18:18. Ate a bean burrito and four bean tacos with pico de gallo and salsa verde. Had two medjool dates for dessert.

Fixed the submodule issue in my dotfiles repository. I had to remove the submodule directories and "clone" them back with "git submodule add <URI> <directory>". It doesn't seem to have any ill effects on my configuration. Whew!

20:02. Watched Wildcard Kitchen on Food Network. I can't seem to get the submodule for the font Adobe Source Code Pro to behave. I can't "git add" it, and removing it causes problems, too.

```
caleb@hermes:~/Private/Projects/dotfiles$ git status
On branch main
Your branch is up to date with 'origin/main'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
  (commit or discard the untracked or modified content in submodules)
        modified:   caleb/.local/lib/source-code-pro (untracked content)

no changes added to commit (use "git add" and/or "git commit -a")
```

Reading the Hyperbola wiki. They're too hardcore for me. While I would like an FSF-free version of OpenBSD, their vision is too radical.

[Hyperbola: Incompatible Packages and Projects](https://wiki.hyperbola.info/doku.php?id=en:philosophy:incompatible_packages) 

