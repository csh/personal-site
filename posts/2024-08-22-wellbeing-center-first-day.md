title: Wellbeing Center: First Day
date: 2024-08-22 17:02
---

Went to Wellbeing Center.  Played on the computer.  Learned new Hebrew words.  Met new people.  Played dominoes.  Cleaned computer room.  Chatted online.  Watched TV.  Performed backups.

Today's goal was to go to the Wellbeing Center, meet people, and get acquainted.

## Morning

06:30.  Woke up to alarm.  Brushed hair, got dressed, put on deodorant, combed and brushed beard.

07:00.  Got picked up.

08:00.  Had morning meeting.

09:00.  Ate two bananas.

10:00.  Played on the computer and studied Hebrew vocabulary.

11:00.  Chatted with people.

## Afternoon

12:00.  Ate lunch: Miso soup and rice, and salad with Italian dressing, and watermelon, and a can of cherry Bubly.

Studied Hebrew in the computer room.

Chatted with people in the activity room.

Played dominoes in the activity room.

Cleaned the computer room.

Got a ride home.

## Evening

17:00.  Got on laptop.  Checked email.  Chatted on Snikket.  Checked Mastodon.  Signed up for the [Fediverse Mutual Aid](https://babka.social/@raf/112927240821471667) because I have money for food but nothing else.  Gave them [my PayPal account](https://www.paypal.me/ke0vvt).

18:00.  Drank a Corona beer.  Watched _Beat Bobby Flay_ on Food Network.  Backed up OS with Timeshift.  Installed updates.

Napped.

Ate hash browns and a grilled cheese with cilantro and green onion.

20:30.  Watched Chiefs game.  Watched _MASH_.  Performed backups.

## Tomorrow

* Brush teeth
* Shower
* Fix Sabbath candles