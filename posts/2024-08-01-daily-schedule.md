title: Daily Schedule
date: 2024-08-01 16:33
---

I crave structure.  This is my attempt at ordering my days, so I don't feel so anxious or depressed all the time.

## Morning

* 08:00: Alarm off
* Prayers
* Brush teeth
* Shower
* Deodorant
* 08:30: Get dressed
* Beard oil
* Comb beard
* Brush hair
* Prayers
* 09:00: Breakfast
* 09:30: Check email
* 10:00: FREE TIME: Sit outside
* 11:00: FREE TIME: Sit outside

## Afternoon

* 12:00: Lunch
* 12:30: FREE TIME: Chat
* 13:00: FREE TIME: Music
* 14:00: FREE TIME: Chat
* 15:00: FREE TIME: Music
* 16:00: FREE TIME: Chat

## Evening

* 17:00: Music
* 18:00: Prepare dinner
* 18:30: Eat dinner
* 19:00: FREE TIME
* 20:00: Turn on AC
* FREE TIME
* 21:00: Watch _M*A*S*H_
* 22:00: Bedtime
* Brush teeth
* Put on pajamas
* Set alarm clock
* Prayers
* Close curtains
* Lights off
