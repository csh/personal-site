title: Took Uncle to Hospital
date: 2024-08-03 21:52
---

Took Uncle Jim to the hospital.  Had pizza.  Anxiety level up.  Sat
outside.  Watched TV.

## Morning

08:30.  Said [מודה אני](https://www.chabad.org/library/article_cdo/aid/623937/jewish/Modeh-Ani-What-and-Why.htm) blessing.  Got up.  Got dressed.  Combed hair.  Went downstairs.  Took pills.  Took vitamin B12, which is [necessary for those on a vegan diet](https://www.vegansociety.com/resources/nutrition-and-health/nutrients/vitamin-b12).  Got a Starry to tide me over until breakfast.

Mom took Uncle Jim to the hospital.

10:00.  Ate a bowl of Cheerios with soy milk.  Sat outside with the
laptop for a few minutes.  Chatted with friend from Norway.

10:40.  Mom returns home from taking Uncle Jim to the hospital.

Took a nap.

11:46.  Woke up.  Chatted on IRC.  Watched [FOSSY conference](https://2024.fossy.us/).

12:30.  Aunt Sue came home from work and put up the dishes.  Talked
about Uncle Jim and all the stuff he's done around this house.

## Afternoon

13:30.  Went to the hospital to pick up Uncle Jim; he still has the
chronic vomiting issue.  Went to the pizza shop to pick up pizza for
tonight.  Took laptop with me in the car.

14:00.  Got pizza from Papa Murphey's.  Summer pizza, pepperoni pizza,
and a vegan cheeseless Groumet Vegetarian.  Also got marinara dipping
sauce.  Chatted on IRC on the way home.

14:15.  Came home.  Brought in the pizzas with my laptop.

15:00.  Waiting on pizza.  Got a Bubly.

15:30.  Ate half a large pizza and five Oreos.  Dipped my crust in
marinara sauce.

16:30.  Got a Bubly bellini bliss.

## Evening

17:00.  I've thought of a new naming scheme for computers, if I ever
manage a new site: Old Spice products!

* Timber
* Swagger
* Aqua Reef
* Fiji
* Night Panther
* Krakengard
* Wolfthorn

Watching _King of the Hill_ on Adult Swim.

17:30.  Mom opened a beer.  I guess it's beer thirty.  I'll get a beer
as well, a Shiner Bock.  Plugged my laptop into the charger.

18:00.  Anxiety level up.  Nothing to do.  Going outside.

19:00.  Came back inside.  Feel a little better.  Almost fell asleep.
Came inside and finished my beer.  Grabbed my last Bubly sparkling
water, bellini bliss.

Feeling lonely.  Hope I can do something tomorrow.

20:00.  Installed the GNU Image Manipulation Program and used a
business card template to make a business card.  Going to put my
[Snikket](https://snikket.org/) invite on it.  It looks good, but I
might just print out a single QR code to keep in my pocket notepad
instead.

21:00.  Watched _MASH_.

21:50.  Published blog entry.  Went upstairs.
