title: Publishing chromebook-guix
date: 2024-04-26 14:07
---
## Summary

Stayed up very late last night. Took shower, did laundry. Hacked on Guix.

## Hacking on Guix

Copied "chromebook-guix" source code to Hermes and uploaded to Codeberg. Tried turning chromebook-alsa-lib.scm into a Guile module, but failed. Posted to the Chrultrabook forum.

[no code for module (chromebook alsa-lib)](https://codeberg.org/csh/chromebook-guix/issues/1) 

[Post on Chrultrabook forum](https://forum.chrultrabook.com/t/trying-to-get-audio-working-on-gnu-guix-system/1796)

## Diary

Stayed up until 6 in the morning installling Guix System on Miller, the Chromebook; there were several errors I had to correct: Guix System now includes "nss-certs" in "%base-packages". Slept until 11:30. Took pills. Took shower. Worked on Guix configuration for Miller. Started laundry.

14:00. Got on laptop (Hermes). Logged into Snikket. Logged into COM on the SDF Public Access UNIX System. Checked email. Checked web feeds. Checked Gemini feeds. Checked Mastodon. Checked System Crafters Forum: Still no replies on my post.

[Help Wanted: Guix Audio on Chromebooks](https://forum.systemcrafters.net/t/help-wanted-guix-audio-on-chromebooks/623)

[Taiwan pledges to remove 760 statues of Chinese dictator Chiang Kai-shek.](https://www.theguardian.com/world/2024/apr/23/taiwan-pledges-to-remove-760-statues-of-chinese-dictator-chiang-kai-shek) 

15:30. Ate potatoes O'Brien with salt and pepper.

19:36. Had matzo ball soup with four matzo balls. Had a cup of Sunny-D.

21:01. Watched TV. Rinsed mouth with Listerine. Went upstairs.

