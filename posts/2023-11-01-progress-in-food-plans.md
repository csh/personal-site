title: Progress In Food Plans
date: 2023-11-01 00:00
---

A lot has changed in three months. I've stopped eating fake meats and
cheeses. I now eat oatmeal for breakfast with apple butter and hemp
seeds. For dinner, I have [potato soup][1], [sweet potato stew][2],
bean burritos, [breakfast lentils](https://veggienumnum.com/vegetarian-recipes/breakfast-lentils/), and more. For drinks, I have [Chinese white tea][4].

[1]: https://blog.fatfreevegan.com/2009/02/quick-and-easy-potato-soup.html
[2]: https://www.drmcdougall.com/recipes/tunisian-sweet-potato-stew/
[3]: https://veggienumnum.com/vegetarian-recipes/breakfast-lentils/
[4]: https://www.shangtea.com/tea/white-tea
