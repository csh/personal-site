title: Are You Amish?
date: 2022-03-23 00:00
---
No.  However, there is a reason I dress this way.

I like wearing the same clothes every day.  It frees me to think and
do other things.  I used to simply wear jeans and a polo shirt, but
this uniform would not work for many occasions.  To remedy this
problem, I've decided to start wearing a suit every day.  All I need
now are dress shoes, a jacket and a tie to go with my pants.

I have a complicated relationship with technology.  Since most
computerized things require
[proprietary software](https://www.gnu.org/proprietary/proprietary.html),
I try to reject most of it.

I love the Pennsylvania German language.  I study it with the help of
Doug Madenford's [PA Dutch 101](https://padutch101.com/) resource.  I
practice with Amish people whenever I can.  I also study Yiddish, and
find the
[similarities between the languages](https://www.yiddishbookcenter.org/collections/oral-histories/excerpts/woh-ex-0001571/yiddish-and-pennsylvania-dutch)
to be fascinating.

[Caleb Herbert](https://bluehome.net/csh/)
