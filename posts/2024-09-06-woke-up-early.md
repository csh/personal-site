title: Woke Up Early
date: 2024-09-06 08:44
---

Chatted online.  Checked email.  Performed backups.  Made phone calls.  Watched TV.

Today's tasks:

* Perform backups
* Light שבת (Sabbath) candles
* Shower

## Morning

08:00.  Got up to alarm.  Said [מודה אני](https://chabad.org/623937).  Got fully dressed.  Went downstairs.  Took pills.  Chatted online.  Checked email.  Brushed teeth.

09:00.  Removed the web browser from my phone yesterday.  Now I just have four apps on my home screen: [Cheogram](https://cheogram.com/), K-9 Mail, Mastodon, and Calculator.  Checked [Free Software Foundation](https://fsf.org/) forum.  Checked Mastodon.  Checked Lemmy.

10:00.  Made tea and oatmeal.  Had tea with caramel creamer and oatmeal with pumpkin butter.

## Afternoon

12:00.  Finished tea.  Called friend on Snikket.

13:30.  Called  the Wellbeing Center about being picked up next week.  Also sent them an email with my address and phone number.  I'll be able to come Wednesday and Friday, but it's too late to be picked up Monday.

14:15.  Emailed [בית שלום (Beith Shalom) in Speyer](https://www.jkgrp.de/) about whether they are Orthodox or Liberal.

15:30.  Installed Cool Retro Term to edit my diary in.  It's delightful!  And it supports Hebrew text better than Apostrophe, ironically!

16:00.  Wished friends שבת שלום (good Sabbath) and assessed what I need to do to prepare.

* Make заварка (tea concentrate)
* Set table
* Fix candlesticks
* Shut off electronics

## Evening

17:00.  Called friend on Snikket.  Ate Amy's Thai red curry with jasmine tea.

18:26.  Made tea.  Set table.  Fixed שבת candlesticks.  Shut off my phone.  Logged off of [SDF](https://sdf.org/).

19:00.  Published this post.  Shut off laptop.

## Tomorrow

* Read תורה (Torah) portion of the week
* Dress in nicer clothes (white shirt, vest)
* No electronics