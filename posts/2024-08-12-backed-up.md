title: Backed Up
date: 2024-08-12 22:04
---

Performed backups.  Took out the trash.  Chatted online.  Harvested tomatoes from the garden.  Listened to the radio.  Watched TV.  Today's agenda was to shower and harvest the tomatoes in the garden.

## Morning

Stayed up til midnight last night.

11:00.  I did not mean to sleep this long!  Got up, got dressed.  Went downstairs, took my meds, checked laptop, made some tea.  Took the trash out.  Backed up flash drive to main drive.  Backed up home directory to main drive.

## Afternoon

12:30.  Checked email.  Downloaded a new episode of _Futurama_ and put it on the Jellyfin.

13:00.  Got two bowls of Cinnamon Toast Crunch with soy milk, and a cup of tea.  

14:00.  Tuned in to classic country station KZHE.  Switched to 94.9 KCMO after a while.

14:30.  Went to the garden to pick tomatoes.

![Tomatoes from garden](https://babka-mastodon.nyc3.cdn.digitaloceanspaces.com/media_attachments/files/112/950/903/531/779/746/original/455bac7734ab7cb6.jpeg)

15:46.  Had a wonderful visit to the restroom.  Tuned in to 98.9 The Rock.  Turned radio off and watched Food Network.

## Evening

17:00.  Downloaded home directories from sdf.org and bluehome.net, so they get included in my daily backups.

Workflow:

* Go to ~/Sync and run ./pull
* Plug in main hard drive
* Open Pika Backup and press Back Up Now

Simple!

I've done this with my sdf.org account as well.  All my data is now firmly in my own possession.

```
caleb@miller:~/Sync$ cat pull 
#!/bin/bash
ssh csh@tty.sdf.org 'tar pvcf - ~' | tar xvf -
rsync -avh csh@bluehome.net@bluehome.net:~/ bluehome/~csh/
```

Got more tea.  Changed desktop to Dark Mode.  Said good night to friend from Norway.

18:00.  Chatted on [SDF](https://sdf.org/) Public Access UNIX System's COM chat system.  Completed bluehome backup.

19:00.  Ate two peanut butter and jelly sandwiches with whole wheat bread, natural peanut butter, and grape jelly.

20:00.  Watched Food Network.

21:00.  Watched _MASH_.  Added `isf` and `pizzapal` on Snikket.

22:00.  Pulled remote home directories to laptop.  Backed up laptop.

## Tomorrow

* Go to dinner with family
