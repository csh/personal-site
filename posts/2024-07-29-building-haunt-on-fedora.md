title: Building Haunt on Fedora
date: 2024-07-29 15:30
---

This is a guide to building (or installing) [Haunt](https://dthompson.us/projects/haunt.html) from source on Fedora 40.

## Packages

Some requirements for Haunt are already packaged in Fedora.

```sh
sudo dnf install guile guile-devel autoconf automake texinfo guile-reader guile-reader-devel
```

## Guild

Guild is installed, but its path needs to be rectified for build processes to see it.  Use `GUILD=$(which guild)` with `./configure` to remedy this.

## Commonmark

`guile-commonmark` needs to be built from source.

```sh
git clone https://github.com/OrangeShark/guile-commonmark
cd guile-commonmark
./bootstrap
GUILD=$(which guild) ./configure
make
sudo make install
```

## Haunt

Now, we can build Haunt.

[Download latest Haunt release](https://files.dthompson.us/releases/haunt/haunt-0.3.0.tar.gz), extract it, and open it in a terminal.

Set variables in current shell.

```sh
GUILD=$(which guild)
GUILE_LOAD_PATH=/usr/local/share/guile/site/2.0/
```

Set variables in future shells.

```sh
cat <<END >> "$HOME/.bashrc"
GUILD=$(which guild)
GUILE_LOAD_PATH=/usr/local/share/guile/site/2.0/
END
```

The above does not work.  Explicitly declare variables with each command.  Build.

```sh
GUILD=$(which guild) GUILE_LOAD_PATH=/usr/local/share/guile/site/2.0/ ./configure
GUILD=$(which guild) GUILE_LOAD_PATH=/usr/local/share/guile/site/2.0/ make
GUILD=$(which guild) GUILE_LOAD_PATH=/usr/local/share/guile/site/2.0/ sudo make install
```

Success!

