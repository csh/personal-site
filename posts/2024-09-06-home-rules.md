title: Home Rules
date: 2024-09-06 10:40
---

This is a list of proposed rules for my home.

## Food

All food brought into the home must be כשר פארעווע (kosher pareve) and vegan, containing no meat, dairy, fish or egg products.  No vegan meats or cheeses, and no vegetable oils, including margarine.  No candy.  No sugar drinks, including fruit juices; grape juice is allowed for שבת (Sabbath) and holidays.

## Internet

Internet is not to be used for entertainment.  Consider other activities such as playing cards, board games, listening to music, swimming, reading, playing ball, riding bikes, walking, journaling, writing letters, knitting, etc.

Internet must be filtered at the router level.  Devices should be in a public area and password-protected.  Kids may not have a social media account.  Kids younger than 11 should not have an email account.  Kids should be accompanied by a parent in chat rooms and groups.

No Twitch, Steam, Snapchat, Facebook, Instagram, WhatsApp, Telegram, Signal, or Reddit.

YouTube videos may be downloaded and watched occasionally using [Video Downloader](https://flathub.org/apps/com.github.unrud.VideoDownloader).

## Technology

No TV, radio, video games, smartwatches, or movies.

Kids may not have a smartphone, tablet or computer, except for the shared home computer.

The home telephone is always available for talking to friends.