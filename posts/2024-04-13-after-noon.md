title: Dear Diary
date: 2024-04-13 21:21
---
Got up after noon. Helped Mom fix a window. Tried setting up SDF OpenVoIP. Finally cleared inbox. Themed my desktop. Helped Mom plant cucumbers. Forked ianmjones.com.

12:00. Got up late. Went downstairs. Took pills. Took vitamin B12. Helped Mom replace a screen in the back window of the house. Made tea and oats. Had oats with apple butter and tea with soy milk. Logged into Snikket and SDF Public Access UNIX System, and tuned into aNONradio in VLC.

13:32. Went upstairs to charge laptop. Chatted in COM on the SDF Public Access UNIX System. Publius gave me information on how to use OpenVoIP. I set up a SIP login with maint. GNU Jami and Linphone both failed to connect to SDF. I will try my Grandstream HT802 later. Then, to use OpenVoIP, I'll just have to move my telephone to the other port that's assigned to SDF.

[Setting up a SIP phone with SDF VoIP](https://sdf.org/?tutorials/sdf_voip)

[Grandstream HT802 User Guide](https://www.grandstream.com/hubfs/Product_Documentation/ht80x_user_guide.pdf) 

14:42. Checked email. Cleared out inbox. Checked Gemini subscriptions. Checked Mastodon.

15:27. Went downstairs with (charged) laptop. Got another can of (strawberry) Bubly. Added one and a half spoonfuls of sugar to it. Downed it. Got a lime Bubly, without sugar. The breeze from the windows is really helping freshen up the house.

16:30. userfxnet on SDF pointed me to World Radio Map, in case I might be able to get KAYQ "The Lake" on there. Sadly, World Radio Map only has Springfield, Missouri.

[Springfield, MO on World Radio Map](https://worldradiomap.com/us-mo/springfield)

17:32. Reconfigured my desktop to look like a traditional GNOME 2 desktop. Nostalgia! Only thing missing is the "Human Clearlooks" theme.

[Image of a terminal with screenfetch on my Trisquel desktop with traditional MATE configuration](https://bluehome.net/csh/assets/images/Screenshot_of_Trisquel_traditional_MATE_at_2024-04-13_17-09-12.png) 

17:45. Posted to the Trisquel forum, asking users how they deal with the problems of the Web.

> Avoiding proprietary software requires lots of addons and tweaking of the web browser. What addons do you use? Do you use Pi-Hole and/or Privoxy? How should we unite against the threat of proprietary scripts? Should we all have the same browser extensions? Is there a One True Browser Configuration?
>
> Currently, I'm using:

> JShelter  
> GNU LibreJS
>
> I thought about Haketilo, but it's a proxy now, and I don't know how to do those. I've also thought of setting up Pi-Hole to block Meta and Google on my network. I used to use Privacy Redirect, but Nitter stopped working, and some of the replacements required JavaScript.

[What browser addons do you use? | Trisquel GNU/Linux](https://trisquel.info/en/forum/what-browser-addons-do-you-use) 

18:23. Signed the Free Software Foundation's petition for a free software tax filing system. This is an important issue. I hope you sign the petition, too!

[Petition: Free as in freedom tax filing](https://my.fsf.org/civicrm/petition/sign?sid=10&reset=1) 

19:18. Ate four bean burritos with guac, pico de gallo, and salsa verde. Had two shots of gin. I'm drunk!

20:34. Helped Mom plant cucumbers in the garden. I have to do more planting tomorrow: Dill, green beans, cucumbers. Chatted on SDF COM.

21:00. Went upstairs. Got ready for bed.

23:28. Forked Ian M. Jones's Gemini capsule.

[ianmjones.com source code](https://git.sr.ht/~ianmjones/ianmjones.com) 
