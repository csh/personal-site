title: How to Flash GNU Boot
date: 2024-04-18 16:48
---
This task was completed on a Lenovo ThinkPad X200s running Trisquel GNU/Linux and Libreboot.

Install flashrom.

```
$ sudo apt install flashrom
```

Find the flash chip size.

```
caleb@hermes:~$ sudo flashrom -p internal
flashrom v1.2 on Linux 5.15.0-102-generic (x86_64)
flashrom is free software, get the source code at https://flashrom.org

Using clock_gettime for delay loops (clk_id: 1, resolution: 1ns).
coreboot table found at 0xbfad6000.
Found chipset "Intel ICH9M-E".
Enabling flash write... OK.
Found Winbond flash chip "W25X64" (8192 kB, SPI) mapped at physical address 0x00000000ff800000.
No operations were specified.
```

"8192 kB" means we want the "8mb" ROM for GNU Boot. Let's download and extract it.

```
$ wget https://ftp.gnu.org/gnu/gnuboot/gnuboot-0.1-rc3/roms/gnuboot-0.1-rc3_x200_8mb.tar.xz
$ tar -xvJf gnuboot-0.1-rc3_x200_8mb.tar.xz
```

Make a backup of your current ROM.

```
$ sudo flashrom -p internal:laptop=force_I_want_a_brick,boardmismatch=force -r dump.bin
```

Make another backup.

```
$ sudo flashrom -p internal:laptop=force_I_want_a_brick,boardmismatch=force -r dump2.bin
```

Make sure the dumps are the same.

```
caleb@hermes:~/Downloads/x200_8mb$ sha1sum dump.bin dump2.bin 
60bb524609ac132eb116f2892160742f21ebc494  dump.bin
60bb524609ac132eb116f2892160742f21ebc494  dump2.bin
```

Flash the ROM.

```
caleb@hermes:~/Downloads/x200_8mb$ sudo flashrom -p internal:laptop=force_I_want_a_brick,boardmismatch=force -w grub_x200_8mb_libgfxinit_corebootfb_ukqwerty.rom
```

You should see the following if successful:

```
Verifying flash... VERIFIED.
```

That's it! Enjoy your 100% free/libre X200!
