title: Vegetable Soup
date: 2024-08-02 15:37
---

Grandma made this soup all the time.  This recipe makes enough for one
person, with some for leftovers.

## Ingredients

* 4 cups vegetable broth
* 2 cups Low Sodium V-8 Juice *or* tomato juice
* Carrots
* Onions
* Potatoes
* Cabbage
* Green beans
* Celery
* Ground hamburger *or* brown lentils

## Instructions

Cook until vegetables are tender and meat is cooked.
