title: Wrote A Script
date: 2024-04-19 16:34
---
Woke up too late. Went to Walmart. Got pizza. Wrote a script.

Got up at noon! Brushed hair, combed and oiled beard. Went to Walmart. Got bread, milk, lime Bubly, trash bags, more plants, and a Starbucks frap. Checked mail.

Had pizza for lunch.

Wrote a script to automate posting to my log. It automatically handles generating a file name from the title of the post, and publishes the post as soon as you finish previewing the document.

[post](https://codeberg.org/csh/dotfiles/src/commit/e96b31336a06154c15809510c8a993df4e5a9456/caleb/.local/bin/post) 

Checked email, Mastodon, web feeds, and Gemini. Ate pizza for dinner. Had a big stick of black licorice and a piece of dark chocolate for dessert.

Watched TV. Chatted on IRC. Got ready for bed.
