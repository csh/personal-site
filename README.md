# Personal Site

My personal site at [bluehome.net](https://bluehome.net/csh/), generated with
the [Haunt][1] static site generateor.

## Getting Started

1. Get all the dependencies for this repo:

		guix shell

2. In `Makefile`, change `csh@bluehome.net@bluehome.net:public_html/` to your
   preferred publishing location.
3. `haunt build`
4. `make publish`

[1]: https://dthompson.us/projects/haunt.html
