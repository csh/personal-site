---
layout: default
title: How I Do My Computing
---

# How I Do My Computing

This page is inspired by [Richard Stallman's page](https://stallman.org/stallman-computing.html) of the same name.

## Devices

I use a laptop, a ThinkPad X200T computer with [Libreboot](https://libreboot.org/).

I use a mobile device, a Google Pixel 4a, mostly for the camera and
playing music.

## Operating Systems

On my mobile device, I use [Graphene OS][1].

[1]: https://grapheneos.org/

On my laptop, I use [Trisquel GNU/Linux](https://trisquel.info/).

## Services

I run Syncthing to sync my photos from my mobile device.

I run [Jellyfin](https://jellyfin.org/) to stream my media to the Roku TV.

I run Tor to transfer large files over the internet with OnionShare.

## Apps

I install most of my apps with Flatpak so that they are sandboxed and
independent of the operating system, and are up to date.

I do use Flathub.  Sadly, it contains proprietary software.  But, I
steer away from that and only install the free packages on Flathub.  I
did experiment with making [my own Flatpak remote](https://bluehome.net/csh/apps/) and would like
to make a mirror of Flathub that contains only free software and
complies with the GNU [Free System Distribution Guidelines](https://www.gnu.org/distros/free-system-distribution-guidelines.en.html).

Most of what I do on my computer is email and chatting.  I have
several apps for that.

- Trisquel's Pidgin, for XMPP, and SMS via [JMP](https://jmp.chat/)
- [Fractal](https://flathub.org/apps/details/org.gnome.Fractal) for Matrix
- Trisquel's Icedove, for my personal mail and business Gmail
- [Mumble](https://flathub.org/apps/details/info.mumble.Mumble), for voice chat
- [Polari](https://flathub.org/apps/details/org.gnome.Polari), for IRC
- [Telegram](https://flathub.org/apps/details/org.telegram.desktop)
- [qTox](https://flathub.org/apps/details/io.github.qtox.qTox), for Tox

For web browsing, I use Trisquel's Abrowser.  For running proprietary web
apps, I use Firefox installed with Flatpak.

For downloading large files, I use [OnionShare](https://flathub.org/apps/details/org.onionshare.OnionShare) and torrent with Trisquel's Transmission.

For watching videos, I download them with [Video Downloader](https://flathub.org/apps/details/com.github.unrud.VideoDownloader) and watch them in Trisquel's VLC.

## Web Apps

[Web applications are apps, too](https://www.gnu.org/philosophy/javascript-trap.html).  Sadly, most of them are
proprietary.  I've tried doing without them several times, but I keep
going back to them.  Reddit, not so much, but Discord has amazing
language-learning communities for Yiddish and Hebrew that I can find
nowhere else.

- Reddit
- Discord
- Microsoft Outlook, for work
- [Paycom](https://paycomonline.com/), for work
- [Online Banking](https://secure.centralbank.net/login/login/Login.jsp), for Sundays when the bank is closed, since I shouldn't bank on Shabbat.

## Mobile Apps

Though I try to keep my mobile use down to just music and a camera,
I've started installing more apps so I can do things away from home.
I install most of my apps from [F-Droid][1] and a few from Aurora
Store.

- [VLC][2] for playing music
- [Syncthing][3] for transferring my photos to my laptop
- [Sefaria][4] for studying Torah.  Free software, but only available on the Aurora Store.
- [Telegram FOSS][5]
- [Revolution IRC][6]


[1]: https://f-droid.org/
[2]: https://f-droid.org/en/packages/org.videolan.vlc/
[3]: https://f-droid.org/en/packages/com.nutomic.syncthingandroid/
[4]: https://sefaria.org/
[5]: https://f-droid.org/en/packages/org.telegram.messenger/
[6]: https://f-droid.org/en/packages/io.mrarm.irc/