---
layout: default
title: Recipes
---
- <a href="https://veggienumnum.com/vegetarian-recipes/breakfast-lentils/">Breakfast lentils</a>
- <a href="https://www.drmcdougall.com/recipes/banana-nut-bread/">Banana bread</a>
- Thanksgiving: <a href="https://www.drfuhrman.com/recipes/1686/nutty-green-bean-casserole">Green bean casserole</a></li>
- My mom’s <a href="/csh/2021/10/18/vegetable-soup">vegetable soup</a>
- <a href="https://www.forksoverknives.com/recipes/mashed-potatoes-and-gravy/">Mashed potatoes and gravy</a>
- <a href="https://www.drmcdougall.com/recipes/roasted-garlic-bread/">Garlic toast</a> and <a href="https://minimalistbaker.com/how-to-make-vegan-parmesan-cheese/">cashew parm</a>.
- <a href="https://www.brandnewvegan.com/recipes/beans-legumes/jeff-novick-bean-burgers">Burger</a> – makes good meatloaf with <a href="https://shaneandsimple.com/lipton-onion-soup-mix-recipe/">copycat lipton onion soup mix</a>.
- <a href="https://www.drmcdougall.com/recipes/peanut-dressing/">Peanut sauce</a>
- <a href="https://www.drmcdougall.com/recipes/asian-ginger-sauce/">Chinese garlic sauce</a><
