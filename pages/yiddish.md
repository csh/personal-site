title: Yiddish
---

- [Dictionary](https://www.cs.uky.edu/~raphael/yiddish/dictionary.cgi)

## Books

- _College Yiddish_ by Uriel Weinreich, [PDF](/csh/assets/books/College_Yiddish.pdf)
- [Harkavy's dictionary](https://www.hebrewbooks.org/43567 harkavy's  extended 1928 edition dictionary)
- <http://verterbukh.org/>
- [Fiziḳ af yedn ṭroṭ miṭ 109 gemeln](https://www.yiddishbookcenter.org/collections/yiddish-books/spb-nybc211074/perelman-yankel-fizik-af-yedn-trot-mit-109-gemeln).  _A basic introduction to physics for children with 109 "experiments"_.
- [_Tom Sawyer_ in Yiddish](https://www.yiddishbookcenter.org/collections/yiddish-books/spb-tomsoyer00twai/twain-mark-volkenshteyn-tom-soyer)
- [History of the telephone and telegraph](https://www.yiddishbookcenter.org/collections/yiddish-books/spb-nybc213321/yeramitski-inzsh-telefon-un-telegraf).  To those interested, there is Morse code at the back. Extremely faded and, unfortunately, for Cyrillic.
- A [children's book](https://www.yiddishbookcenter.org/collections/yiddish-books/spb-nybc213492/kvitko-leib-mashines) of songs and poems about factories and machines in praise of the work of the USSR.
- [_Sinbad the Sailor_](https://www.yiddishbookcenter.org/collections/yiddish-books/spb-nybc213244/reznik-lipe-sindbad-der-yamforer-an-arabish-maysele). Sindbad der yamforer an Arabish maysele.
- [How to make and use chlorine (can never have enough!)](https://www.yiddishbookcenter.org/collections/yiddish-books/spb-nybc213137/sas-tisovski-b-a-khlor-vi-me-bakumt-im-un-vi-me-nutst-im-oys)
- [_Ballad of Reading Gaol_](https://archive.org/details/nybc211546/page/n4/mode/2up) by Oscar Wilde
- [_The Tales of Rabbi Nachman_](https://yi.wikisource.org/wiki/%D7%9E%D7%A4%D7%AA%D7%97%D7%95%D7%AA_%D7%A1%D7%A4%D7%95%D7%A8%D7%99_%D7%9E%D7%A2%D7%A9%D7%99%D7%95%D7%AA)
- [_di koyech fun yiddischen humor_]"(https://www.yiddishbookcenter.org/collections/yiddish-books/spb-nybc202580/dzigan-shimon-der-koyekh-fun-yidishn-humor) by Shimon Dzigan. It was published in 1947 and chronicles his life onstage and off from Poland to Israel.
- From the [_Survivors’ Haggadah_](https://digipres.cjh.org/delivery/DeliveryManagerServlet?dps_pid=IE2389127) (its facsimile of the original from 1946 on the left, English translation and notes on the right).
- [Yiddish](https://archive.org/details/nybc213577) [jokes](https://sites.google.com/site/onlineyiddishresources/siteonlineyiddishresourcesfolklor/siteonlineyiddishresourcessiteonlineyiddishresourcesfolklorhumor)
- [בײלע מיר פֿאָרן](https://www.yiddishbookcenter.org/collections/yiddish-books/spb-nybc213140/schaechter-gottesman-beyle-mir-forn), 3 minute read

* * *

### Elie Wiesel's _Night_ in the original Yiddish

The novel was serialized in the Forverts in 1965, which you can find online on the [Historical Jewish Press](https://www.nli.org.il/en/newspapers/frw?&e=-------en-20--1--img-txIN%7ctxTI--------------1)  website. I don't know however if this differs significantly from the version published in book form in 1956 in Buenos Aires.

Here are the Forverts dates:

- 16-Apr-65
- 19-Apr-65
- 20-Apr-65
- 21-Apr-65
- 22-Apr-65
- 23-Apr-65
- 26-Apr-65
- 27-Apr-65
- 28-Apr-65
- 29-Apr-65
- 30-Apr-65
- 3-May-65
- 4-May-65
- 5-May-65
- 6-May-65
- 7-May-65
- 10-May-65
- 11-May-65
- 12-May-65
- 13-May-65
- 14-May-65
- 17-May-65
- 18-May-65
- 19-May-65
- 20-May-65
- 21-May-65
- 24-May-65
- 25-May-65
- 26-May-65
- 27-May-65
- 28-May-65
- 31-May-65
- 1-Jun-65
- 2-Jun-65
- 3-Jun-65
- 4-Jun-65
- 7-Jun-65
- 8-Jun-65
- 9-Jun-65
- 10-Jun-65
- 11-Jun-65
- 14-Jun-65
- 15-Jun-65
- 16-Jun-65
- 17-Jun-65
- 18-Jun-65
- 21-Jun-65
- 22-Jun-65
- 23-Jun-65
- 24-Jun-65
- 25-Jun-65
- 28-Jun-65
- 29-Jun-65
- 30-Jun-65
- 1-Jul-65
- 2-Jul-65
- 5-Jul-65
- 6-Jul-65

* * *

### Yiddish Health Tips

New book in Yiddish health tips, from Hamaspik monthly publications.

Here's two copies of the Yiddish publication I managed to find online. 

I receive it in the post, you can probably get the pdf version as well by emailing [gazette@nyshainc.org](mailto:gazette@nyshainc.org).

- <175-Gazette-Yiddish.pdf>
- <191_Yiddish.pdf>
- <https://nyshainc.org/>

* * *

### Yiddish Jokes

[ייִדישע װיצן](https://www.yiddishbookcenter.org/collections/yiddish-books/spb-nybc200848/rawnitzki-yehoshua-hana-yidishe-vitsn-vol-1)

This one might be funny, if read with a Polish Yiddish accent.

![A goy on a yid](https://cdn.discordapp.com/attachments/702910644613152799/943544963252228166/unknown.png)

Jokes about spelling are hilarious.

![A libe-shidekh](https://cdn.discordapp.com/attachments/702910644613152799/943546815310078073/unknown.png)

![In mayn tsetl](https://cdn.discordapp.com/attachments/702910644613152799/943546863636873306/unknown.png)

## Resources

- <https://www.jiddischwoordenboek.nl/>
- <https://www.amazon.com/Mayne-ershte-toyznt-verter-yidish/dp/3947994303>
- <http://yiddish.biz/>
- <http://dovidkatz.net/dovid/PDFLinguistics/2-1987-Grammar-Yiddish.pdf>
- <http://www.cycobooks.org/Zucker_Yiddish.pdf>
- [Podcast in Yiddish](https://itunes.apple.com/us/podcast/the-yiddish-voice-podcast/id915622071?mt=2)
- [A good course for learning the alphabet](https://www.memrise.com/course/59628/yiddish-alphabet/)
- [A good course on memrise](https://www.memrise.com/course/1120518/modern-yiddish-complete-with-audio/)
- [Yiddish dialect dictionary](https://archive.org/details/YiddishDialectDictionary)
