---
layout: default
title: Caleb Herbert's Wish List
---

# Wish List

- $5: Server: [Backplane for motherboard](https://www.aliexpress.com/item/3256801143520214.html)
- $8: Server: [RAM](https://www.newegg.com/p/N82E16820148112)
- $8: Server: [Thermal paste](https://www.newegg.com/p/N82E16835100007)
- $10: Server: [Intel Core 2 Duo E6850](https://www.aliexpress.com/item/2251832579520924.html)
- $10: Server: [USB 3 to USB 2 header adapter](https://www.newegg.com/p/181-013S-00043)
- $24: Server: [CPU Fan](https://www.newegg.com/p/N82E16835230011)
- $25: Matcha powder
- $25: Server: Motherboard: Gigabyte GA-G41M-ES2L.
- $26: Server: [Power supply](https://www.amazon.com/dp/B077BQ76TP)
- $27: Server: [DVD Drive](https://www.newegg.com/p/N82E16827140119)
- $27: Books: [_Speak With Distinction_](https://www.amazon.com/Speak-Distinction-Classic-Skinner-Method/dp/1557830479/ref=sr_1_1?crid=1AXF9QX2FNS97&keywords=speak+with+distinction&qid=1662840092&sprefix=speak+with+distinction%2Caps%2C1284&sr=8-1)
- $35: Books: [Torah in Yiddish](https://www.shopeichlers.com/products/chumash-ori-veyishi-5-volume-set/45015)
- $40: Server: [Micro ATX Case](https://www.newegg.com/p/N82E16811353150)
- $90: Laptop: [battery](https://www.laptopbatteryexpress.com/Lenovo-ThinkPad-X200-laptop-Battery-43R9255-47-p/len-47xt.htm)
- $395: Wardrobe: [Black Cap-toe Oxford shoes](https://www.allenedmonds.com/product/mens-park-avenue-cap-toe-oxford-3023014/black-ec4001390)
- $790: Wardrobe: black Borsalino fedora
- $800: Wardrobe: navy 3-piece suit from [Houndstooth](http://www.houndstoothkc.com/)
