---
layout: page
title: About
permalink: /about/
---

I'm just a Jewish nerd who hates the social ills of modern technology.

### Contact me

[csh@bluehome.net](mailto:csh@bluehome.net)

[(816) 892-9669](tel:+1-816-892-9669)

[@kolev@babka.social](https://babka.social/@kolev)